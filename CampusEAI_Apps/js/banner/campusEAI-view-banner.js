//-------------------- Code for Student Registration ------------------//
/**
 * <dispStudentRegList>
 * <display all student registration list>
 * @param {json array} json array of registration list
 * @return {none}
 */

function dispStudentRegList(data) {
    
    $("#studentRegistrationDetailsMain").empty();
    //console.log(data.termDropDown[0].TERM_CODE);
    var html_view = "<select id='studentRegistrationInOption' data-theme='f'  name='reg' onchange='getRegCRNList(this.value);'>";
    html_view += "<option value=''>Search by Term</option>";
    if(data !== undefined) {
        for(i=0; i<data.termDropDown.length; i++) {
            var termCode = data.termDropDown[i].TERM_CODE;
            var termDesc = data.termDropDown[i].TERM_DESC;
            html_view +="<option id='"+i+"' value='"+termCode+"'>"+termDesc+"</option>";
        }
        
        html_view +="</select>";
    }
    $("#studentRegistrationInfo").html(html_view);
    $("select#studentRegistrationInOption").find("option#0").attr("selected", true);
    getRegCRNList(data.termDropDown[0].TERM_CODE);
    $.mobile.changePage("index.html#studentRegistrationPage",{transition:"none"});
    $('#studentRegistrationPage').trigger('create');
}


/**
 * <dispStdRegOptionsDetails>
 * <display all student registration details page>
 * @param {json array} json array of CRN no.
 * @return {none}
 */

function dispStdRegOptionsDetails() {
   
    var html_view = "";
    html_view +="<div class='cb10'></div>";
    html_view +="<div style='margin:-4px 0 8px 0;float:left;width:100%;text-align:center;font-size:16px;font-weight:bold;color:#ffffff;text-shadow:none;'>Add Classes</div>";
    html_view +="<div style='clear:both;float:left;width:90%;margin-left:4%;'>";
    html_view +="<div class='registartion_addclass' style='padding:11px 0 10px 0;'>CRN:</div><div class='registartion_addclass_right'><input type='number' data-theme='c' name='crn1' id='crn1'  value=''  placeholder='CRN'></div>";
    // html_view +="<div class='registartion_addclass'><input type='text' name='crn3' id='crn3'  value='' placeholder='CRN'></div><div class='registartion_addclass_right'><input type='text' name='crn4' id='crn4'  value='' placeholder='CRN'></div></div>";
    // html_view +="<div class='registartion_addclass'><input type='text' name='crn5' id='crn5'  value='' placeholder='CRN'></div><div class='registartion_addclass_right'><input type='text' name='crn6' id='crn6'  value='' placeholder='CRN'></div>";
    html_view +="<p class='cb10'></p>";
    html_view +="<div data-role='button' data-theme='f' style='margin:15px auto;width:75%;text-align:center;text-shadow:none!important;' onclick='submitRegistrationDataST()'>Submit</div>";
    html_view +="<div class='lookup_container_h'><div class='lookup_heading'>Look Up Classes</div><div class='lookup_btn_h' data-role='button' data-theme='f' style='margin:15px auto;width:75%;text-align:center;text-shadow:none!important;' onclick='BeforeStudentRegLookUp()'>Look Up Classes</div><div id='showLookupClassesFilterFormData'></div></div>";
    html_view +="<div data-role='button' data-theme='f' style='margin:15px auto;width:75%;text-align:center;text-shadow:none!important;' onclick='getCurrentClassesDataST()'>Current Classes</div>";
    html_view +="<div class='cb10'></div>";
    html_view +="<div style='clear:both;width:90%;margin:27px auto 0 auto; padding:10px 0;border-top:#ffffff solid 1px;font-size:10px;color:#ffffff;text-shadow:none;'>";
    html_view +="<span style='font-weight:bold; font-size:11px!important;'>Note:</span>&nbsp;If you are unsure of which classes to add, tap Look Up Classes to review the class schedule.</div>";
    
    $("#studentRegistrationDetailsMain").html(html_view);
    $('#studentRegistrationPage').trigger('create');
}

function BeforeStudentRegLookUp()
{
    var id = $("#studentRegistrationInOption").val();
    if(!id || id=="Select") return;
    
    
    url_service="/enmu/services/student/bannergetOtherClassSearchParams/termCode="+id;
    return_Where=showRegLookupClassesFilterFormData;
    request_ST(url_service,return_Where,false);
}

/**
 * <dispLookUpClasses>
 * <display all student look up classes>
 * @param {json array} json array of look up classes details
 * @return {none}
 */

function dispLookUpClasses(lookupClassData, currentClassData) {
  
    console.log(currentClassData.scheduleCourseDetail);
    console.log(lookupClassData.CLASSLOOKUP);
  
   $("#stdLookUpClassesDetails").empty();
    var html_view = "";
    var flag = "true";
    
    var myClasses = {};
    if(currentClassData.scheduleCourseDetail && currentClassData.scheduleCourseDetail.length)
    {
      var myClassLen = currentClassData.scheduleCourseDetail.length;
      for(var j=0; j < myClassLen; j++)
      {
        if(currentClassData.scheduleCourseDetail[j].REG_CODE_DESC != 'Web Drop') 
        {
          myClasses[currentClassData.scheduleCourseDetail[j].CRN] = true;
        }
      }
    }
    
    console.log("SEE THIS");
    console.log(myClasses);
        
    if(lookupClassData.CLASSLOOKUP.length) { 
        
        var lookClassLen = lookupClassData.CLASSLOOKUP.length;
        for(i=0; i<lookClassLen; i++) {
        if(!myClasses[lookupClassData.CLASSLOOKUP[i][0].CRN]) {
            
            breakCrsID=lookupClassData.CLASSLOOKUP[i][0].COURSE_ID.split(" ");
            
             html_view +="<div class='look_detail_holder'><div class='head_holder'><div id='lookupTitle"+i+"' class='look_detail_head_left' style='width:90%;'>"+lookupClassData.CLASSLOOKUP[i][0].TITLE+": "+breakCrsID[0]+" "+breakCrsID[1]+"</div></div>";
            
            html_view += "<div class='list_holder'><div class='list_holder_left'>Section: </div><div class='list_holder_right'>"+breakCrsID[2]+"</div></div>";
            
            
             html_view += "<div class='list_holder'><div class='list_holder_left'>CRN: </div><div id='lookupcrn"+i+"' class='list_holder_right'>"+lookupClassData.CLASSLOOKUP[i][0].CRN+"</div></div>";
             html_view += "<div class='list_holder'><div class='list_holder_left'>Credits: </div><div id='lookupCredits"+i+"' class='list_holder_right'>"+lookupClassData.CLASSLOOKUP[i][0].CREDIT_HOURS+"</div></div>";
             html_view += "<div class='list_holder'><div class='list_holder_left'>Instructor: </div><div id='lookupInstructor"+i+"' class='list_holder_right'>"+lookupClassData.CLASSLOOKUP[i][0].INSTRUCTOR+"</div></div>";
            
             if(lookupClassData.CLASSLOOKUP[i][0].TIME.length > 8) {
                 
                var time = lookupClassData.CLASSLOOKUP[i][0].TIME.split(" ");
                var subTime = time[1].split("-");
                
                 var T1=getFormattedTime(subTime[0]);
                 var T2=getFormattedTime(subTime[1]);
                 
                html_view += "<div class='list_holder'><div class='list_holder_left'>Day/Time: </div><div id='lookupTime"+i+"' class='list_holder_right'>"+T1+" - "+T2+"</div></div>";
                 
             } else {
               html_view += "<div class='list_holder'><div class='list_holder_left'>Day/Time: </div><div id='lookupTime"+i+"' class='list_holder_right'>To Be Announced</div></div>";
             }
            
            html_view += "<div class='list_holder'><div class='list_holder_left'>Location: </div><div id='lookupInstructor"+i+"' class='list_holder_right'>"+lookupClassData.CLASSLOOKUP[i][0].LOCATION+"</div></div>";
            
            if(lookupClassData.CLASSLOOKUP[i][0].FEE == 0){}else{
            html_view += "<div class='list_holder'><div class='list_holder_left'>Fee: </div><div id='lookupInstructor"+i+"' class='list_holder_right'>$"+lookupClassData.CLASSLOOKUP[i][0].FEE+"</div></div>";
            }
            
            html_view += "<div class='list_holder'><div class='list_holder_left'>Duration: </div><div id='lookupInstructor"+i+"' class='list_holder_right'>"+lookupClassData.CLASSLOOKUP[i][0].DURATION+"</div></div>";
            
            html_view += "<div class='list_holder'><div class='list_holder_left'>Capacity: </div><div id='lookupInstructor"+i+"' class='list_holder_right'>"+lookupClassData.CLASSLOOKUP[i][0].CAPACITY+"</div></div>";
            
            html_view += "<div class='list_holder'><div class='list_holder_left'>Remaining: </div><div id='lookupInstructor"+i+"' class='list_holder_right'>"+lookupClassData.CLASSLOOKUP[i][0].REMAINING+"</div></div>";
            
        
            
             
             html_view += "<div class='list_holder' style='border-bottom-left-radius:5px;border-bottom-right-radius:5px;'><div class='week_day_look'><div class='week_day' style='border-left:#ffffff solid 1px;border-top-left-radius:4px;border-bottom-left-radius:4px;' id='monday"+i+"'>Mon</div> <div class='week_day' id='tuesday"+i+"'>Tue</div> <div class='week_day' id='wednesday"+i+"'>Wed</div> <div class='week_day' id='thursday"+i+"'>Thu</div> <div class='week_day' id='friday"+i+"'>Fri</div> <div class='week_day' id='saturday "+i+"'>Sat</div> <div class='week_day' style='border-top-right-radius:4px;border-bottom-right-radius:4px;' id='sunday"+i+"'>Sun</div> </div></div></div>";
            
             html_view += "<div class='reg-update-holder'><div class='list_holder_left' style='font-weight:normal;font-size:11px;color:#000000'><div class='week_day_icon_dark'></div>Class Available<br><div class='week_day_icon_white'></div>Class Unavailable</div><div class='edit_btn right' data-role='button' data-theme='f' id='"+i+"' onclick='registerLUCStudentST(this.id)'>Register</div></div>";
      }
      }
    } else {
        html_view += "<div style='font-size:16px;font-weight:bold;text-align:center;text-shadow:none;margin-top:30%'>No classes could be found for selected term code</div>";
    }
    
    if(html_view == "") { 
        html_view += "<div style='font-size:16px;font-weight:bold;text-align:center;text-shadow:none;margin-top:30%'>No classes available for registration</div>";
    }    
    
    $("#stdLookUpClassesDetails").html(html_view);
    $.mobile.changePage("index.html#stdLookUpClassesPage",{transition:"none"});
    $('#stdLookUpClassesPage').trigger('create');
    stopActivitySpinner(); 
    
    
    for(var i=0; i<lookupClassData.CLASSLOOKUP.length; i++) {
      
      if(lookupClassData.CLASSLOOKUP[i][0].TIME.length >= 3) {
        
          var time = lookupClassData.CLASSLOOKUP[i][0].TIME.split(" ");
          for(var j=0; j<time[0].length; j++) {
              
              switch (time[0][j]) {
                  case "M":{                    
                      $("#monday"+i).removeClass('week_day');
                      $("#monday"+i).addClass('week_day_bg_color');
                      break;
                  }
                  case "T":{                   
                      $("#tuesday"+i).removeClass('week_day');                      
                      $("#tuesday"+i).addClass('week_day_bg_color');                      
                      break;
                  }
                  case "W":{                    
                      $("#wednesday"+i).removeClass('week_day');
                      $("#wednesday"+i).addClass('week_day_bg_color');
                      break;
                  }
                  case "R":{                   
                      $("#thursday"+i).removeClass('week_day');
                      $("#thursday"+i).addClass('week_day_bg_color');
                      break;
                  }
                  case "F":{                    
                      $("#friday"+i).removeClass('week_day');
                      $("#friday"+i).addClass('week_day_bg_color');
                      break;
                  }
                  case "S":{                    
                      $("#saturday"+i).removeClass('week_day');
                      $("#saturday"+i).addClass('week_day_bg_color');
                      break;
                  }
                  case "U":{                   
                      $("#sunday"+i).removeClass('week_day');
                      $("#sunday"+i).addClass('week_day_bg_color');
                      break;
                  }
              }
          }
       }
    }
}


/**
 * <dispCurrentClasses>
 * <display all current classes assigned to particular student>
 * @param {json array} json array of current classes details
 * @return {none}
 */

function dispCurrentClasses(data) {
    $("#stdCurrentClassesDetails").empty();
    var html_view = "";
    
    if(data.scheduleCourseDetail.length != '0') {
        for(i=0; i<data.scheduleCourseDetail.length; i++) {
            
            var courseTitle = data.scheduleCourseDetail[i].COURSE_TITLE;
            var courseNo = data.scheduleCourseDetail[i].SUBJ_CODE+"-"+data.scheduleCourseDetail[i].COURSE_NUMB;
            var crn = data.scheduleCourseDetail[i].CRN;
            var credits = data.scheduleCourseDetail[i].CREDIT_HR;
            var regCodeDesc = data.scheduleCourseDetail[i].REG_CODE_DESC;
            var regDate = data.scheduleCourseDetail[i].REG_DATE;
            
            html_view +="<div class='look_detail_holder'><div class='head_holder'><div class='look_detail_head_left'>"+courseTitle+"</div><div class='look_detail_head_right'>"+courseNo+"</div></div>"
            html_view += "<div class='list_holder'><div class='list_holder_left'>CRN: </div><div class='list_holder_right'>"+crn+"</div></div>";
            html_view += "<div class='list_holder'><div class='list_holder_left'>Credits: </div><div class='list_holder_right'>"+credits+"</div></div>";
            html_view += "<div class='list_holder'><div class='list_holder_left'>Term: </div><div class='list_holder_right'>"+data.scheduleCourseDetail[i].TERM_DESC+"</div></div>";
            
            if(regCodeDesc == 'Web Drop') {
              html_view += "<div class='list_holder' style='border-bottom-left-radius:5px;border-bottom-right-radius:5px;'><div class='list_holder_left' style='color:red;'>"+regCodeDesc+" on</div><div class='list_holder_right' style='color:red;'>"+regDate+"</div></div></div>";
            } else {
               html_view += "<div class='list_holder' style='border-bottom-left-radius:5px;border-bottom-right-radius:5px;'><div class='list_holder_left' style='color:green;'>"+regCodeDesc+" on</div><div class='list_holder_right' style='color:green;'>"+regDate+"</div></div></div>";
               html_view += "<div class='reg-update-holder' ><div class='delete_btn right' data-role='button' data-theme='r' id="+i+" onclick='dropRegistrationClassesST("+crn+")'>Drop Class</div></div>";
            }
        }
    } else {
        html_view += "<div style='font-size:16px;font-weight:bold;text-align:center;text-shadow:none;margin-top:30%;color:#ffffff;'>No current classes could be found for selected semester.</div>";
    }
    
    if(html_view == "") {      
        html_view += "<div style='font-size:16px;font-weight:bold;text-align:center;text-shadow:none;margin-top:30%;color:#ffffff;'>No current classes could be found for selected semester.</div>";
    }
    
    $("#stdCurrentClassesDetails").html(html_view);
    $.mobile.changePage("index.html#stdCurrentClassesPage",{transition:"none"});
    $('#stdCurrentClassesPage').trigger('create');
    stopActivitySpinner();
}


//-------------------- Code for Classroom Availability ------------------//

/**
 * <dispStudentTermCode>
 * <display all term code list>
 * @param {json array} json array of term code list
 * @return {none}
 */

function dispStudentTermCode(data) {
    
    $("#classroomAvailabilityMain").empty();
    //console.log(data.termDropDown[0].TERM_CODE);
    var html_view = "<select id='termCodeListDispOption' data-theme='f'  name='reg' onchange='getClassroomAvailDataTicket(this.value);'>";
    html_view += "<option value=''>Search by Term Code</option>";
    if(data !== undefined) {
        for(i=0; i<data.termDropDown.length; i++) {
            var termCode = data.termDropDown[i].TERM_CODE;
            var termDesc = data.termDropDown[i].TERM_DESC;
            html_view +="<option value='"+termCode+"'>"+termDesc+"</option>";
        }
        
        html_view +="</select>";
    }
    $("#termCodeListDisp").html(html_view);
    
    $.mobile.changePage("index.html#getTermCodePage",{transition:"none"});
    $('#getTermCodePage').trigger('create');
}

/**
 * <showClassroomAvailabilityPage>
 * <display Classroom Availability for particular student>
 * @return {none}
 */

function showClassroomAvailabilityPage(data) {
    
    localStorage.roomAttrArray = "";
    var html_view_page = "";
    html_view_page +="<div style='text-align:center;font-size:16px;color:#ffffff;text-shadow:none;font-weight:bold;margin-top:8px;'>Select the class details</div>";
    html_view_page +="<div style='width:96%;padding:3px 2%;float:left;clear:both;'>";
    html_view_page +="<div id='classroomMeetingCode' class='clsasava_select_holder'></div>";
    html_view_page +="<div id='classroomSiteCode' class='clsasava_select_holder' ></div>";
    html_view_page +="<div id='classroomCampusCode' class='clsasava_select_holder' ></div>";
    html_view_page +="<div id='classroomBuildingCode' class='clsasava_select_holder' style='margin-bottom:5px!important;' ></div>";
    html_view_page +="<div id='classroomWeekday' style='width:260px;margin:0 auto;clear:both;'>";
    html_view_page +="<fieldset data-role='controlgroup' data-type='horizontal' style='width:100%; margin:0; padding:0;text-shadow:none!important;'>";
    html_view_page +="<input type='checkbox' data-theme='f' name='monday' id='monday' /><label for='monday'>M</label>";
    html_view_page +="<input type='checkbox' data-theme='f' name='tuesday' id='tuesday' /><label for='tuesday'>T</label>";
    html_view_page +="<input type='checkbox' data-theme='f' name='wednesday' id='wednesday' /><label for='wednesday'>W</label>";
    html_view_page +="<input type='checkbox' data-theme='f' name='thursday' id='thursday' /><label for='thursday'>T</label>";
    html_view_page +="<input type='checkbox' data-theme='f' name='friday' id='friday' /><label for='friday'>F</label>";
    html_view_page +="</fieldset></div>";
    html_view_page +="</div>";
    html_view_page +="<div style='width:96%;padding:3px 0 3px 4%;float:left;'>";
    html_view_page +="<div class='update-holder' style='font-size:12px;color:#ffffff;text-shadow:none;font-weight:bold;margin-left:5px;margin-bottom:4px;'><div style='float:left;width:33%;margin-left:3px;'>Start Time</div><div style='float:left;width:33%;'>End Time</div><div style='float:left;width:33%;'>Capacity</div></div>";
    html_view_page +="<div style='float:left;width:33%;'><input type='number' style='width:80%;background-color:#ffffff;' name='classroomStartTime' id='classroomStartTime' value='' placeholder='24hr hhmm' /></div>";
    html_view_page +="<div style='float:left;width:33%;'><input type='number' style='width:80%;background-color:#ffffff;' name='classroomEndTime' id='classroomEndTime'  value='' placeholder='24hr hhmm' /></div>";
    html_view_page +="<div style='float:left;width:33%;'><input type='number' style='width:80%;background-color:#ffffff;' name='classroomCapacity' id='classroomCapacity'  value='' placeholder='Capacity' /></div>";
    html_view_page +="<div class='clear'></div>";
    html_view_page +="</div>";
    html_view_page +="<div class='classava_detail_holder'><div class='classava_head_holder'>Desired room attributes</div>";
    html_view_page +="<div id='selectedRoomAttr' class='added_attrib'></div>";
    html_view_page +="<div class='classava_add_attrib' onclick='openAttributeListPopUp()' style='border-bottom-left-radius:5px;border-bottom-right-radius:5px;'>Add an attributes</div>";
    html_view_page +="</div>";
    html_view_page +="<div style='float:left;width:90%;padding:2px 5%;margin:10px 0 30px 0;'>";
    html_view_page +="<div class='update-holder'><div style='text-shadow:none!important;' data-role='button' data-theme='f' onclick='getClassAvailDataTicket()'>Check Classroom Availability</div></div>";
    html_view_page +="</div><div class='clear'></div>";
    
    $("#classroomAvailabilityMain").html(html_view_page);
    
    
    var attributeDropDwnLen = data.roomAttributesDropDown.length;
    
    if(data) {
        
        var html_MeetingCode = "<select data-theme='f' id='meetingCodesDropDownId'><option value='meetingCodesDropDown'>Meeting Codes</option>";
        try {
          for(i=0; i<data.meetingCodesDropDown.length; i++) {
              html_MeetingCode +="<option value='"+data.meetingCodesDropDown[i].MEETING_CODE+"'>"+data.meetingCodesDropDown[i].MEETING_CODE+"</option>";
          }
        } catch(e) {}
        html_MeetingCode +="</select>";
        $("#classroomMeetingCode").html(html_MeetingCode);
        
        var html_siteCode = "<select data-theme='f' id='siteCodesDropDown'><option value=''>Site Codes</option>";
        try {
          for(i=0; i<data.siteCodesDropDown.length; i++) {
              html_siteCode +="<option value='"+data.siteCodesDropDown[i].SITE_CODE+"'>"+data.siteCodesDropDown[i].SITE_DESC+"</option>";
          }
        } catch(e) {}
        html_siteCode +="</select>";
        $("#classroomSiteCode").html(html_siteCode);
        
        var html_campusCode = "<select data-theme='f' id='campusCodesDropDown'><option value=''>Campus</option>";
        try {
          for(i=0; i<data.campusCodesDropDown.length; i++) {
              html_campusCode +="<option value='"+data.campusCodesDropDown[i].CAMPUS_CODE+"'>"+data.campusCodesDropDown[i].CAMPUS_DESC+"</option>";
          }
        } catch(e) {}
        html_campusCode +="</select>";
        $("#classroomCampusCode").html(html_campusCode);
        
        var html_buildingCode = "<select data-theme='f' id='buildingCodesDropDown'><option value=''>Building</option>";
        try {
          for(i=0; i<data.buildingCodesDropDown.length; i++) {
              html_buildingCode +="<option value='"+data.buildingCodesDropDown[i].BUILDING_CODE+"'>"+data.buildingCodesDropDown[i].BUILDING_DESC+"</option>";
          }
        } catch(e) {}
        html_buildingCode +="</select>";
        $("#classroomBuildingCode").html(html_buildingCode);
        
        var html_roomAttributes = "<div data-role='content' class='look_detail_holder_classava'><div class='classava_head_holder' style='text-align:center;'>Select Room Attributes</div><div class='classava_popup_holder'>";
        try {
          for(i=0; i<data.roomAttributesDropDown.length; i++) {
              // html_roomAttributes += "<div class='classava_list_holder'><div class='classava_list_holder_left'><input data-role='none' type='checkbox' class='checkbox_popup' id='roomAttribute"+i+"' value='"+data.roomAttributesDropDown[i].ATTRIBUTE_CODE+"'/><div id='roomAttrDesc"+i+"' class='popup_content'>"+data.roomAttributesDropDown[i].ATTRIBUTE_DESC+"</div></div></div>";
              var roomAttrId = "roomAttribute"+i;
              html_roomAttributes += "<div id='roomAttributeDiv"+i+ "' class='classava_list_holder' onClick='showCheckBox(\""+roomAttrId+"\")'><div class='classava_list_holder_left'><input data-role='none' type='checkbox' class='checkbox_popup' onClick='preventCheckBoxClick(\""+roomAttrId+"\")' id='roomAttribute"+i+"' value='"+data.roomAttributesDropDown[i].ATTRIBUTE_CODE+" '/><div id='roomAttrDesc"+i+"' class='popup_content'>"+data.roomAttributesDropDown[i].ATTRIBUTE_DESC+"</div></div></div>";              
          }
        } catch(e) {}
        html_roomAttributes +="</div><div class='classava_list_holder'><div class='popup_btn' onclick='hideAttributeListPopUp(\""+attributeDropDwnLen+"\")'>Done</div></div></div>";
        $("#AttributeListPopUp").html(html_roomAttributes);
    }
    
    $('#getTermCodePage').trigger('create');   
    stopActivitySpinner();
}


function preventCheckBoxClick(roomAttrId) {
    if($('#'+roomAttrId).is(':checked')) {
        jQuery('#'+roomAttrId).removeAttr('checked');
    } else {
        jQuery('#'+roomAttrId).attr('checked','checked');
    }
}

function showCheckBox(roomAttrId) {
    if($('#'+roomAttrId).is(':checked')) {
        jQuery('#'+roomAttrId).removeAttr('checked');
    } else {
        jQuery('#'+roomAttrId).attr('checked','checked');
    }
}

/**
 * <dispClassroomAvailData>
 * <display Classroom Availe data for particular student>
 * @param {json array} json array of class available data
 * @return {none}
 */

function dispClassroomAvailData(data) {
    
    var html_view = "";
    if(data.availableClassrooms.length != '0') {
        
        for(i=0; i<data.availableClassrooms.length; i++) {
            try {
                if(data.availableClassrooms[i][0].SITE == null){
                    site = "XXXXX";
                } else {
                    site = data.availableClassrooms[i][0].SITE;
                }
                
                html_view +="<div class='look_detail_holder'><div class='head_holder'><div class='look_detail_head_left'>Room: </div><div class='look_detail_head_right'>"+data.availableClassrooms[i][0].ROOM+"</div></div>";
                html_view += "<div class='list_holder'><div class='list_holder_left'>Building: </div><div class='list_holder_right'>"+data.availableClassrooms[i][0].BUILDING+"</div></div>";
                html_view += "<div class='list_holder'><div class='list_holder_left'>Description: </div><div class='list_holder_right' style='overflow:hidden;white-space: nowrap;text-overflow:ellipsis;'>"+data.availableClassrooms[i][0].DESCRIPTION+"</div></div>";
                html_view += "<div class='list_holder'><div class='list_holder_left'>Campus: </div><div class='list_holder_right'>"+data.availableClassrooms[i][0].CAMPUS+"</div></div>";
                html_view += "<div class='list_holder'><div class='list_holder_left'>Site: </div><div class='list_holder_right'>"+site+"</div></div>";
                html_view += "<div class='list_holder' style='border-bottom-left-radius:5px;border-bottom-right-radius:5px;'><div class='list_holder_left'>Capacity: </div><div class='list_holder_right'>"+data.availableClassrooms[i][0].CAPACITY+"</div></div>";
                html_view += "</div>";
                
            } catch(e) {
                
            }
        }
    } else {
        html_view += "<div style='color:#ffffff;font-size:16px;font-weight:bold;text-align:center;text-shadow:none;margin-top:30%'>No class room could be found with those search parameter</div>";
    }
    $("#selectedRoomAttr").html("");
    $("#stdAvailableClassDetails").html(html_view);
    $('#selectedRoomAttr').empty();
    $.mobile.changePage("index.html#stdAvailableClassDetailsPage",{transition:"none"});
}


function openAttributeListPopUp() {
    
    $('#meetingCodesDropDownId').attr('disabled', 'disabled');
    $('#siteCodesDropDown').attr('disabled', 'disabled');
    $('#campusCodesDropDown').attr('disabled', 'disabled');
    $('#buildingCodesDropDown').attr('disabled', 'disabled');
    
    $('html').animate({scrollTop:0});
    $('body').animate({scrollTop:0});
    
    var id = $("#AttributeListPopUp");
    var maskHeight = $(document).height();
    var maskWidth  = $(document).width();
    $("#AttributeListPopUpMask").css({"height":maskHeight,"width":maskWidth});
    $("#AttributeListPopUpMask").fadeIn(100);
    $("#AttributeListPopUpMask").fadeTo("slow",0.5);
    $("#AttributeListPopUpMask").show();
    //$(id).css({'left':'5%'});
    $(id).fadeIn(200);
    
}

function hideAttributeListPopUp(attributeTength) {
    
    $('#meetingCodesDropDownId').removeAttr('disabled');
    $('#siteCodesDropDown').removeAttr('disabled');
    $('#campusCodesDropDown').removeAttr('disabled');
    $('#buildingCodesDropDown').removeAttr('disabled');
    
    $('#selectedRoomAttr').empty();
    var html_string = "";
    localStorage.roomAttrArray = "";
    roomAttrArray = [];
    for(i=0; i<attributeTength; i++) {
        if ($('#roomAttribute'+i).is(":checked")) {
            html_string +="<div><div>"+$('#roomAttrDesc'+i).text()+"</div>";
            roomAttrArray.push($('#roomAttribute'+i).val());
        }
    }
    html_string +="</div>";
    console.log(roomAttrArray);
    if(roomAttrArray.length != '0') {
        localStorage.roomAttrArray=roomAttrArray;
        $("#selectedRoomAttr").html(html_string);
    } else {
        localStorage.roomAttrArray="";
        $("#selectedRoomAttr").html("");
    }
    
    $("#AttributeListPopUp").hide();
    $("#AttributeListPopUpMask").hide();
    setTimeout("listRefreshHelper('getTermCodePage')",100);
}


/******************************************************* Added content for emergeny contacts **********************************************/
/**
 *emergencyContacts();
 * <function Displaying Emergency Contacts>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message
 * @return {JSON}
 */
function emergencyContacts(data)
{
    //console.log(JSON.stringify(data));
    var html_view = "<div data-role='none' class='list-holder'>";
    var oldPriority = data.fetch.length?data.fetch.length:1;
    var priority = oldPriority;
    if(data.fetch.length > 0)
    {
        oldPriority = parseInt(data.fetch[data.fetch.length-1].PRIORITY)+1?parseInt(data.fetch[data.fetch.length-1].PRIORITY)+1:1;
        priority = oldPriority;
        for(var i=0;i<data.fetch.length;i++)
        {
            var relation = encodeURIComponent((data.fetch[i].RELATIONSHIP_CODE==null?'':data.fetch[i].RELATIONSHIP_CODE));
            var lastNameVal = encodeURIComponent((data.fetch[i].LAST_NAME==null?'':data.fetch[i].LAST_NAME));
            var firstNameVal = encodeURIComponent((data.fetch[i].FIRST_NAME==null?'':data.fetch[i].FIRST_NAME));
            var zip = encodeURIComponent((data.fetch[i].ZIP==null?'':data.fetch[i].ZIP));
            var state = encodeURIComponent((data.fetch[i].STATE_CODE==null?'':data.fetch[i].STATE_CODE));
            var priorityCode = encodeURIComponent((data.fetch[i].PRIORITY==null?'':data.fetch[i].PRIORITY));
            //var oldPriorityCode = data.fetch[i].RELATIONSHIP_CODE;
            var sur = encodeURIComponent((data.fetch[i].SURNAME_PREFIX==null?'':data.fetch[i].SURNAME_PREFIX));
            var middleNameVal = encodeURIComponent((data.fetch[i].MIDDLE_NAME==null?'':data.fetch[i].MIDDLE_NAME));
            var houseNumberVal = encodeURIComponent((data.fetch[i].HOUSE_NUMBER==null?'':data.fetch[i].HOUSE_NUMBER));
            var street1 = encodeURIComponent((data.fetch[i].STREET_LINE1==null?'':data.fetch[i].STREET_LINE1));
            var street2 = encodeURIComponent((data.fetch[i].STREET_LINE2==null?'':data.fetch[i].STREET_LINE2));
            var street3 = encodeURIComponent((data.fetch[i].STREET_LINE3==null?'':data.fetch[i].STREET_LINE3));
            var address = encodeURIComponent((data.fetch[i].STREET_LINE4==null?'':data.fetch[i].STREET_LINE4));
            var cityVal = encodeURIComponent((data.fetch[i].CITY==null?'':data.fetch[i].CITY));
            var country = encodeURIComponent((data.fetch[i].COUNTRY_CODE==null?'':data.fetch[i].COUNTRY_CODE));
            var countryPhone = encodeURIComponent((data.fetch[i].PHONE_COUNTRY_CODE==null?'':data.fetch[i].PHONE_COUNTRY_CODE));
            var areaPhone = encodeURIComponent((data.fetch[i].PHONE_AREA==null?'':data.fetch[i].PHONE_AREA));
            var phone = encodeURIComponent((data.fetch[i].PHONE_NUMBER==null?'':data.fetch[i].PHONE_NUMBER));
            var extension = encodeURIComponent((data.fetch[i].PHONE_EXTN==null?'':data.fetch[i].PHONE_EXTN));
            var getNumber = ((data.fetch[i].PHONE_COUNTRY_CODE==null)?'':data.fetch[i].PHONE_COUNTRY_CODE)+((data.fetch[i].PHONE_AREA==null)?'':data.fetch[i].PHONE_AREA)+((data.fetch[i].PHONE_NUMBER==null)?'':data.fetch[i].PHONE_NUMBER)+((data.fetch[i].PHONE_EXTN==null)?'':data.fetch[i].PHONE_EXTN);
            
            
            html_view += "<div class='emergency_container'>";
            html_view += "<div class='emergency_container_first'><div class='contact-head-name'><div class='emergency_name'>"+data.fetch[i].FIRST_NAME+"&nbsp;"+data.fetch[i].LAST_NAME+"</div></div><div class='contact-action-btns' ><div data-role='button' data-inline='true' data-iconpos='notext' data-icon='alert' data-theme='a' onclick='addNewContactView();getContactCodesJSON();updateContactView(\""+relation+"\",\""+lastNameVal+"\",\""+firstNameVal+"\",\""+zip+"\",\""+state+"\",\""+priorityCode+"\",\""+sur+"\",\""+middleNameVal+"\",\""+houseNumberVal+"\",\""+street1+"\",\""+street2+"\",\""+street3+"\",\""+address+"\",\""+cityVal+"\",\""+country+"\",\""+countryPhone+"\",\""+areaPhone+"\",\""+phone+"\",\""+extension+"\");'></div><div data-icon='delete' data-theme='r' data-iconpos='notext' data-role='button' data-inline='true' onclick='emergencyContactRemove(\""+data.fetch[i].PRIORITY+"\")' ><input type='hidden' value="+data.fetch[i].PRIORITY+" /></div></div></div>";
            html_view += "<div class='emergency_container_second'><div class='emergency_left'>Relationship:</div><div class='emergency_right'>"+getEmegencyCodeName(null,null,data.fetch[i].RELATIONSHIP_CODE)+"</div></div>";
            
           // html_view += "<div class='emergency_container_second'><div class='emergency_left'>Address:</div><div class='emergency_right'><div style='float:left;line-height:22px;width:100%;'>"+((data.fetch[i].STREET_LINE1 == null)?'':data.fetch[i].STREET_LINE1)+" "+((data.fetch[i].STREET_LINE4 == null)?'':data.fetch[i].STREET_LINE4)+" "+((data.fetch[i].CITY == null)?'':data.fetch[i].CITY)+" "+((data.fetch[i].ZIP == null)?'':data.fetch[i].ZIP)+"</div><div style='float:left;width:100%;'>"+(data.fetch[i].STATE_CODE==null?'':getEmegencyCodeName(null,data.fetch[i].STATE_CODE,null)+'<br />')+getEmegencyCodeName(data.fetch[i].COUNTRY_CODE,null,null)+"</div></div></div>";
            
            
            html_view += "<div class='emergency_container_second'><div class='emergency_left'>Address:</div><div class='emergency_right'><div style='float:left;line-height:22px;width:100%;'>"+((data.fetch[i].STREET_LINE1 == null)?'':data.fetch[i].STREET_LINE1)+"<div>"+((data.fetch[i].STREET_LINE2 == null)?'':data.fetch[i].STREET_LINE2)+"</div><div>"+((data.fetch[i].STREET_LINE3 == null)?'':data.fetch[i].STREET_LINE3)+"</div><div>"+((data.fetch[i].CITY == null)?'':data.fetch[i].CITY)+((data.fetch[i].STATE_CODE == null)?'':', '+getEmegencyCodeName(null,data.fetch[i].STATE_CODE,null))+((data.fetch[i].ZIP == null)?'':' '+data.fetch[i].ZIP)+"</div></div></div>";
            
            
            if(getNumber!='')
            {
               
                html_view += "<div class='emergency_container_second' style='border-bottom-left-radius:5px;border-bottom-right-radius:5px;'>";
                html_view +=  "<a href='tel:"+((data.fetch[i].PHONE_COUNTRY_CODE==null)?'':data.fetch[i].PHONE_COUNTRY_CODE)+((data.fetch[i].PHONE_AREA==null)?'':data.fetch[i].PHONE_AREA)+((data.fetch[i].PHONE_NUMBER==null)?'':data.fetch[i].PHONE_NUMBER)+"' class='emergency_link' style='color:white !important;'><div class='emergency_left '><div class='call_holder'>Phone:</div></div>";
                
                var a = "+"+data.fetch[i].PHONE_COUNTRY_CODE;
                
                
                html_view += "<div class='emergency_right' ><div class='call_holder'><div class='call_button'></div>"+((data.fetch[i].PHONE_COUNTRY_CODE==null)?'':a)+" "+((data.fetch[i].PHONE_AREA==null)?'':('('+data.fetch[i].PHONE_AREA+')'))+" "+((data.fetch[i].PHONE_NUMBER==null)?'':data.fetch[i].PHONE_NUMBER)+" "+((data.fetch[i].PHONE_EXTN==null)?'':('x'+data.fetch[i].PHONE_EXTN))+"</div></div></a></div></div>";
            }
            
            else{
               // html_view += "<div class='emergency_container_second'><div class='emergency_left'>Address:</div><div class='emergency_right'><div style='float:left;line-height:22px;width:100%;'>"+((data.fetch[i].STREET_LINE1 == null)?'':data.fetch[i].STREET_LINE1)+" "+((data.fetch[i].STREET_LINE4 == null)?'':data.fetch[i].STREET_LINE4)+" "+((data.fetch[i].CITY == null)?'':data.fetch[i].CITY)+" "+((data.fetch[i].ZIP == null)?'':data.fetch[i].ZIP)+"</div><div style='float:left;width:100%;'>"+(data.fetch[i].STATE_CODE==null?'':getEmegencyCodeName(null,data.fetch[i].STATE_CODE,null)+'<br />')+getEmegencyCodeName(data.fetch[i].COUNTRY_CODE,null,null)+"</div></div></div>";
                
                //html_view +="</div>";
            }
            
            
            html_view += "</div>"
            html_view +="<div class='cb10'></div>";
            
        }
    }
    
    html_view += "<div><input type='hidden' id='set_priority' value='"+priority+"' /></div><div><input type='hidden' id='set_old_priority' value='"+oldPriority+"' /><input type='hidden' id='remove_priority' /></div>";
    html_view += "<div class='add_new_holder'><a href='javascript:void(0)' onclick='startActivitySpinner();addNewContactView();getContactCodesJSON();'  data-role='button' data-theme='f' style='text-shadow:none!important;' data-icon='plus' data-iconpos='right'>Add New Contact</a></div>";
    html_view += "</div>";
    document.getElementById('emergency_contacts_Display').innerHTML = html_view;
    stopActivitySpinner();
    $.mobile.changePage('index.html#emergency_contacts',{transition:"none"});
    setTimeout('listRefreshHelper("emergency_contacts")', 100);
    $('#emergency_contacts').trigger('create');
    //backToHome();  //add a event listener to redirect back to home
}

/**
 *To get emergency contacts codes getContactCodesJSON
 */
function emergencycontactcodes()
{
    url_service = "/enmu/services/student/banneremergencycontactcodes";
    return_Where = getEmergencyContactCodes;
    request_ST(url_service, return_Where, false);
}

/**
 *Display to insert a new contact
 */
function addNewContactView()
{
    //startActivitySpinner();
    // In address update chage title before change page
    $("#emergency_contacts_title").html("");
    
    var priority = $('#set_priority').val();
    var oldPriority = $('#set_old_priority').val();
    
    var html_view ="<div data-role='' style='color:#ffffff;float:left;width:100% !important;text-shadow:none !important;'>";
    html_view += "<div style='float:left;width:90%;padding:10px 5%;'>";
    html_view += "<div class='update-holder'>Relationship<span class='red'>*</span></div>";
    html_view += "<div class='update-holder'><input type='hidden' type='text' placeholder='Order' id='order' /><select id='relation' data-theme='f'><option>Select</option></select></div>";
    html_view += "<div class='update-holder'><input data-theme='c' type='hidden' placeholder='priority' id='priority' value='"+priority+"' /></div>";
    html_view += "<div class='update-holder'><input data-theme='c' type='hidden' placeholder='old priority' id='old_priority' value='"+oldPriority+"' /></div>";
    html_view += "<fieldset style='border:1px solid #ffffff;float:left;width:100%;'><legend style='font-weight:bold;font-size:14px;color:#ffffff'>Contact Details</legend>";
    html_view += "<div class='additional_detail_hldr1'><div class='update-holder'>First Name<span class='red'>*</span></div>";
    html_view += "<div class='update-holder'><input data-theme='c' type='text' placeholder='First Name' id='first_name' /></div>";
    html_view += "<div class='update-holder'>Middle Name</div>";
    html_view += "<div class='update-holder'><input data-theme='c' type='text' placeholder='Middle Name' id='middle_name' /></div>";
    html_view += "<div class='update-holder'>Last Name<span class='red'>*</span></div>";
    html_view += "<div class='update-holder'><input data-theme='c' type='text' placeholder='Last Name' id='last_name' /></div>";
    
    //html_view += "<div class='update-holder'><div style='font-weight:bold;width:40%;float:left;'>House Number</div><div style='font-weight:bold;width:40%;float:right;'>Zip Code<span class='red' id='zipEmergencyContactMandatoryStar' style='display:none'>*</span></div></div>";
    //html_view +="<div class='update-holder'><span style='display:block;float:left;width:40%;'><input data-theme='c' type='text' placeholder='House Number' id='house_number' /></span><span style='display:block;float:right;width:40%;'><input data-theme='c' MaxLength='9' type='text'   placeholder='Zip Code' id='zip' /></span></div>";
    
    html_view += "<div class='update-holder'>Address</div>";
    html_view += "<div class='update-holder'><input data-theme='c' type='text' placeholder='Address Line 1' id='street1' /></div>";
    html_view += "<div class='update-holder'><input data-theme='c' type='text' placeholder='Address Line 2' id='street2' /></div>";
    html_view += "<div class='update-holder'><input data-theme='c' type='text' placeholder='Address Line 3' id='street3' /></div>";
    //html_view += "<div class='update-holder'>Address</div>";
    //html_view += "<div class='update-holder'><textarea data-theme='c' rows='4' cols='40' type='text' placeholder='Address' id='address'></textarea></div>";
    //html_view += "<div class='update-holder'>Country<span class='red'>*</span></div>";
    //html_view += "<div class='update-holder' style='-webkit-border-radius:8px; overflow:hidden!important'><select  data-theme='c' id='country' onchange='disableFieldsContacts()'  ><option>Select</option></select></div>";
    //html_view += "<div class='emergency_contact_state_showhide'><div class='update-holder'>State</div>";
    html_view += "<div><div class='update-holder'>State</div>";
    html_view += "<div class='update-holder' style='-webkit-border-radius:8px; overflow:hidden!important'><select  id='state' onchange='showHideMandatoryEmergency(this.value)' data-theme='c' ><option>Select</option></select></div></div>";
    
    
    //html_view += "<div class='update-holder'><div style='font-weight:bold;width:40%;float:left;'>City</div><div style='font-weight:bold;width:40%;float:right;'>Zip Code<span class='red' id='zipEmergencyContactMandatoryStar' style='display:none'>*</span></div></div>";
    
    html_view += "<div class='update-holder'><div style='font-weight:bold;width:40%;float:left;'>City</div><div style='font-weight:bold;width:40%;float:right;'>Zip Code</div></div>";
    
    
    html_view +="<div class='update-holder'><span style='display:block;float:left;width:40%;'><input data-theme='c' type='text' placeholder='City' id='city' /></span><span style='display:block;float:right;width:40%;'><input data-theme='c' MaxLength='10' type='text'   placeholder='Zip Code' id='zip' /></span></div>";
    
    
    
    
    
    //html_view += "<div class='update-holder'>City</div>";
    //html_view += "<div class='update-holder'><input type='text' placeholder='City' id='city' data-theme='c' /></div></div></fieldset>";
    
    html_view += "<fieldset style='border:1px solid #ffffff;float:left;width:100%;margin-top:12px'><legend style='font-weight:bold;font-size:14px;color:#ffffff'>Contact Phone</legend>";
    html_view += "<div class='additional_detail_hldr1'><div class='update-holder'>Phone Number<span class='red'>*</span></div>";
    
    //html_view += "<div class='update-holder'><input data-theme='c'  type='tel' placeholder='Country' id='country_phone' MaxLength='3' style='float:left;width:18% !important;' /><input type='tel'  placeholder='Area' data-theme='c' id='area_phone' MaxLength='5'  style='float:left;width:18% !important;margin-left:3px;' /><input data-theme='c' type='tel' MaxLength='10' placeholder='Phone' id='phone' style='float:left;width:23% !important;margin-left:3px;'/> <input type='tel' data-theme='c' placeholder='Ext' id='extension' MaxLength='4'  style='float:left;width:10% !important;margin-left:3px;'/></div></div></fieldset>";
    
    html_view += "<div class='update-holder'><input type='tel'  placeholder='Area' data-theme='c' id='area_phone' MaxLength='3'  style='float:left;width:18% !important;margin-left:3px;' /><input data-theme='c' type='tel' MaxLength='7' placeholder='Phone' id='phone' style='float:left;width:23% !important;margin-left:3px;'/> <input type='tel' data-theme='c' placeholder='Ext' id='extension' MaxLength='4'  style='float:left;width:10% !important;margin-left:3px;'/></div></div></fieldset>";
    
    // html_view += "<div style='width:100%;float:left;' style='font-size:10px;'><div style='float:left;width:10% !important;margin-left:5px;'>Country</div><div style='float:left;width:18% !important;margin-left:34px;'>Area</div><div style='float:left;width:18% !important;'>Phone</div><div style='float:left;width:35% !important;margin-left:15px;text-align:center;'>Extension</div></div>";
    
    html_view += "<div style='float:left;width:100%;margin:20px 0;'><div class='add_details_btn' data-role='button' data-theme='f' value='add' onclick='emergencyContactSave();' ><span id='insert'>Save Contact</span></div><div class='reset_details_btn' data-role='button' data-theme='a' onclick='resetDetail();getContactCodesJSON();' >Reset</div></div>";
    html_view += "</div>";
    html_view += "</div>";
    document.getElementById('save_emergency_contacts').innerHTML = html_view;
    $("#emergency_contacts_title").html("Add New Contact");
    stopActivitySpinner();
    //$('.emergency_contact_state_showhide').show();// to hide state
    $.mobile.changePage('index.html#set_emergency_contacts',{transition:"none"});
    $('#set_emergency_contacts').trigger('create');
}


/**
 *To save/update emergency contacts
 */
function emergencyContactSave()
{
    startActivitySpinner();
    url_service = "/enmu/services/student/banneremergencycontactsave";
    return_Where = setEmergencyContact;
    request_ST(url_service, return_Where, false);
}

/**
 *To remove emergency contact
 */
function emergencyContactRemove(data)
{
    var removePriority = data;
    $('#remove_priority').val(removePriority);
    navigator.notification.confirm('Are you sure you want to delete this contact?', confirmCall, 'Emergency Contact',['Yes','No']);
    function confirmCall(button)
    {
        if(button==1)
        {
            startActivitySpinner();
            url_service = "/enmu/services/student/banneremergencycontactdelete";
            return_Where = deleteEmergencyContact;
            request_ST(url_service, return_Where, false);
        }
        else
        {
            return false; //do nothing
        }
    }
}


/**
 *To update Contact view
 */
function updateContactView(relation, lastNameVal, firstNameVal, zip, state, priorityCode, sur, middleNameVal, houseNumberVal, street1, street2, street3, address, cityVal, country, countryPhone, areaPhone, phone, extension)
{
    $("#emergency_contacts_title").html("Update Contact"); 
    startActivitySpinner();
    var relation = decodeURIComponent(relation);
    var lastNameVal = decodeURIComponent(lastNameVal);
    var firstNameVal = decodeURIComponent(firstNameVal);
    var zip = decodeURIComponent(zip);
    var state = decodeURIComponent(state);
    var priorityCode = decodeURIComponent(priorityCode);
    //var oldPriorityCode = decodeURIComponent(data.fetch[i].RELATIONSHIP_CODE);
    var sur = decodeURIComponent(sur);
    var middleNameVal = decodeURIComponent(middleNameVal);
    var houseNumberVal = decodeURIComponent(houseNumberVal);
    var street1 = decodeURIComponent(street1);
    var street2 = decodeURIComponent(street2);
    var street3 = decodeURIComponent(street3);
    var address = decodeURIComponent(address);
    var cityVal = decodeURIComponent(cityVal);
    var country = decodeURIComponent(country);
    var countryPhone = decodeURIComponent(countryPhone);
    var areaPhone = decodeURIComponent(areaPhone);
    var phone = decodeURIComponent(phone);
    var extension = decodeURIComponent(extension);
    
    $('#relation').val(relation);$('#relation').selectmenu("refresh");
    $('#last_name').val(lastNameVal);
    $('#first_name').val(firstNameVal);
    $('#zip').val(zip);
    $('#state').val(state);$('#state').selectmenu("refresh");
    $('#priority').val(priorityCode);
    $('#old_priority').val(priorityCode); // Set to update same content
    $('#sur').val(((sur=='null')?'':sur));
    $('#middle_name').val(((middleNameVal=='null')?'':middleNameVal));
    $('#house_number').val(((houseNumberVal=='null')?'':houseNumberVal));
    $('#street1').val(((street1=='null')?'':street1));
    $('#street2').val(((street2=='null')?'':street2));
    $('#street3').val(((street3=='null')?'':street3));
    $('#address').val(((address=='null')?'':address));
    $('#city').val(((cityVal=='null')?'':cityVal));
    $('#country').val(country);$('#country').selectmenu("refresh");
    $('#country_phone').val(((countryPhone=='null')?'':countryPhone));
    $('#area_phone').val(((areaPhone=='null')?'':areaPhone));
    $('#phone').val(((phone=='null')?'':phone));
    $('#extension').val(((extension=='null')?'':extension));
    $('#insert').text("Update");
    $('#insert').val("update");
    disableFieldsContacts('one');
    showHideMandatoryEmergency(state);
    stopActivitySpinner();
}

/**
 *Reset values of contacts
 */
function resetDetail()
{
    var header = "";
    $('#relation').val(""); $('#relation').selectmenu("refresh");
    $('#last_name').val("");
    $('#first_name').val("");
    $('#zip').val("");
    $('#state').val("Select"); $('#state').selectmenu("refresh");
    //$('#priority').val("");
    // $('#old_priority').val("");
    $('#sur').val("");
    $('#middle_name').val("");
    $('#house_number').val("");
    $('#street1').val("");
    $('#street2').val("");
    $('#street3').val("");
    $('#address').val("");
    $('#city').val("");
    $('#country').val(""); $('#country').selectmenu("refresh");
    $('#country_phone').val("");
    $('#area_phone').val("");
    $('#phone').val("");
    $('#extension').val("");
    if($('#insert').val() == "add")
    {
        $("#emergency_contacts_title").html("Add New Contact");
    }
    else
    {
        $("#emergency_contacts_title").html("Update Contact");
    }
}

/**
 *Function to disable the country and zip incase country US for Emergency contact
 */
function disableFieldsContacts(str)
{
    if(str == 'one')
    {
        if($('#country').val() != countryUSA)
        {
            //$('#zip').val('');
            //$('#zip').attr('disabled',true);
            $('.emergency_contact_state_showhide').hide();
            $('#state').val('Select');
            //$('#state').attr('disabled',true);$('#state').selectmenu('refresh');
            $('#state').val('');
            $('#zipEmergencyContactMandatoryStar').hide();
            
        }
        else
        {
            //do nothing
            $('.emergency_contact_state_showhide').show();
        }
    }
    else if($('#country').val() != countryUSA)
    {
        //$('#zip').val('');
        //$('#zip').attr('disabled',true);
        $('.emergency_contact_state_showhide').hide();
        $('#state').val('Select');
        //$('#state').attr('disabled',true);$('#state').selectmenu('refresh');
        $('#state').val('');
        $('#zipEmergencyContactMandatoryStar').hide();
        
    }
    else
    {
        //$('#zip').val();
        //$('#zip').attr('disabled',false);
        $('#state').val('Select');$('#state').attr('disabled',false);$('#state').selectmenu('refresh');
        $('#state').val('Select');
        $('.emergency_contact_state_showhide').show();
    }
}

/*******************************************************end**********************************************/

/**
 *Function for showing red mandatory mark on state selection either it is hide
 */
function showHideMandatoryEmergency(param)
{
    /*var zipCode = $('#zip').val();
    if(param && param != 'Select'){
        if(!zipCode){
           // navigator.notification.alert('Please enter Zip',null,'Emergency Contact');
        }
        $('#zipEmergencyContactMandatoryStar').show();
    }
    else if($('#country').val() != countryUSA){
        $('#zipEmergencyContactMandatoryStar').hide();
    }
    else{
        $('#zipEmergencyContactMandatoryStar').hide();
    }*/
}

/********************************************Added content for address***********************************/

var Clickededitbutton=false;
function callEditContactAddress(i)
{
    //console.log(i);
    Clickededitbutton=true;
    if(i || i ==0) $(".my_edit_hidden_"+i).trigger("click");
}


/**
 * To fetch address
 *
 */
function addressDisplay(data)
{
    //console.log("addressDisplay --> "+JSON.stringify(data));
    
    /** To save all address type in local to check against no same addresses insert or update **/
    var addtypeArr = {};
    for(var i=0;i<data.addresses.length;i++)
    {
        addtypeArr[data.addresses[i].ADDRESS_TYPE_CODE] = data.addresses[i].TO_DATE;
    }
    
    window.localStorage.setItem("CHECK_ADDRESS_TYPE",JSON.stringify(addtypeArr));
    /**  ------------------------------------------------ **/
    
    var html_view = "<div data-role='none' class='list-holder'>";
    for(var i=0;i<data.addresses.length;i++)
    {
        var phoneCountryCode="", phoneAreaCode="", phoneNumber="", extension="", internationalAccessCode="", unlistedInd="", primaryPhoneSeq="", primaryPhoneCode="", additionalTeleCodes = [], additionalCountryCodes = [], additionalareaCodes = [], additonalPhoneNumbers = [], additionalExtensions = [], additionalIntAccess = [], additionalUnlistedInd = [], additionalSequence = [], addPhoneLength, countLength, addPhoneLengthTemp;
        
        var check_null = function(str)
        {
          if(!str || str == "null") return "";
          else return str;
        }
        
        
        var addressTypeCode = encodeURIComponent(check_null(data.addresses[i].ADDRESS_TYPE_CODE));
        var sequence = encodeURIComponent(check_null(data.addresses[i].ADDRESS_SEQ));
        var fromDate = encodeURIComponent(check_null(data.addresses[i].FROM_DATE));
        var toDate = encodeURIComponent(check_null(data.addresses[i].TO_DATE));
        var houseNumber = encodeURIComponent(check_null(data.addresses[i].HOUSE_NUMBER));
        var street1 = encodeURIComponent(check_null(data.addresses[i].STREET_LINE1));
        var street2 = encodeURIComponent(check_null(data.addresses[i].STREET_LINE2));
        var street3 = encodeURIComponent(check_null(data.addresses[i].STREET_LINE3));
        var street4 = encodeURIComponent(check_null(data.addresses[i].STREET_LINE4));
        var city = encodeURIComponent(check_null(data.addresses[i].CITY));
        var stateCode = encodeURIComponent(check_null(data.addresses[i].STATE_CODE));
        var zip = encodeURIComponent(check_null(data.addresses[i].ZIP));
        var countyCode = encodeURIComponent(check_null(data.addresses[i].COUNTY_CODE));
        var nationCode = encodeURIComponent(check_null(data.addresses[i].NATION_CODE));
        

        
        
        // var additionalDelete = $('#addDelete').val();
        
        
        
        
        
        
        //first block
       
        
        html_view += "<div class='emergency_container emergency_container_"+i+"'><div class='emergency_container_inside ui-bar-f'>";
        //html_view += "<input type='hidden' id='getSeq'><input type='hidden' id='getATCode'>";
        html_view += "<div class='emergency_address_first' style='text-align:left; font-size:16px; margin-left:10px; width:67%'>"+getAddressCodeName(null,null,null,null,data.addresses[i].ADDRESS_TYPE_CODE)+"</div>";
        
         html_view += "<div class='contact-action-btns' ><div data-inline='true' data-iconpos='notext' data-icon='alert' data-theme='a' data-role='button' onclick='callEditContactAddress("+i+")'></div><div data-role='button' data-inline='true' data-iconpos='notext' data-icon='delete' data-theme='r' onclick='addressRemove(\""+sequence+"\",\""+addressTypeCode+"\")' ></div></div>";
        
        html_view +="</div>";
         
        //html_view += "<div class='emergency_address_second' >"+(data.addresses[i].HOUSE_NUMBER==null?'':data.addresses[i].HOUSE_NUMBER)+"  "+(data.addresses[i].STREET_LINE4==null?'':data.addresses[i].STREET_LINE4)+" "+(data.addresses[i].CITY==null?'':data.addresses[i].CITY);
        
        
       // html_view += "<div class='emergency_container_second'><div class='emergency_left'>Address:</div><div class='emergency_right'><div style='float:left;line-height:16px;width:100%;'>"+check_null(data.addresses[i].HOUSE_NUMBER)+"  "+check_null(data.addresses[i].STREET_LINE1)+" "+check_null(data.addresses[i].CITY)+"</div><div style='float:left;line-height:16px;width:100%;'>"+(data.addresses[i].STATE_CODE==null?'':getAddressCodeName(null,null,null,data.addresses[i].STATE_CODE,null))+" "+(data.addresses[i].ZIP==null?'':data.addresses[i].ZIP+'<br />')+" "+(data.addresses[i].COUNTY_CODE==null?'':getAddressCodeName(data.addresses[i].COUNTY_CODE,null,null,null,null)+'<br />')+(data.addresses[i].NATION_CODE==null?'':getAddressCodeName(null,null,data.addresses[i].NATION_CODE,null,null))+"</div></div></div>";
        
        if(data.addresses[i].ZIP == null){
           var addZip="";
        }else{
            if(data.addresses[i].ZIP.length == 9)
            {
               var addZip=data.addresses[i].ZIP.substr(0,3)+"-"+data.addresses[i].ZIP.substr(3);
            }
            else{
               var addZip=data.addresses[i].ZIP;
            }
        }
        
        try{   html_view += "<div class='emergency_container_second'><div class='emergency_left'>Address:</div><div class='emergency_right'><div style='float:left;line-height:16px;width:100%;'>"+check_null(data.addresses[i].HOUSE_NUMBER)+"  "+check_null(data.addresses[i].STREET_LINE1)+"<div>"+check_null(data.addresses[i].STREET_LINE2)+"</div>"+check_null(data.addresses[i].STREET_LINE3)+"</div>"+check_null(data.addresses[i].CITY)+(data.addresses[i].STATE_CODE==null?'':', '+getAddressCodeName(null,null,null,data.addresses[i].STATE_CODE,null))+" "+addZip+"</div></div>";
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }catch(e){alert(e)}
        
       //html_view += "<div class='emergency_container_second'><div class='emergency_left'>Valid Date:</div><div class='emergency_right'><div style='float:left;line-height:16px;width:100%;'>"+(data.addresses[i].FROM_DATE==null?'':dateViewFormat(data.addresses[i].FROM_DATE))+"</div></div></div>";
       //html_view += "<div class='emergency_container_second'><div class='emergency_left'>Until Date:</div><div class='emergency_right'><div style='float:left;line-height:16px;width:100%;'>"+(data.addresses[i].TO_DATE==null?'(No until date)':dateViewFormat(data.addresses[i].TO_DATE))+"</div></div></div>";
        
        
        
        
        
        //html_view += "<br />"+(data.addresses[i].STATE_CODE==null?'':getAddressCodeName(null,null,null,data.addresses[i].STATE_CODE,null))+" "+(data.addresses[i].ZIP==null?'':' - '+data.addresses[i].ZIP+'<br />')+" "+(data.addresses[i].COUNTY_CODE==null?'':getAddressCodeName(data.addresses[i].COUNTY_CODE,null,null,null,null)+'<br />')+(data.addresses[i].NATION_CODE==null?'':getAddressCodeName(null,null,data.addresses[i].NATION_CODE,null,null))+"</div>";
        //html_view += "<div class='emergency_address_second' style='border-bottom-left-radius:5px;border-bottom-right-radius:5px;'>"+(data.addresses[i].FROM_DATE==null?'':dateViewFormat(data.addresses[i].FROM_DATE))+"&nbsp;&#8722;&nbsp;"+(data.addresses[i].TO_DATE==null?'(No end Date)':dateViewFormat(data.addresses[i].TO_DATE))+"</div></div>";
        
        for(var j=0;j<data.primaryphones.length;j++)
        {
            
            var additionalTeleCodesTemp = [], additionalCountryCodesTemp =[], additionalareaCodesTemp = [], additonalPhoneNumbersTemp = [], additionalExtensionsTemp = [], additionalIntAccessTemp = [], additionalUnlistedIndTemp = [], additionalSequenceTemp = [];
            
            //second block
            countLength = 1;
            addPhoneLength = countLength;
            //addPhoneLengthTemp = countLength;
            if(data.addresses[i].ADDRESS_SEQ == data.primaryphones[j].ADDRESS_SEQ && data.addresses[i].ADDRESS_TYPE_CODE == data.primaryphones[j].ADDRESS_TYPE_CODE)
            {
                
                phoneCountryCode = encodeURIComponent(check_null(data.primaryphones[j].COUNTRY_CODE));
                phoneAreaCode = encodeURIComponent(check_null(data.primaryphones[j].AREA_CODE));
                phoneNumber = encodeURIComponent(check_null(data.primaryphones[j].PHONE_NUMBER));
                extension = encodeURIComponent(check_null(data.primaryphones[j].EXTENSION));
                internationalAccessCode = encodeURIComponent(check_null(data.primaryphones[j].INTERNATIONAL_ACCESS));
                unlistedInd = encodeURIComponent(check_null(data.primaryphones[j].UNLISTED_IND));
                primaryPhoneSeq = encodeURIComponent(check_null(data.primaryphones[j].PHONE_SEQ));
                primaryPhoneCode = encodeURIComponent(check_null(data.primaryphones[j].PHONE_CODE));
                
                
                html_view += "<div class='emergency_address_first ui-bar-f' style='padding:8px 0; width:100%'>Contact Numbers</div>";
                
                html_view += "<div class='emergency_container_additional'><a class='address_additional' href='tel:"+check_null(data.primaryphones[j].COUNTRY_CODE)+check_null(data.primaryphones[j].AREA_CODE)+check_null(data.primaryphones[j].PHONE_NUMBER)+"' ><div class='emergency_left_additional' >Primary:</div><div class='emergency_right_additional'><div class='call_button'></div>"+((data.primaryphones[j].COUNTRY_CODE==null)?'':('+'+data.primaryphones[j].COUNTRY_CODE))+((data.primaryphones[j].AREA_CODE==null)?'':(' ('+data.primaryphones[j].AREA_CODE+')'))+((data.primaryphones[j].PHONE_NUMBER==null)?'':(' '+data.primaryphones[j].PHONE_NUMBER))+((data.primaryphones[j].EXTENSION==null)?'':(' x'+data.primaryphones[j].EXTENSION))+"</div></a></div>";
                
                
                for(var k=0;k<data.additionalphones.length;k++)
                {
                    
                    /** to edit and remove additional phone **/
                    //alert(data.primaryphones[j].ADDRESS_SEQ+" "+data.additionalphones[k].ADDRESS_SEQ+" "+data.primaryphones[j].ADDRESS_TYPE_CODE+" "+additionalphones[k].ADDRESS_TYPE_CODE);
                    
                    if(data.primaryphones[j].ADDRESS_SEQ == data.additionalphones[k].ADDRESS_SEQ && data.primaryphones[j].ADDRESS_TYPE_CODE == data.additionalphones[k].ADDRESS_TYPE_CODE)
                    {
                        //addPhoneLengthTemp = countLength++;
                        additionalTeleCodesTemp.push(encodeURIComponent((data.additionalphones[k].PHONE_CODE==null?'':data.additionalphones[k].PHONE_CODE)));
                        additionalCountryCodesTemp.push(encodeURIComponent((data.additionalphones[k].COUNTRY_CODE==null?'':data.additionalphones[k].COUNTRY_CODE)));
                        additionalareaCodesTemp.push(encodeURIComponent((data.additionalphones[k].AREA_CODE==null?'':data.additionalphones[k].AREA_CODE)));
                        additonalPhoneNumbersTemp.push(encodeURIComponent((data.additionalphones[k].PHONE_NUMBER==null?'':data.additionalphones[k].PHONE_NUMBER)));
                        additionalExtensionsTemp.push(encodeURIComponent((data.additionalphones[k].EXTENSION==null?'':data.additionalphones[k].EXTENSION)));
                        additionalIntAccessTemp.push(encodeURIComponent((data.additionalphones[k].INTERNATIONAL_ACCESS==null?'':data.additionalphones[k].INTERNATIONAL_ACCESS)));
                        additionalUnlistedIndTemp.push(encodeURIComponent((data.additionalphones[k].UNLISTED_IND==null?'':data.additionalphones[k].UNLISTED_IND)));
                        additionalSequenceTemp.push(encodeURIComponent((data.additionalphones[k].PHONE_SEQ==null?'':data.additionalphones[k].PHONE_SEQ)));
                        
                    }
                }
                /** to edit and remove additional phone **/
                
                for(var k=0;k<data.additionalphones.length;k++)
                {
                    
                    if(data.primaryphones[j].ADDRESS_SEQ == data.additionalphones[k].ADDRESS_SEQ && data.primaryphones[j].ADDRESS_TYPE_CODE == data.additionalphones[k].ADDRESS_TYPE_CODE)
                    {
                        addPhoneLength = countLength++;
                        additionalTeleCodes.push(encodeURIComponent((data.additionalphones[k].PHONE_CODE==null?'':data.additionalphones[k].PHONE_CODE)));
                        additionalCountryCodes.push(encodeURIComponent((data.additionalphones[k].COUNTRY_CODE==null?'':data.additionalphones[k].COUNTRY_CODE)));
                        additionalareaCodes.push(encodeURIComponent((data.additionalphones[k].AREA_CODE==null?'':data.additionalphones[k].AREA_CODE)));
                        additonalPhoneNumbers.push(encodeURIComponent((data.additionalphones[k].PHONE_NUMBER==null?'':data.additionalphones[k].PHONE_NUMBER)));
                        additionalExtensions.push(encodeURIComponent((data.additionalphones[k].EXTENSION==null?'':data.additionalphones[k].EXTENSION)));
                        additionalIntAccess.push(encodeURIComponent((data.additionalphones[k].INTERNATIONAL_ACCESS==null?'':data.additionalphones[k].INTERNATIONAL_ACCESS)));
                        additionalUnlistedInd.push(encodeURIComponent((data.additionalphones[k].UNLISTED_IND==null?'':data.additionalphones[k].UNLISTED_IND)));
                        additionalSequence.push(encodeURIComponent((data.additionalphones[k].PHONE_SEQ==null?'':data.additionalphones[k].PHONE_SEQ)));
                        
                        html_view += "<div class='emergency_container_additional' ><a class='address_additional'  href='tel:"+((data.additionalphones[k].COUNTRY_CODE==null)?'':data.additionalphones[k].COUNTRY_CODE)+((data.additionalphones[k].AREA_CODE==null)?'':data.additionalphones[k].AREA_CODE)+((data.additionalphones[k].PHONE_NUMBER==null)?'':data.additionalphones[k].PHONE_NUMBER)+"' ><div class='emergency_left_additional'>"+(getAddressCodeName(null,data.additionalphones[k].PHONE_CODE,null,null,null)+':')+"</div><div class='emergency_right_additional'><div class='call_button'></div>"+((data.additionalphones[k].COUNTRY_CODE==null)?'':('+'+data.additionalphones[k].COUNTRY_CODE))+((data.additionalphones[k].AREA_CODE==null)?'':(' ('+data.additionalphones[k].AREA_CODE+')'))+((data.additionalphones[k].PHONE_NUMBER==null)?'':(' '+data.additionalphones[k].PHONE_NUMBER))+ ((data.additionalphones[k].EXTENSION==null)?'':(' x'+data.additionalphones[k].EXTENSION))+"</div>";
                        html_view += "</a>";
                        html_view +="<div class='edit_delete_toggle' data-inline='true' data-iconpos='notext' data-icon='arrow-d'  data-theme='f' data-role='button' style='float:right' onClick='toggleEditDelete("+k+")'></div>"
                        html_view += "<div class='emergency_btn_holder "+k+"contact_edit_h'><div class='contact_delete_btn' data-theme='r' data-inline='true' data-mini='true' data-theme='b' data-role='button' onclick='deleteAddNumber(\""+addressTypeCode+"\",\""+sequence+"\",\""+fromDate+"\",\""+toDate+"\",\""+houseNumber+"\",\""+street1+"\",\""+street2+"\",\""+street3+"\",\""+street4+"\",\""+city+"\",\""+stateCode+"\",\""+zip+"\",\""+countyCode+"\",\""+nationCode+"\",\""+phoneCountryCode+"\",\""+phoneAreaCode+"\",\""+phoneNumber+"\",\""+extension+"\",\""+internationalAccessCode+"\",\""+unlistedInd+"\",\""+primaryPhoneSeq+"\",\""+primaryPhoneCode+"\",\""+additionalTeleCodesTemp+"\",\""+additionalCountryCodesTemp+"\",\""+additionalareaCodesTemp+"\",\""+additonalPhoneNumbersTemp+"\",\""+additionalExtensionsTemp+"\",\""+additionalIntAccessTemp+"\",\""+additionalUnlistedIndTemp+"\",\""+additionalSequenceTemp+"\",\""+addPhoneLength+"\")'>Delete</div>";
                        
                        html_view += "<div class='contact_edit_btn' data-inline='true' data-theme='a' data-role='button' data-mini='true' onclick='editAddContact(\""+addressTypeCode+"\",\""+sequence+"\",\""+fromDate+"\",\""+toDate+"\",\""+houseNumber+"\",\""+street1+"\",\""+street2+"\",\""+street3+"\",\""+street4+"\",\""+city+"\",\""+stateCode+"\",\""+zip+"\",\""+countyCode+"\",\""+nationCode+"\",\""+phoneCountryCode+"\",\""+phoneAreaCode+"\",\""+phoneNumber+"\",\""+extension+"\",\""+internationalAccessCode+"\",\""+unlistedInd+"\",\""+primaryPhoneSeq+"\",\""+primaryPhoneCode+"\",\""+additionalTeleCodesTemp+"\",\""+additionalCountryCodesTemp+"\",\""+additionalareaCodesTemp+"\",\""+additonalPhoneNumbersTemp+"\",\""+additionalExtensionsTemp+"\",\""+additionalIntAccessTemp+"\",\""+additionalUnlistedIndTemp+"\",\""+additionalSequenceTemp+"\",\""+addPhoneLength+"\");getAddressCodesJSON();' style='background-image: -webkit-gradient(linear, left top, left bottom, from( #8D8A8A), to( #868181));'>Edit</div></div>";
                        
              html_view += "</div>";        
                    }
                    
                }
               
            }
            
        }
        html_view += "<input type='hidden' id='addSeq' /><input type='hidden' id='addType' /><input type='hidden' id='deleteFlag' value='false' /><input type='hidden' id='delValue' />";
       
        html_view += "<div class='emergency_container_second_address' ><div  data-role='button' data-theme='a' onclick='addNewAddContact(\""+addressTypeCode+"\",\""+sequence+"\",\""+fromDate+"\",\""+toDate+"\",\""+houseNumber+"\",\""+street1+"\",\""+street2+"\",\""+street3+"\",\""+street4+"\",\""+city+"\",\""+stateCode+"\",\""+zip+"\",\""+countyCode+"\",\""+nationCode+"\",\""+phoneCountryCode+"\",\""+phoneAreaCode+"\",\""+phoneNumber+"\",\""+extension+"\",\""+internationalAccessCode+"\",\""+unlistedInd+"\",\""+primaryPhoneSeq+"\",\""+primaryPhoneCode+"\",\""+additionalTeleCodes+"\",\""+additionalCountryCodes+"\",\""+additionalareaCodes+"\",\""+additonalPhoneNumbers+"\",\""+additionalExtensions+"\",\""+additionalIntAccess+"\",\""+additionalUnlistedInd+"\",\""+additionalSequence+"\",\""+addPhoneLength+"\");getAddressCodesJSON();'>Add Contact Number</div></div>"
        
        html_view += "</div></div>";
        
        
        
        html_view += "<div class='my_edit_hidden_"+i+"' style='display:none' onclick='addNewAddressView();getAddressCodesJSON();setTimeout(function(){updateAddressView(\""+addressTypeCode+"\",\""+sequence+"\",\""+fromDate+"\",\""+toDate+"\",\""+houseNumber+"\",\""+street1+"\",\""+street2+"\",\""+street3+"\",\""+street4+"\",\""+city+"\",\""+stateCode+"\",\""+zip+"\",\""+countyCode+"\",\""+nationCode+"\",\""+phoneCountryCode+"\",\""+phoneAreaCode+"\",\""+phoneNumber+"\",\""+extension+"\",\""+internationalAccessCode+"\",\""+unlistedInd+"\",\""+primaryPhoneSeq+"\",\""+primaryPhoneCode+"\",\""+additionalTeleCodes+"\",\""+additionalCountryCodes+"\",\""+additionalareaCodes+"\",\""+additonalPhoneNumbers+"\",\""+additionalExtensions+"\",\""+additionalIntAccess+"\",\""+additionalUnlistedInd+"\",\""+additionalSequence+"\",\""+addPhoneLength+"\")},500);' ></div>";

    }
    
    
    //==>
    html_view += "<div class='add_new_add'><a href='javascript:void(0)' onclick='addNewAddressView();getAddressCodesJSON();'  data-role='button' data-theme='f' style='text-shadow:none!important;' data-icon='plus' data-iconpos='right'>Add New Address</a></div>";
    html_view += "</div>";
    
    document.getElementById('showAddressDisplay').innerHTML = html_view;
    stopActivitySpinner();
    $.mobile.changePage('index.html#addressDisplay',{transition:"none"});
    setTimeout('listRefreshHelper("addressDisplay")', 100);
    $('#addressDisplay').trigger('create');
    //backToHome(); //add a event listener to redirect back to home
}


function toggleEditDelete(val){
  $('.'+val+'contact_edit_h').slideToggle();
  
  
};







/**
 *To get address contact codes
 */
function addressContactCodes()
{
    url_service = "/enmu/services/student/banneraddresscodes";
    return_Where = getAddressContactCodes;
    request_ST(url_service, return_Where, false);
}

/**
 *Display the address
 *
 */
function addNewAddressView()
{
    try{ lengthCal = 1;
    //startActivitySpinner();
    var html_view ="<form id='saveAddressForm'><div data-role='none' style='float:left;width:100% !important;'>";
    html_view += "<div style='float:left;width:90%;padding:10px 5%;'>";
  //  html_view += "<div class='update-holder'><div style='float:left;width:30% !important;'>Valid From<span class='red'>*</span></div><div style='float:right;width:45% !important;'>Until</div></div>";
   // html_view += "<div class='update-holder'><input type='date' data-theme='c' placeholder='Valid From' id='validFrom' style='float:left;width:39% !important;' /><input type='date' data-theme='c' id='until' style='float:right;width:39% !important;' /></div>";
    
    //set hidden values starts-->
    //html_view += "<input type='hidden' id='setUpdate' placeholder='update flag' value='false' />"
    html_view += "<input type='hidden' id='seq' placeholder='sequence no' />";
    html_view += "<input type='hidden' id='addDelete' placeholder='delete flag'  />";
    //hidden values end-->
    
    
    
   // html_view += "<div class='update-holder'><textarea data-theme='c' rows='4' cols='40' type='text' placeholder='Address'  id='address1' /></textarea></div>";
    html_view += "<div class='update-holder'>Address Type<span class='red'>*</span></div>";
    html_view += "<div class='update-holder'><select data-theme='f' id='addressTypeCode' style='float:left;width:100% !important;'><option>Select</option></select></div>";
    html_view += "<div class='update-holder'>Address<span class='red'>*</span></div>";
        
    html_view += "<div class='update-holder'><input type='text' placeholder='Street Address 1' id='address1' data-theme='c' /></div>";
    
    html_view += "<div class='update-holder'><input type='text' placeholder='Street Address 2' id='address2' data-theme='c' /></div>";
    
    html_view += "<div class='update-holder'><input type='text' placeholder='Street Address 3' id='address3' data-theme='c' /></div>";
        
    
    //html_view += "<div class='update-holder'><input data-theme='c' type='text' placeholder='Address Line 2' id='address2' /></div>";
    //html_view += "<div class='update-holder'><input data-theme='c' type='text' placeholder='Address Line 3' id='address3' /></div>";
    //html_view += "<div class='update-holder'><textarea data-theme='c' rows='4' cols='40' type='text' placeholder='Address'  id='address4' /></textarea></div>";
  
    
    //html_view += "<div class='update-holder'><span style='display:block;float:left;width:40%;'><input data-theme='c' type='text' placeholder='House Number' id='houseNumber' /></span><span style='display:block;float:right;width:40%;'><input data-theme='c' MaxLength='9' type='text'  placeholder='Zip Code' id='zipCode' /><span class='red'>*</span></span></div>";
    
    
    
    //<span style='padding-left: 40px;position: relative;top: 10px;'>Zip<span class='red' id='zipAddressMandatoryStar' style='display:none' >*</span></span>

        var data = window.localStorage.getItem("ADDRESS_JSON");
        data = jQuery.parseJSON(data);
        
        
    html_view += "<div class='update-holder'>City<span class='red'>*</span></div>";
    html_view += "<div class='update-holder'><input type='text' placeholder='City' id='cityValue' data-theme='c' /></div>";
    //html_view += "<div class='update-holder'>Nation<span class='red'>*</span></div>";
    
    //html_view += "<div class='new_address_citystate_showhide1' ><div class='update-holder'>State<span class='red'>*</span></div>";
    //html_view += "<span style='display:block;float:left;width:40%;'><input type='text' data-theme='c' id='stateDispVal' placeholder='State'  /><input type='hidden'  id='stateValue' /></span></div>";
    
    html_view += "<div class='update-holder'>State Code<span class='red'>*</span></div>";
    html_view += "<div class='update-holder'><select id='stateValue' data-theme='c'><option>Select State</option></select></div>";
        
        
    //html_view += "<div class='update-holder'>State Code<span class='red'>*</span></div>";
    //html_view += "<div class='update-holder'><input type='text' placeholder='State' id='stateDispVal' data-theme='c' /></div>";
        
        
    //html_view += "<div class='update-holder'><span class='red'>*</span><span style='display:block;float:left;width:40%;'><input data-theme='c' MaxLength='10' type='text'  placeholder='State' id='stateDispVal' /></span>";
    
    html_view += "<div class='update-holder'>ZIP<span class='red'>*</span></div>";
    
   
    html_view += "<div class='update-holder'><input type='text' placeholder='Zip' id='zipCode' data-theme='c' /></div>";
        
    //html_view += "<span style='display:block;float:left;width:38.7%;margin:-6% 0 0 19.6%;'><input data-theme='c' MaxLength='10' type='text' placeholder='Zip Code' id='zipCode' /></span><div style='clear:both;'></div><span class='red'>*</span></div>";
     html_view += "<div class='update-holder'>Nation</div>";    
    html_view += "<div class='update-holder'><input type='text' data-theme='c' placeholder='Nation' id='nationDispVal' /><input type='hidden' id='nationValue' /></div>";
        
    
        
    //html_view += "<div class='update-holder'>County</div>";
    //html_view += "<div class='update-holder'><input type='text' data-theme='c' id='countyDispVal' placeholder='County' /><input type='hidden'  id='countyValue'  /></div></div>";
       
    
    
    /*html_view += "<div class='update-holder'>Phone Number</div>";
    html_view += "<div class='update-holder'><input type='hidden' placeholder='primaryPhoneSeq' id='primaryPhoneSeq'><input type='hidden' id='primaryPhoneCode' placeholder='primaryPhoneCode' ><input type='hidden' id='unlistedInd' placeholder='unlistedInd' /></div>";
    html_view += "<div class='update-holder'><input data-theme='c' type='tel'  placeholder='Country' id='countryPhone' MaxLength='3' style='float:left;width:20% !important;margin-left:3px;' /><input type='tel' placeholder='Area' MaxLength='5' data-theme='c' id='areaPhone' style='float:left;width:18% !important;margin-left:2px;' /><input data-theme='c' type='tel'  placeholder='Phone' id='phoneValue' MaxLength='10' style='float:left;width:23% !important;margin-left:2px;'/> <input type='tel' data-theme='c' placeholder='Ext' id='extensionValue' MaxLength='4' style='float:left;width:10% !important;margin-left:2px;'/></div>";
     
     */
        
    // html_view += "<div style='width:100%;float:left;style='font-size:10px;'><div style='float:left;width:10% !important;'>Country</div><div style='float:left;width:15% !important;'>Area</div><div style='float:left;width:20% !important;'>Phone</div><div style='float:left;width:25% !important;text-align:center;'>Extension</div></div>";
        
   // html_view += "<div class='update-holder'>International Access Code</div>";
   // html_view += "<div class='update-holder'><input type='tel'  placeholder='International Access Code' id='internationalAccess' MaxLength='5' data-theme='c' /></div>";
    
    
    //for additional numbers starts-->
    html_view += "<div id='addNumberPart'>";
    html_view += "<div class='update-holder'><div class='additional_container'>Phone Number</div></div>";
    
    html_view += "<input type='hidden' id='addSequence' placeholder='addSequence' /><input type='hidden' id='addPriUpdate' /><input type='hidden' id='addUnlistedInd' placeholder='addUnlistedInd' />";
    
   // html_view += "<div class='additional_detail_hldr'> <div class='additional_number' ><div class='update-holder'>Phone Number Type</div>";
   // html_view += "<div class='update-holder'><select data-theme='f' id='teleTypeCode' style='float:left;width:100% !important;'><option>Select</option></select></div>"
    
   // html_view += "<div class='update-holder'>Phone Numbers</div>";
        
    //html_view += "<div class='update-holder'>"+/*<input data-theme='c' type='tel' placeholder='Country' id='addCountryPhone' MaxLength='3' style='float:left;width:20% !important;margin-left:4px;' />*/"+<input type='tel'  placeholder='Area' MaxLength='5' data-theme='c' id='addAreaPhone' style='float:left;width:15% !important;margin-left:2px;' /><input data-theme='c' type='tel' placeholder='Phone' MaxLength='10' id='addPhoneValue' style='float:left;width:23% !important;margin-left:2px;'/> <input type='tel' data-theme='c' placeholder='Ext' id='addExtensionValue' MaxLength='4' style='float:left;width:10% !important;margin-left:2px;'/></div>";
        
    html_view += "<div class='update-holder'><input type='tel'  placeholder='Area' MaxLength='3' data-theme='c' id='areaPhone' style='float:left;width:15% !important;margin-left:2px;' /><input data-theme='c' type='tel' placeholder='Phone' MaxLength='7' id='phoneValue' style='float:left;width:23% !important;margin-left:2px;'/> <input type='tel' data-theme='c' placeholder='Ext' id='extensionValue' MaxLength='4' style='float:left;width:10% !important;margin-left:2px;'/></div>";
        
        
    // html_view += "<div style='width:100%;float:left;' style='font-size:10px;'><div style='float:left;width:10% !important;margin-left:5px;'>Country</div><div style='float:left;width:18% !important;margin-left:34px;'>Area</div><div style='float:left;width:18% !important;'>Phone</div><div style='float:left;width:35% !important;margin-left:15px;text-align:center;'>Extension</div></div>";
    
   // html_view += "<div class='update-holder'>International Access Code</div>";
   // html_view += "<div class='update-holder'><input type='tel' placeholder='International Access Code' id='addInternationalAccess' MaxLength='5' data-theme='c' /></div></div></div>";
    
    html_view += "<div id='addPhoneDetails_1' ></div>"; //preextended list
    
    //html_view += "<div class='update-holder' id='addPhoneDetailsNew'> <div data-role='button' data-icon='plus' data-theme='f' style='text-shadow:none!important;' data-iconpos='right' onclick='newAddDetails("+1+")'>Add Additional Contact</div></div>";
    html_view += "</div></form>";
    
    //for additional numbers end-->
    
    html_view += "<div class='update-holder'><div id='insertAddressSave' onclick='addressSave();' class='add_address_btn' data-role='button' data-theme='f' ><span id='insertAddress'>Add Address</span></div><div class='reset_btn' data-role='button' data-theme='a' onclick='resetAddressDetail()' >Reset</div></div></div>";
    
    html_view += "</div>";
    html_view += "</div>";
    document.getElementById('saveAddress').innerHTML = html_view;
    $("#addressTitle").html("Add New Address");
    //stopActivitySpinner();
    //ifAndroid(); //to check calendar plugin in android device
    //$('.new_address_citystate_showhide').hide();// to hide state and county fields
    window.localStorage.setItem("UPDATE_ADDRESS_FLAG",0); // set flag in case of update address
    $.mobile.changePage('index.html#setAddress',{transition:"none"});
    $('#setAddress').trigger('create');
    }catch(e){alert(e)}
}

/**
 *To save/update address
 */
function addressSave()
{
    startActivitySpinner();
    url_service = "/enmu/services/student/banneraddressinsert";
    return_Where = setAddress;
    request_ST(url_service, return_Where, false);
}


/**
 *To update address
 */
function addressUpdate()
{
    startActivitySpinner();
    //$('#setUpdate').val(booleanx); // check and switch url incase of calling setAddress function
    url_service = "/enmu/services/student/banneraddressupdate";
    return_Where = setUpdateAddress;
    request_ST(url_service, return_Where, false);
}

/**
 *To update address view
 */

var updateAdditionalTeleCodes,UpdateadditionalCountryCodes,UpdateadditionalareaCodes,UpdateadditonalPhoneNumbers,UpdateadditionalExtensions,UpdateadditionalIntAccess,UpdateadditionalUnlistedInd,UpdateadditionalSequence;

function updateAddressView(addressTypeCode, sequence, fromDate, toDate, houseNumber, street1, street2, street3, street4, city, stateCode, zip, countyCode, nationCode, phoneCountryCode, phoneAreaCode, phoneNumber, extension, internationalAccessCode, unlistedInd, primaryPhoneSeq, primaryPhoneCode, additionalTeleCodes, additionalCountryCodes, additionalareaCodes, additonalPhoneNumbers, additionalExtensions, additionalIntAccess, additionalUnlistedInd, additionalSequence, addPhoneLength)
{
    window.localStorage.setItem("UPDATE_ADDRESS_FLAG",1); // set flag in case of update address
    var addressTypeCode = decodeURIComponent(addressTypeCode);
    var sequence = decodeURIComponent(sequence);
    var fromDate = decodeURIComponent(fromDate);
    var toDate = decodeURIComponent(toDate);
    var houseNumber = decodeURIComponent(houseNumber);
    var street1 = decodeURIComponent(street1);
    var street2 = decodeURIComponent(street2);
    var street3 = decodeURIComponent(street3);
    var street4 = decodeURIComponent(street4);
    var city = decodeURIComponent(city);
    var stateCode = decodeURIComponent(stateCode);
    var zip = decodeURIComponent(zip);
    var countyCode = decodeURIComponent(countyCode);
    var nationCode = decodeURIComponent(nationCode);
    
    var phoneCountryCode = decodeURIComponent(phoneCountryCode);
    var phoneAreaCode = decodeURIComponent(phoneAreaCode);
    var phoneNumber = decodeURIComponent(phoneNumber);
    var extension = decodeURIComponent(extension);
    var internationalAccessCode = decodeURIComponent(internationalAccessCode);
    var unlistedInd = decodeURIComponent(unlistedInd);
    var primaryPhoneSeq = decodeURIComponent(primaryPhoneSeq);
    var primaryPhoneCode = decodeURIComponent(primaryPhoneCode);
    
    var additionalTeleCodes = decodeURIComponent(additionalTeleCodes);
    var additionalCountryCodes = decodeURIComponent(additionalCountryCodes);
    var additionalareaCodes = decodeURIComponent(additionalareaCodes);
    var additonalPhoneNumbers = decodeURIComponent(additonalPhoneNumbers);
    var additionalExtensions = decodeURIComponent(additionalExtensions);
    var additionalIntAccess = decodeURIComponent(additionalIntAccess);
    var additionalUnlistedInd = decodeURIComponent(additionalUnlistedInd);
    var additionalSequence = decodeURIComponent(additionalSequence);
    
    var addPhoneLength = addPhoneLength;
    
    
    updateAdditionalTeleCodes=additionalTeleCodes;
    UpdateadditionalCountryCodes=additionalCountryCodes;
    UpdateadditionalareaCodes=additionalareaCodes;
    UpdateadditonalPhoneNumbers=additonalPhoneNumbers;
    UpdateadditionalExtensions=additionalExtensions;
    UpdateadditionalIntAccess=additionalIntAccess;
    UpdateadditionalUnlistedInd=additionalUnlistedInd;
    UpdateadditionalSequence=additionalSequence;
    
    
    
    //alert(additionalTeleCodes+" "+additionalareaCodes+" "+additonalPhoneNumbers);
    
    $('#addressTypeCode').val(addressTypeCode).selectmenu("refresh").attr('disabled',true);
    $('#seq').val(sequence);
    //$('#validFrom').val(inputTypeDate(fromDate));
    /*if(toDate)
    {
      $('#until').val(inputTypeDate(toDate));  
    }
    else
    {
        $('#until').val("");
    }*/
    try{
        
    $('#houseNumber').val(houseNumber);
    $('#address1').val(street1);
    $('#address2').val(street2);
    $('#address3').val(street3);
    //$('#address4').val(street4);
    $('#cityValue').val(city);
    $('#stateValue').val(stateCode);
    $('#zipCode').val(zip);
    $('#countyValue').val(countyCode);
    $('#nationValue').val(nationCode);
    
    /** to show in input fields **/
    $('#stateDispVal').val(getAddressCodeName(null, null, null, stateCode, null));
    $('#countyDispVal').val(getAddressCodeName(countyCode, null, null, null, null));
    $('#nationDispVal').val(getAddressCodeName(null, null, nationCode, null, null));
    
    $('#countryPhone').val(phoneCountryCode);
    $('#areaPhone').val(phoneAreaCode);
    $('#phoneValue').val(phoneNumber);
    $('#extensionValue').val(extension);
    $('#internationalAccess').val(internationalAccessCode);
    $('#unlistedInd').val(unlistedInd);
    $('#primaryPhoneSeq').val(primaryPhoneSeq);
    $('#primaryPhoneCode').val(primaryPhoneCode);
    
    //$('#teleTypeCode').val(additionalTeleCodes);
    $('#addPriUpdate').val(additionalTeleCodes);
    $('#addCountryPhone').val(additionalCountryCodes);
    $('#addAreaPhone').val(additionalareaCodes);
    $('#addPhoneValue').val(additonalPhoneNumbers);
    $('#addExtensionValue').val(additionalExtensions);
    $('#addInternationalAccess').val(additionalIntAccess);
    $('#addUnlistedInd').val(additionalUnlistedInd);
    
    $('#addSequence').val(additionalSequence);  //to update primary content
   // $('#addNumberPart').hide();  // add additional button remove in case of address update
    $('#insertAddress').text("Update Address");
    $('#addressTitle').text("Address Update");
    $('#insertAddressSave').attr("onclick","addressUpdate()");
    
    $('#stateValue').val(stateCode);$('#stateValue').selectmenu("refresh");
    /** NOTE: if validate fill the state in case of selected country **/
    window.localStorage.setItem("INITIAL_UPDATE_ADDRESS_STATE",stateCode);
    window.localStorage.setItem("INITIAL_UPDATE_ADDRESS_COUNTRY",nationCode);
    disableFields('one'); // to disable fields related to US
    //showHideMandatoryAddress(stateCode);
    }catch(e){alert(e)}
}






/**
 *Add new additional contact
 */

function addNewAddContact(addressTypeCode, sequence, fromDate, toDate, houseNumber, street1, street2, street3, street4, city, stateCode, zip, countyCode, nationCode, phoneCountryCode, phoneAreaCode, phoneNumber, extension, internationalAccessCode, unlistedInd,  primaryPhoneSeq, primaryPhoneCode, additionalTeleCodesAdd, additionalCountryCodesAdd, additionalareaCodesAdd, additonalPhoneNumbersAdd, additionalExtensionsAdd, additionalIntAccessAdd, additionalUnlistedIndAdd, additionalSequenceAdd, addPhoneLength)
{
    
    
    var html_view = "";
    
    html_view += "<input type='hidden' id='addSequence' placeholder='addSequence' /><input type='hidden' id='addPriUpdate' /><input type='hidden' id='addUnlistedInd' placeholder='addUnlistedInd' />";
    
    html_view += "<div class='additional_detail_hldr'><div style='margin-top:5px;'></div><div class='additional_number' ><div class='update-holder'>Phone Number Type</div>";
    html_view += "<div class='update-holder'><select data-theme='f' id='teleTypeCode' style='float:left;width:100% !important;'><option>Select</option></select></div>"
    
    html_view += "<div class='update-holder'>Phone Number</div>";
    
    html_view += "<div class='update-holder'><input type='tel'  placeholder='Area' MaxLength='3' data-theme='c' id='addAreaPhoneNew' style='float:left;width:15% !important;margin-left:2px;' /><input data-theme='c' type='tel' placeholder='Phone' MaxLength='7' id='addPhoneValueNew' style='float:left;width:23% !important;margin-left:2px;'/> <input type='tel' data-theme='c' placeholder='Ext' id='addExtensionValueNew' MaxLength='4' style='float:left;width:10% !important;margin-left:2px;'/></div>";
    
    
   // html_view += "<div class='update-holder'>"+/*<!--<input data-theme='c' type='tel' placeholder='Country' id='addCountryPhoneNew' MaxLength='3' style='float:left;width:20% !important;margin-left:4px;' />*/+"<input type='tel'  placeholder='Area' MaxLength='5' data-theme='c' id='addAreaPhoneNew' style='float:left;width:15% !important;margin-left:2px;' /><input data-theme='c' type='tel' placeholder='Phone' MaxLength='10' id='addPhoneValueNew' style='float:left;width:23% !important;margin-left:2px;'/> <input type='tel' data-theme='c' placeholder='Ext' id='addExtensionValueNew' MaxLength='4' style='float:left;width:10% !important;margin-left:2px;'/></div>";
    // html_view += "<div style='width:100%;float:left;' style='font-size:10px;'><div style='float:left;width:10% !important;margin-left:5px;'>Country</div><div style='float:left;width:18% !important;margin-left:34px;'>Area</div><div style='float:left;width:18% !important;'>Phone</div><div style='float:left;width:35% !important;margin-left:15px;text-align:center;'>Extension</div></div>";
    
  //  html_view += "<div class='update-holder'>International Access Code</div>";
  //  html_view += "<div class='update-holder'><input type='tel' placeholder='International Access Code' id='addInternationalAccessNew' MaxLength='5' data-theme='c' /></div></div></div>";
    html_view += "<div class='update-holder'><div class='add_address_btn' id='addClick' data-role='button' data-theme='f'  ><span>Save Number</span></div><div class='reset_btn' data-role='button' data-theme='a' onclick='resetAddressDetail();' >Reset</div>";
    document.getElementById('saveAddress').innerHTML = html_view;
    $("#addressTitle").html("Additional Number");
    $.mobile.changePage('index.html#setAddress',{transition:"none"});
    $('#setAddress').trigger('create');
    
    
    var additionalTeleCodesArr = [], additionalCountryCodesArr = [], additionalareaCodesArr = [], additonalPhoneNumbersArr = [], additionalExtensionsArr = [], additionalIntAccessArr = [], additionalUnlistedIndArr = [], additionalSequenceArr = [];
    
    var addressTypeCode = decodeURIComponent(addressTypeCode);
    var sequence = decodeURIComponent(sequence);
    var fromDate = decodeURIComponent(fromDate);
    var toDate = decodeURIComponent(toDate);
    var houseNumber = decodeURIComponent(houseNumber);
    var street1 = decodeURIComponent(street1);
    var street2 = decodeURIComponent(street2);
    var street3 = decodeURIComponent(street3);
    var street4 = "";//decodeURIComponent(street4);
    var city = decodeURIComponent(city);
    var stateCode = decodeURIComponent(stateCode);
    var zip = decodeURIComponent(zip);
    var countyCode = decodeURIComponent(countyCode);
    var nationCode = decodeURIComponent(nationCode);
    
    var phoneCountryCode = decodeURIComponent(phoneCountryCode);
    var phoneAreaCode = decodeURIComponent(phoneAreaCode);
    var phoneNumber = decodeURIComponent(phoneNumber);
    var extension = decodeURIComponent(extension);
    var internationalAccessCode = decodeURIComponent(internationalAccessCode);
    var unlistedInd = "";
    var primaryPhoneSeq = decodeURIComponent(primaryPhoneSeq);
    var primaryPhoneCode = decodeURIComponent(primaryPhoneCode);
    
    /** to click inherit model function **/
    $('#addClick').click(function(){
                         addAddNewContact();
                         
                         });
    /**----------------------------**/
    
    
    function addAddNewContact()
    {
        startActivitySpinner();
        url_service = "/enmu/services/student/banneraddressupdate";
        return_Where = newAddAddressModel;
        request_ST(url_service, return_Where, false);
    }
    
    
    function newAddAddressModel(serviceTicket)
    {
        
        try{
            
        
        var a = $('#teleTypeCode').val();
        if (a == "Select") {
            navigator.notification.alert("Please select a phone number type.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }
        var b= "";
        
        /*var c = $('#addCountryPhoneNew').val();
        if (c.search(/^$|^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }*/
        c="";
        
        var d = $('#addAreaPhoneNew').val();
        if (d.search(/^[0-9 ]+$/) === -1 || d.length != 3) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }
        var e = $('#addPhoneValueNew').val();
        if (e.search(/^[0-9 ]+$/) === -1 || e.length != 7) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }
        var f = $('#addExtensionValueNew').val();
        if (f.search(/^$|^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }
        /*var g = $('#addInternationalAccessNew').val();
        if (g.search(/^$|^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Enter numbers in international access code.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }*/
        var g ="";
        var h ="";
        
        var additionalTeleCodes = decodeURIComponent(additionalTeleCodesAdd)+","+a;
        var additionalCountryCodes = decodeURIComponent(additionalCountryCodesAdd)+","+c;
        var additionalareaCodes = decodeURIComponent(additionalareaCodesAdd)+","+d;
        var additonalPhoneNumbers = decodeURIComponent(additonalPhoneNumbersAdd)+","+e;
        var additionalExtensions = decodeURIComponent(additionalExtensionsAdd)+","+f;
        var additionalIntAccess = decodeURIComponent(additionalIntAccessAdd)+","+g;
        var additionalUnlistedInd = decodeURIComponent(additionalUnlistedIndAdd.replace(/null/g,""))+","+h;
        var additionalSequence = decodeURIComponent(additionalSequenceAdd)+","+b;
        
         var addDelInd = decodeURIComponent(additionalUnlistedIndAdd.replace(/null/g,""))+","+h; // use unlisted indicator value here to update content
        
        var server_url = serviceName+"/enmu/services/student/banneraddressupdate"+"?ticket="+serviceTicket;
        var storeUpdateData = "seqno="+sequence+"&";
        
        console.log("data passed in add additional phone address --> "+storeUpdateData+"atypcode="+addressTypeCode+"&fromdate="+fromDate+"&todate="+toDate+"&housenumber="+houseNumber+"&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addlseqnos="+additionalSequence+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes="+additionalCountryCodes+"&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+"&addldelinds="+addDelInd);
        
        //startActivitySpinner();
        var xhr=$.ajax({
                       url: server_url,
                       contentType: 'application/json;charset=UTF-8',
                       type: 'POST',
                       data: storeUpdateData+"atypcode="+addressTypeCode+"&fromdate="+fromDate+"&todate="+toDate+"&housenumber="+houseNumber+"&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addlseqnos="+additionalSequence+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes="+additionalCountryCodes+"&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+"&addldelinds="+addDelInd,
                       beforeSend:function (){
                       setTimeout(function(){
                                  xhr.abort();
                                  },globalAjaxTimer);},
                       success: function(data)
                       {
                       console.log('success setUpdateAddress--> ');
                       console.log(data);
                       console.log(JSON.stringify(data));
                       if(data)
                       {
                       navigator.notification.alert('Phone number added successfully.',null,'Address Update','Ok');
                       }
                       url_service="/enmu/services/student/banneraddressfetch";
                       return_Where = getAddress;
                       request_ST(url_service,return_Where,false);
                       },
                       error: function(e)
                       {
                       console.log('error setUpdateAddress--> ');
                       console.log(e);
                       stopActivitySpinner();
                       alertServerError(e.status);
                       }
                       });
        }catch(e){alert(e)}
        
    }
    
}



/**
 *To reset address details field
 */
function resetAddressDetail()
{ 
    if(($('#addressTitle').html()).indexOf("Update") != -1)
    {
        $('#addressTypeCode').selectmenu('refresh');
    }
    else
    {
        $('#addressTypeCode').val("Select").selectmenu('refresh');
    }
    
    //$('#seq').val("");
    //$('#validFrom').val("");
    //$('#until').val("");
    //$('#houseNumber').val("");
    $('#address1').val("");
    $('#address2').val("");
    $('#address3').val("");
    $('#address4').val("");
    $('#cityValue').val("");
    $('#stateValue').val("Select");
    $('#zipCode').val("");
    $('#countyValue').val("Select");
    $('#nationValue').val("Select");
    $('#teleTypeCode').val("Select").selectmenu('refresh');
    $('#countryPhone').val("");
    $('#areaPhone').val("");
    $('#phoneValue').val("");
    $('#extensionValue').val("");
    $('#internationalAccess').val("");
    $('#unlistedInd').val("");
    $('#primaryPhoneSeq').val("");
    $('#primaryPhoneCode').val("");
    
    $('#stateDispVal').val("");
    $('#countyDispVal').val("");
    $('#nationDispVal').val("");
    
    
    if(($('#addressTitle').html()).indexOf("Update") == -1)  // In case additional contact view, not want to reset its value
    {
    $('#addCountryPhone').val("");
    $('#addAreaPhone').val("");
    $('#addPhoneValue').val("");
    $('#addExtensionValue').val("");
    $('#addInternationalAccess').val("");
    $('#addUnlistedInd').val("");
    //$('#addSequence').val("");
    }
    /** To reset additional appended fields **/
    if(lengthCal > 1)
    {
        for(var i=1;i<lengthCal;i++)
        {
            $('#teleTypeCode_'+i).val("Select").selectmenu('refresh');;
            $('#addCountryPhone_'+i).val("");
            $('#addAreaPhone_'+i).val("");
            $('#addPhoneValue_'+i).val("");
            $('#addExtensionValue_'+i).val("");
            $('#addInternationalAccess_'+i).val("");
            $('#addUnlistedInd_'+i).val("");
        }
    }
    
    /** to reset additional phone field **/
    $('#addCountryPhoneNew').val("");
    $('#addAreaPhoneNew').val("");
    $('#addPhoneValueNew').val("");
    $('#addExtensionValueNew').val("");
    //$('#addInternationalAccessNew').val("");
    
}


/**
 *To remove address
 */
function addressRemove(addressSeq, addressType)
{
    var addressSequence = addressSeq;
    var addressTypeCode = addressType;
    $('#addSeq').val(addressSequence);
    $('#addType').val(addressTypeCode);
    
    navigator.notification.confirm('Are you sure you want to delete this address?', confirmCall, 'Address', ['Yes','No']);
    function confirmCall(button)
    {
        if(button==1)
        {
            url_service = "/enmu/services/student/banneraddressdelete";
            return_Where = deleteAddress;
            request_ST(url_service, return_Where, false);
        }
        else
        {
            return false; //do nothing
        }
    }
}



/**
 *To add new additional view
 */
function newAddDetails(clicks)
{
    var setVal = clicks
    lengthCal = setVal+1;
    
    if(clicks>1)
    {
        $('#addPhoneDetailsNew_'+(clicks)).remove();
        $('#addPhoneDetailsNew_'+(clicks-1)).remove();
    }
    //alert(clicks);
    var num = clicks;
    var html_view = "";
    html_view += "<input type='hidden' id='addSequence_"+num+"'  /><input type='hidden' id='addUnlistedInd_"+num+"'  />";
    html_view += "<div class='additional_detail_hldr'><div class='update-holder'>Phone Number Type</div>";
    html_view += "<div class='update-holder'><select data-theme='f' id='teleTypeCode_"+num+"'  style='float:left;width:100% !important;'><option>Select</option></select></div>";
    
    html_view += "<div class='update-holder'>Phone Number</div>";
    
    html_view += "<div class='update-holder'><input type='tel' placeholder='Area' MaxLength='3' data-theme='c' id='addAreaPhone_"+num+"'  style='float:left;width:15% !important;margin-left:2px;' /><input type='tel' data-theme='c' placeholder='Phone' MaxLength='7' id='addPhoneValue_"+num+"'  style='float:left;width:23% !important;margin-left:2px;'/> <input type='tel' data-theme='c' placeholder='Ext' MaxLength='4' id='addExtensionValue_"+num+"'   style='float:left;width:10% !important;margin-left:2px;'/></div>";
   
    // html_view += "<div style='width:100%;float:left;' style='font-size:10px;'><div style='float:left;width:10% !important;margin-left:5px;'>Country</div><div style='float:left;width:18% !important;margin-left:34px;'>Area</div><div style='float:left;width:18% !important;'>Phone</div><div style='float:left;width:35% !important;margin-left:15px;text-align:center;'>Extension</div></div>";
    
   // html_view += "<div class='update-holder'>International Access Code</div>";
   // html_view += "<div class='update-holder'><input type='tel' placeholder='International Access Code' MaxLength='5' id='addInternationalAccess_"+num+"' data-role='none' /></div></div>";
    var set = num+1;
    html_view += "<div class='update-holder' id='addPhoneDetailsNew_"+num+"'><div data-role='button' data-icon='plus' data-theme='f' style='text-shadow:none!important;' data-iconpos='right' onclick='newAddDetails("+(set)+")'>Add Additional Contact</div></div>";
    html_view += "<div id='addPhoneDetails_"+(num+1)+"' ></div>"
    $('#addPhoneDetailsNew').remove();
    $('#addPhoneDetails_'+num).html(html_view);
    generateTeleCodes(clicks);
    $('#seq').val(null); //set sequence number null in updation of contact
    //$('#teleTypeCode_'+num-2).selectmenu('refresh');
    $('#setAddress').trigger('create');
    
}

/**
 *To genrate Telecodes on page
 */
function generateTeleCodes(code)
{
    var data = window.localStorage.getItem("ADDRESS_JSON");
    data = jQuery.parseJSON(data);
    var jsonKey = ["telephonetypecodes","nationcodes","statecodes","addresstypecodes"];
    
    if(Object_keys(data,1) == jsonKey[1])
    {
        var html_data = "<option>Select</option>";
        for(var i=0;i<data.telephonetypecodes.length;i++)
        {
            html_data += "<option value='"+data.telephonetypecodes[i].TELE_CODE+"'>"+data.telephonetypecodes[i].TELE_DESC+"</option>";
        }
        $('#teleTypeCode_'+code).html(html_data);
    }
    
}


/**
 *To delete additional number
 */
function deleteAddNumber(addressTypeCode, sequence, fromDate, toDate, houseNumber, street1, street2, street3, street4, city, stateCode, zip, countyCode, nationCode, phoneCountryCode, phoneAreaCode, phoneNumber, extension, internationalAccessCode, unlistedInd, primaryPhoneSeq, primaryPhoneCode, additionalTeleCodes, additionalCountryCodes, additionalareaCodes, additonalPhoneNumbers, additionalExtensions, additionalIntAccess, additionalUnlistedInd, additionalSequence, deleteValue)
{
    var addressTypeCode = decodeURIComponent(addressTypeCode);
    var sequence = decodeURIComponent(sequence);
    var fromDate = decodeURIComponent(fromDate);
    var toDate = decodeURIComponent(toDate);
    var houseNumber = decodeURIComponent(houseNumber);
    var street1 = decodeURIComponent(street1);
    var street2 = "";//decodeURIComponent(street2);
    var street3 = "";//decodeURIComponent(street3);
    var street4 = "";//decodeURIComponent(street4);
    var city = decodeURIComponent(city);
    var stateCode = decodeURIComponent(stateCode);
    var zip = decodeURIComponent(zip);
    var countyCode = decodeURIComponent(countyCode);
    var nationCode = decodeURIComponent(nationCode);
    var phoneCountryCode = decodeURIComponent(phoneCountryCode);
    var phoneAreaCode = decodeURIComponent(phoneAreaCode);
    var phoneNumber = decodeURIComponent(phoneNumber);
    var extension = decodeURIComponent(extension);
    var internationalAccessCode = decodeURIComponent(internationalAccessCode);
    var unlistedInd = "";//decodeURIComponent(unlistedInd);
    var primaryPhoneSeq = decodeURIComponent(primaryPhoneSeq);
    var primaryPhoneCode = decodeURIComponent(primaryPhoneCode);
    
    var additionalTeleCodes = decodeURIComponent(additionalTeleCodes);
    var additionalCountryCodes = decodeURIComponent(additionalCountryCodes);
    var additionalareaCodes = decodeURIComponent(additionalareaCodes);
    var additonalPhoneNumbers = decodeURIComponent(additonalPhoneNumbers);
    var additionalExtensions = decodeURIComponent(additionalExtensions);
    var additionalIntAccess = decodeURIComponent(additionalIntAccess);
    var additionalUnlistedInd = (decodeURIComponent(additionalUnlistedInd).replace(/null/g,""));
    var additionalSequence = decodeURIComponent(additionalSequence);
    
    
    //    var addSeqLen = [];
    //    addSeqLen = additionalSequence.split(",");  //to find and delete that additional number
    var deleteString = [];
    deleteString  = additionalUnlistedInd.split(",");
    //alert(addSeqLen.length);
    //    for(var i=0;i<addSeqLen.length;i++)
    //    {
    //        deleteString.push(",");
    //    }
    //alert("Array "+deleteString);
    //alert("Del Value "+deleteValue);
    deleteString[(deleteValue-1)] = "Y";  //add to delete that row additional number
    
    navigator.notification.confirm('Are you sure you want to delete this number?', confirmCall, 'Address',['Yes','No']);
    function confirmCall(button)
    {
        if(button==1)
        {
            startActivitySpinner();
            url_service = "/enmu/services/student/banneraddressupdate";
            return_Where = deleteAddNumberModel;
            request_ST(url_service, return_Where, false);
        }
        else
        {
            return false; //do nothing
        }
    }
    
    function deleteAddNumberModel(serviceTicket)
    {
        var server_url = serviceName+"/enmu/services/student/banneraddressupdate"+"?ticket="+serviceTicket;
        var storeUpdateData = "seqno="+sequence+"&";
        console.log(storeUpdateData+"atypcode="+addressTypeCode+"&fromdate="+fromDate+"&todate="+toDate+"&housenumber="+houseNumber+"&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addlseqnos="+additionalSequence+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes="+additionalCountryCodes+"&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+"&addldelinds="+deleteString);
        
        //startActivitySpinner();
        var xhr=$.ajax({
                       url: server_url,
                       contentType: 'application/json;charset=UTF-8',
                       type: 'POST',
                       data: storeUpdateData+"atypcode="+addressTypeCode+"&fromdate="+fromDate+"&todate="+toDate+"&housenumber="+houseNumber+"&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addlseqnos="+additionalSequence+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes="+additionalCountryCodes+"&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+"&addldelinds="+deleteString,
                       beforeSend:function (){
                       setTimeout(function(){
                                  xhr.abort();
                                  },globalAjaxTimer);},
                       success: function(data)
                       {
                       console.log('success deleteAdditionalNumber--> ');
                       console.log(data);
                       console.log(JSON.stringify(data));
                       if(data)
                       {
                         navigator.notification.alert('Phone number deleted successfully.',null,'Address Update','Ok');
                       }
                       url_service="/enmu/services/student/banneraddressfetch";
                       return_Where = getAddress;
                       request_ST(url_service,return_Where,false);
                       },
                       error: function(e)
                       {
                       console.log('error deleteAdditionalNumber--> ');
                       console.log(e);
                       stopActivitySpinner();
                       alertServerError(e.status);
                       }
                       });
        
        
    }
}




/**
 *Edit additional contact
 */
function editAddContact(addressTypeCode, sequence, fromDate, toDate, houseNumber, street1, street2, street3, street4, city, stateCode, zip, countyCode, nationCode, phoneCountryCode, phoneAreaCode, phoneNumber, extension, internationalAccessCode, unlistedInd, primaryPhoneSeq, primaryPhoneCode, additionalTeleCodesAdd, additionalCountryCodesAdd, additionalareaCodesAdd, additonalPhoneNumbersAdd, additionalExtensionsAdd, additionalIntAccessAdd, additionalUnlistedIndAdd, additionalSequenceAdd, addPhoneLength)
{
    
    var additionalTeleCodesArr = [], additionalCountryCodesArr = [], additionalareaCodesArr = [], additonalPhoneNumbersArr = [], additionalExtensionsArr = [], additionalIntAccessArr = [], additionalUnlistedIndArr = [], additionalSequenceArr = [], addPhoneLength;
    addPhoneLength = parseInt(addPhoneLength)-1;
    additionalTeleCodesArr = additionalTeleCodesAdd.split(",");
    additionalCountryCodesArr = additionalCountryCodesAdd.split(",");
    additionalareaCodesArr = additionalareaCodesAdd.split(",");
    additonalPhoneNumbersArr = additonalPhoneNumbersAdd.split(",");
    additionalExtensionsArr = additionalExtensionsAdd.split(",");
    additionalIntAccessArr = additionalIntAccessAdd.split(",");
    //additionalUnlistedIndArr = additionalUnlistedIndAdd.split(",");
    
    var html_view = "";
    
    html_view += "<input type='hidden' id='addSequence' placeholder='addSequence' /><input type='hidden' id='addPriUpdate' /><input type='hidden' id='addUnlistedInd' placeholder='addUnlistedInd' />";
    
    html_view += "<div class='additional_detail_hldr'><div style='margin-top:5px;'></div><div class='additional_number' ><div class='update-holder'>Phone Number Type</div>";
    html_view += "<div class='update-holder'><select data-theme='f' id='teleTypeCode' style='float:left;width:100% !important;'><option>Select</option></select></div>"
    
    html_view += "<div class='update-holder'>Phone Number</div>";
   // html_view += "<div class='update-holder'>"+/*<input data-theme='c' type='tel' placeholder='Country' value='"+additionalCountryCodesArr[addPhoneLength]+"' id='addCountryPhoneNew' MaxLength='3' style='float:left;width:20% !important;margin-left:4px;' />*/+"<input type='tel'  placeholder='Area' MaxLength='5' data-theme='c' value='"+additionalareaCodesArr[addPhoneLength]+"' id='addAreaPhoneNew' style='float:left;width:15% !important;margin-left:2px;' /><input data-theme='c' type='tel' placeholder='Phone' MaxLength='10' value='"+additonalPhoneNumbersArr[addPhoneLength]+"' id='addPhoneValueNew' style='float:left;width:23% !important;margin-left:2px;'/> <input type='tel' data-theme='c' placeholder='Ext' value='"+additionalExtensionsArr[addPhoneLength]+"' id='addExtensionValueNew' MaxLength='4' style='float:left;width:10% !important;margin-left:2px;'/></div>";
    
    html_view += "<div class='update-holder'><input type='tel'  placeholder='Area' MaxLength='3' data-theme='c' value='"+additionalareaCodesArr[addPhoneLength]+"' id='addAreaPhoneNew' style='float:left;width:15% !important;margin-left:2px;' /><input data-theme='c' type='tel' placeholder='Phone' MaxLength='7' value='"+additonalPhoneNumbersArr[addPhoneLength]+"' id='addPhoneValueNew' style='float:left;width:23% !important;margin-left:2px;'/> <input type='tel' data-theme='c' placeholder='Ext' value='"+additionalExtensionsArr[addPhoneLength]+"' id='addExtensionValueNew' MaxLength='4' style='float:left;width:10% !important;margin-left:2px;'/></div>";
    
    
    
    // html_view += "<div style='width:100%;float:left;' style='font-size:10px;'><div style='float:left;width:10% !important;margin-left:5px;'>Country</div><div style='float:left;width:18% !important;margin-left:34px;'>Area</div><div style='float:left;width:18% !important;'>Phone</div><div style='float:left;width:35% !important;margin-left:15px;text-align:center;'>Extension</div></div>";
    
  //  html_view += "<div class='update-holder'>International Access Code</div>";
  //  html_view += "<div class='update-holder'><input type='tel' placeholder='International Access Code' value='"+additionalIntAccessArr[addPhoneLength]+"' id='addInternationalAccessNew' MaxLength='5' data-theme='c' /></div></div></div>";
    html_view += "<div class='update-holder'><div id='addUpdateClick' class='add_address_btn' data-role='button' data-theme='f'  ><span>Update</span></div><div class='reset_btn' data-role='button' data-theme='a' onclick='resetAddressDetail();' >Reset</div>";
    document.getElementById('saveAddress').innerHTML = html_view;
    $("#addressTitle").html("Additional Number");
    $.mobile.changePage('index.html#setAddress',{transition:"none"});
    $('#setAddress').trigger('create');
    
    
    
    setTimeout(function(){$('#teleTypeCode').val(additionalTeleCodesArr[addPhoneLength]);$('#teleTypeCode').selectmenu('refresh');},200) // update teletype value after dom ready
    
    var addressTypeCode = decodeURIComponent(addressTypeCode);
    var sequence = decodeURIComponent(sequence);
    var fromDate = decodeURIComponent(fromDate);
    var toDate = decodeURIComponent(toDate);
    var houseNumber = decodeURIComponent(houseNumber);
    var street1 = decodeURIComponent(street1);
    var street2 = decodeURIComponent(street2);
    var street3 = decodeURIComponent(street3);
    var street4 = "";//decodeURIComponent(street4);
    var city = decodeURIComponent(city);
    var stateCode = decodeURIComponent(stateCode);
    var zip = decodeURIComponent(zip);
    var countyCode = decodeURIComponent(countyCode);
    var nationCode = decodeURIComponent(nationCode);
    var phoneCountryCode = decodeURIComponent(phoneCountryCode);
    var phoneAreaCode = decodeURIComponent(phoneAreaCode);
    var phoneNumber = decodeURIComponent(phoneNumber);
    var extension = decodeURIComponent(extension);
    var internationalAccessCode = decodeURIComponent(internationalAccessCode);
    var unlistedInd = "";
    var primaryPhoneSeq = decodeURIComponent(primaryPhoneSeq);
    var primaryPhoneCode = decodeURIComponent(primaryPhoneCode);
    
    
    /** to click inherit model function **/
    $('#addUpdateClick').click(function(){
                               
                               updateAddNewContact();
                               
                               });
    /**----------------------------**/
    
    
    function updateAddNewContact()
    {
        startActivitySpinner();
        url_service = "/enmu/services/student/banneraddressupdate";
        return_Where = newAddAddressModel;
        request_ST(url_service, return_Where, false);
    }
    
    
    function newAddAddressModel(serviceTicket)
    {

        var a = $('#teleTypeCode').val();
        if (a == "Select") {
            navigator.notification.alert("Please select a phone number type.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }
        var b= "";
        
        /*var c = $('#addCountryPhoneNew').val();
        if (c.search(/^$|^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }*/
        var d = $('#addAreaPhoneNew').val();
        if (d.search(/^[0-9 ]+$/) === -1 || d.length != 3) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }
        var e = $('#addPhoneValueNew').val();
        if (e.search(/^[0-9 ]+$/) === -1 || e.length != 7) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }
        var f = $('#addExtensionValueNew').val();
        if (f.search(/^$|^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }
       /* var g = $('#addInternationalAccessNew').val();
        if (g.search(/^$|^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Enter numbers in international access Code.",null,"Add Phone Number",'Ok');
            stopActivitySpinner();
            return;
        }*/
        var g ="";
        var h ="";
        
        
        additionalTeleCodesArr[addPhoneLength] = a;
        var additionalTeleCodes = additionalTeleCodesArr;
        
        /*additionalCountryCodesArr[addPhoneLength] = c;
        var additionalCountryCodes = additionalCountryCodesArr;*/
        
        additionalareaCodesArr[addPhoneLength] = d;
        var additionalareaCodes = additionalareaCodesArr;
        
        additonalPhoneNumbersArr[addPhoneLength] = e;
        var additonalPhoneNumbers = additonalPhoneNumbersArr;
        
        additionalExtensionsArr[addPhoneLength] = f;
        var additionalExtensions = additionalExtensionsArr;
        
        additionalIntAccessArr[addPhoneLength] = g;
        var additionalIntAccess = additionalIntAccessArr;
        
        var additionalUnlistedInd = decodeURIComponent(additionalUnlistedIndAdd.replace(/null/g,""));
        
        var addDelInd = decodeURIComponent(additionalUnlistedIndAdd.replace(/null/g,"")); // sent same blank value as unlisted indicator
        var additionalSequence = decodeURIComponent(additionalSequenceAdd);
        
        
        var server_url = serviceName+"/enmu/services/student/banneraddressupdate"+"?ticket="+serviceTicket;
        var storeUpdateData = "seqno="+sequence+"&";

         //console.log("data passed in address update--> "+storeUpdateData+"atypcode="+addressTypeCode+"&fromdate="+fromDate+"&todate="+toDate+"&housenumber="+houseNumber+"&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addlseqnos="+additionalSequence+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes="+additionalCountryCodes+"&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+"&addldelinds="+addDelInd);
        
        console.log("data passed in address update--> "+storeUpdateData+"atypcode="+addressTypeCode+"&fromdate="+fromDate+"&todate="+toDate+"&housenumber="+houseNumber+"&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addlseqnos="+additionalSequence+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes=&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+"&addldelinds="+addDelInd);
        
        //startActivitySpinner();
        var xhr=$.ajax({
                       url: server_url,
                       contentType: 'application/json;charset=UTF-8',
                       type: 'POST',
                       data: storeUpdateData+"atypcode="+addressTypeCode+"&fromdate="+fromDate+"&todate="+toDate+"&housenumber="+houseNumber+"&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addlseqnos="+additionalSequence+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes=&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+"&addldelinds="+addDelInd,
                       beforeSend:function (){
                       setTimeout(function(){
                                  xhr.abort();
                                  },globalAjaxTimer);},
                       success: function(data)
                       {
                       console.log('success editAddUpdateAddress--> ');
                       console.log(data);
                       console.log(JSON.stringify(data));
                       if(data)
                       {
                       navigator.notification.alert('Phone number added successfully.',null,'Address Update','Ok');
                       }
                       url_service="/enmu/services/student/banneraddressfetch";
                       return_Where = getAddress;
                       request_ST(url_service,return_Where,false);
                       },
                       error: function(e)
                       {
                       console.log('error editAddUpdateAddress--> ');
                       console.log(e);
                       stopActivitySpinner();
                       alertServerError(e.status);
                       }
                       });
        
    }
    
}

/**
 *function to disable the other fields in case of country US
 */
function disableFields(str)
{
    var initialState = window.localStorage.getItem("INITIAL_UPDATE_ADDRESS_STATE");
    var initialCountry = window.localStorage.getItem("INITIAL_UPDATE_ADDRESS_COUNTRY");
    var changeCountry = $('#nationValue').val();
    
    //alert(initialCountry+" "+$('#nationValue').val()+" "+countryUSA+" "+changeCountry)
    if(initialCountry == ""){
        initialCountry = countryUSA;
    }
   
    if(str == 'one')
    {
//       if($('#nationValue').val() != countryUSA)
//       {
//           //$('#zipCode').val('');
//           //$('#zipCode').attr('disabled',true);
//           $('.new_address_citystate_showhide').hide();
//          // $('#stateDispVal').attr('disabled',true);
//          // $('#countyDispVal').attr('disabled',true);
//           var stateValue =  $('#stateDispVal').val();
//           /** NOTE: here is the case if backend gives us country blank and state exists or country is not us and state is exists **/
//           if(stateValue != 'Select')
//           {
//               $('#stateDispVal').val();
//               $('#stateValue').val();
//           }
//           else
//           {
//           $('#stateDispVal').val('');
//           $('#stateValue').val('');
//           }
//           
//           $('#countyDispVal').val('');$('#countyValue').val('');
//           //$('#zipAddressMandatoryStar').hide();
//       }
//       else
//       {
//           $('.new_address_citystate_showhide').show();          
//       }
        if(initialCountry == changeCountry)
        {
            $('#stateDispVal').val(getAddressCodeName(null, null,null, initialState, null));
            $('#stateValue').val(initialState);
        } else{
           // $('#stateDispVal').val('');
           // $('#stateValue').val('');
        }
        if($('#nationValue').val() == countryUSA){
            $('.new_address_citystate_showhide').show();
        }else{
           // $('.new_address_citystate_showhide').hide();
        }
    }
    else if($('#nationValue').val() != countryUSA)
    {
        //$('#zipCode').val('');
        //$('#zipCode').attr('disabled',true);
        //$('.new_address_citystate_showhide').hide();
        // $('#stateDispVal').attr('disabled',true);
        // $('#countyDispVal').attr('disabled',true);
       // $('#stateDispVal').val('');
        $('#countyDispVal').val('');$('#stateValue').val('');$('#countyValue').val('');
       // $('#zipAddressMandatoryStar').hide();
        
    }
    else
    {
        //$('#zipCode').val();
        //$('#zipCode').attr('disabled',false);
        $('#stateDispVal').attr('disabled',false);
        $('#countyDispVal').attr('disabled',false);
        $('.new_address_citystate_showhide').show();

        //$('#stateDispVal').val('');$('#countyDispVal').val('');
    }
}