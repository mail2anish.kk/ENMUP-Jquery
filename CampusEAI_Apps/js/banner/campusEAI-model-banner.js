//-------------------- Code for Student Registration ------------------//

/**
 * studentRegistration()
 * <function to get list of student from server>
 * @param {string} serviceTicket genetaed after successfull login
 * @return {JSON}
 */

function studentRegistration(serviceTicket) {
    
    var server_url= serviceName+"/enmu/services/student/bannertermsForRegistrationLookup"+"?ticket="+serviceTicket;
    
    console.log(server_url);
    
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function () {
                   setTimeout(function() {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data) {
                   dispStudentRegList(data);
                   stopActivitySpinner();
                   
                   },
                   
                   error: function(e) {
                   stopActivitySpinner();
                   dispStudentRegList();
                   }
                   });
}


/**
 * getRegCRNList()
 * <function to get crn list from server>
 * @param {string} termCode
 * @return {JSON}
 */

function getRegCRNList(termCode) {
    
    if(termCode != "") {
        regTermCode = termCode;
        dispStdRegOptionsDetails();
    } else {
        $("#studentRegistrationDetailsMain").empty();
    }
}


/**
 * getLookupClassesData()
 * <function to get look up classes data from server>
 * @return {JSON}
 */

// function getLookupClassesData() {
//
// startActivitySpinner();
//
// server_url= serviceName+"/enmu/services/student/bannerclassLookup/termCode="+regTermCode;
// var xhr=$.ajax({
// url: server_url,
// type: "GET",
// dataType:"JSON",
// async:true,
// data:{},
// beforeSend:function (){
// // alert(globalAjaxTimer);
// setTimeout(function()
// {
// xhr.abort();
// },globalAjaxTimer);
// },
//
// success: function(data) {
// //console.log(JSON.stringify(data));
// dispLookUpClasses(data);
// stopActivitySpinner();
// },
//
// error: function(e) {
// stopActivitySpinner();
// //alertServerError(e.status);
// }
// });
// }


/// THIS APPENDS THE TEXT BOX

function showRegLookupClassesFilterFormData(serviceTicket)
{
    // if($("#showLookupClassesFilterFormData").find("select")[0])
    // {
    // console.log("FORM HAS ALREADY BEEN MADE, ABORTING");
    // return;
    // }
    
    var html = '<div style="width: 75%;margin: 10px auto;">\
    <select type="select" data-theme="f" class="studentRegClassLookupSubjectSel_h">\
    <option value="">Loading Subjects</option>\
    </select>\
    <div style="margin-top:8px";>\
    <div style="margin-left:5px;width:40%;text-shadow: none; " data-role="button" data-inline="true" data-theme="a" onClick="cancelRegLookupClassesFilter()">Cancel</div>\
    <div style="margin-right:5px;width:40%;float:right;text-shadow: none;" data-role="button" data-inline="true" data-theme="f" onClick="pickRegLookupClassesFilterFormData()">Look Up</div></div>\
    </div>';
    //$("#showLookupClassesFilterFormData").show();
    $("#showLookupClassesFilterFormData").html(html);
    $("#showLookupClassesFilterFormData").find("select").selectmenu();
    $('.lookup_container_h').addClass('lookup_container');
    $('.lookup_container_h').trigger('create');
    
    var id = $("#studentRegistrationInOption").val();
    if(!id || id=="Select") return;
    server_url= serviceName+"/enmu/services/student/bannergetOtherClassSearchParams/termCode="+id+"?ticket="+serviceTicket;
    console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   {
                   var html2 = '<option value="">-Select Subject-</option>';
                   if(data.subjectDropDown && data.subjectDropDown.length)
                   {
                   for(var j=0; j < data.subjectDropDown.length; j++)
                   {
                   html2 += '<option value="'+data.subjectDropDown[j].SUBJECT_CODE+'">'+data.subjectDropDown[j].SUBJECT_DESC+'</option>'
                   }
                   }
                   $("#showLookupClassesFilterFormData").find("select").html(html2)
                   $("#showLookupClassesFilterFormData").find("select").selectmenu('refresh');
                   },
                   error: function(e)
                   {
                   alertServerError(e.status);
                   }
                   
                   });
    
    
}

function cancelRegLookupClassesFilter()
{
    $('.lookup_container_h').removeClass('lookup_container');
}

var appendData = {}
var optionalAppendData;
function pickRegLookupClassesFilterFormData()
{
    var code = $("#showLookupClassesFilterFormData").find("select").val();
    if(!code || code=="Select")
    {
        navigator.notification.alert('Please select a subject before searching.',doNothing,'Alert','Ok');
        return;
    }
    

    appendData.string = '&subjectCode='+code;
    appendData.exclude_key = code;
    optionalAppendData=appendData;
    getLookupClassesDataST()
    
}

function getLookupClassesDataST()
{
    url_service = "/enmu/services/student/bannerstudentDetailSchedule/termCode="+regTermCode;
    console.log(JSON.stringify("optionalAppened="+optionalAppendData));
    
    if(optionalAppendData && optionalAppendData.string)
    {
        url_service = url_service + optionalAppendData.string;
    }
    
    console.log("getLookupClassesDataST = "+url_service);
    return_Where = getLookupClassesData;
    request_ST(url_service,return_Where,false);

}


function getLookupClassesData(serviceTicket) {
    
    startActivitySpinner();
    
    var server_url= serviceName+"/enmu/services/student/bannerstudentDetailSchedule/termCode="+regTermCode;
    
    
    if(optionalAppendData && optionalAppendData.string)
    {
        server_url = server_url + optionalAppendData.string;
    }
    
    server_url=server_url+"?ticket="+serviceTicket;
    console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   currentClassData:{},
                   beforeSend:function () {
                   setTimeout(function() {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data) {
                   console.log(JSON.stringify(data));
                   bannerclassLookupST(data);
                   
                   },
                   
                   error: function(e) {
                   stopActivitySpinner();
                   //alertServerError(e.status);
                   }
                   });
}

var currentClassData;
function bannerclassLookupST(data)
{
    currentClassData=data;
    url_service = "/enmu/services/student/bannerclasslookup/termCode="+regTermCode;
    if(optionalAppendData && optionalAppendData.string)
    {
        url_service = url_service + optionalAppendData.string;
    }
    console.log(url_service);
    return_Where = bannerClassLUC;
    request_ST(url_service,return_Where,false);
}

function bannerClassLUC(serviceTicket)
{
    var server_url= serviceName+"/enmu/services/student/bannerclasslookup/termCode="+regTermCode;
    console.log(JSON.stringify(server_url));
    if(optionalAppendData && optionalAppendData.string)
    {
        server_url = server_url + optionalAppendData.string;
    }
    
    server_url=server_url+"?ticket="+serviceTicket;
    
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   lookupClassData:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(lookupClassData) {
                   console.log(JSON.stringify(lookupClassData));
                   dispLookUpClasses(lookupClassData, currentClassData);
                   stopActivitySpinner();
                   },
                   
                   error: function(e) {
                   stopActivitySpinner();
                   //alertServerError(e.status);
                   }
                   });
}

var ClassCrnValueReg;
function registerLUCStudentST(registerBtnId)
{
    var termCode = regTermCode;
    ClassCrnValueReg= $('#lookupcrn'+registerBtnId).html();
    var url_service = "/enmu/services/student/bannerregisterStudent";
    console.log("url_json :"+url_service);
    return_Where=registerLookUpClasses;
    request_ST(url_service,return_Where,false);
}
/**
 * registerLookUpClasses()
 * <function to register classes to the server>
 * @param {string} termCode of subject
 * @param {string} crn number
 * @return {JSON}
 */

function registerLookUpClasses(serviceTicket) {
    
    var termCode = regTermCode;
    var crnValue = ClassCrnValueReg;
    startActivitySpinner();
    var url_json = serviceName+"/enmu/services/student/bannerregisterStudent?ticket="+serviceTicket;
    
    console.log("url_json :"+url_json);
    console.log("termCode :"+termCode);
    console.log("crnValue :"+crnValue);
    try{
        $.ajax(
               {
               url:url_json,
               dataType:'JSON',
               async:true,
               type:"POST",
               data:{termCode:termCode,crn:crnValue},
               beforeSend:function () {
               setTimeout(function() {
                          xhr.abort();
                          },globalAjaxTimer);
               },
               
               success:function(response) {
               console.log(JSON.stringify(response));
               if(response.p_update_out == 'Y') {
               navigator.notification.alert(response.p_msg_out,doNothing,'Add / Drop Classes','Ok');
               } else if(response.p_update_out == 'N') {
               if (response.p_msg_out == "INVALID LEVEL FOR COURSE") {
               navigator.notification.alert("You cannot currently register for this class. Please contact your advisor for assistance.", doNothing, 'Add / Drop Classes', 'Ok');
               } else {
               navigator.notification.alert(response.p_msg_out, doNothing, 'Add / Drop Classes', 'Ok');
               }
               }
               stopActivitySpinner();
               },
               error:function(response) {
               console.log(JSON.stringify(response));
               //alertServerError(e.status);
               stopActivitySpinner();
               },
               
               });
    }catch(e){}
}

function getCurrentClassesDataST()
{
    startActivitySpinner();
    url_service="/enmu/services/student/bannerstudentDetailSchedule/termCode="+regTermCode;
    return_Where=getCurrentClassesData;
    request_ST(url_service,return_Where,false);
}

function getCurrentClassesData(serviceTicket) {
    server_url= serviceName+"/enmu/services/student/bannerstudentDetailSchedule/termCode="+regTermCode+"?ticket="+serviceTicket;
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data) {
                   console.log(JSON.stringify(data));
                   dispCurrentClasses(data);
                   stopActivitySpinner();
                   },
                   
                   error: function(e) {
                   stopActivitySpinner();
                   //alertServerError(e.status);
                   }
                   });
}
function submitRegistrationDataST()
{
    url_service="/enmu/services/student/bannerregisterStudent";
    return_Where=submitRegistrationData;
    request_ST(url_service,return_Where,false);
}


function submitRegistrationData(serviceTicket) {
    
    startActivitySpinner();
    var termCode = regTermCode;
    
    var crn1 = $("#crn1").val();
    var count = -1;
    var crnArray = "";
    if(crn1 == "") {
        navigator.notification.alert("Please enter a CRN.",doNothing,'Add Class','Ok');
        stopActivitySpinner();
    }  else if(crn1.length > 5 || crn1.length <= 4){
        navigator.notification.alert("This CRN is not valid. Look up classes below to find a valid course number.",doNothing,'Registration Error','Ok');
        stopActivitySpinner();
    }
    else if(crn1 != ""){
        crnArray = crn1;
        var url_json = serviceName+"/enmu/services/student/bannerregisterStudent?ticket="+serviceTicket;
        console.log("url_json :"+url_json);
        console.log("termCode :"+termCode);
        console.log("crnValue :"+crnArray);
        try{ $.ajax(
                    {
                    url:url_json,
                    dataType:'JSON',
                    async:true,
                    type:"POST",
                    
                    data:{termCode:termCode,crn:crnArray},
                    
                    beforeSend:function () {
                    setTimeout(function() {
                               xhr.abort();
                               },globalAjaxTimer);
                    },
                    
                    success:function(response) {
                    console.log(JSON.stringify(response));
                    if(response.p_update_out == 'Y') {
                    navigator.notification.alert(response.p_msg_out,doNothing,'Add / Drop Classes','Ok');
                    
                    } else if(response.p_update_out == 'N') {if (response.p_msg_out == "INVALID LEVEL FOR COURSE") {
                    navigator.notification.alert("You cannot currently register for this class. Please contact your advisor for assistance.", doNothing, 'Add / Drop Classes', 'Ok');
                    }else{
                    navigator.notification.alert(response.p_msg_out,doNothing,'Add / Drop Classes','Ok');
                    }
                    } else {
                    navigator.notification.alert("This class is not offered during the semester you selected.",null,"Class Not Offered",'Ok');
                    }
                    $('#crn1').val('');
                    stopActivitySpinner();
                    },
                    error:function(response) {
                    //alert("error"+JSON.stringify(response));
                    //alertServerError(e.status);
                    stopActivitySpinner();
                    },
                    
                    });
        }catch(e){}
        
    }
}

var dropCRN;
function dropRegistrationClassesST(crn)
{
    dropCRN=crn
    url_service="/enmu/services/student/bannerdropStudent";
    return_Where=dropRegistrationClasses;
    request_ST(url_service,return_Where,false);
}


function dropRegistrationClasses(serviceTicket) {
    
    var termCode = regTermCode;
    var crnValue = dropCRN;
    startActivitySpinner();
    var url_json = serviceName+"/enmu/services/student/bannerdropStudent?ticket="+serviceTicket;
    
    console.log("url_json :"+url_json);
    console.log("termCode :"+termCode);
    console.log("crnValue :"+crnValue);
    
    try{
        $.ajax(
               {
               url:url_json,
               dataType:'JSON',
               async:true,
               type:"POST",
               
               data:{termCode:termCode,crn:crnValue},
               
               beforeSend:function () {
               setTimeout(function() {
                          xhr.abort();
                          },globalAjaxTimer);
               },
               
               success:function(response) {
               console.log(JSON.stringify(response));
               if(response.p_update_out == 'Y') {
               //var res=response.p_update_out.toLowerCase();
               //res=res.substr(0, 1).toUpperCase() + res.substr(1);
               navigator.notification.alert(response.p_msg_out,doNothing,'Add / Drop Classes','Ok');
               //getCurrentClassesData();
               getCurrentClassesDataST();
               //$.mobile.changePage("index.html#studentRegistrationDetailsPage");
               } else if(response.p_update_out == 'N') {
               navigator.notification.alert(response.p_msg_out,doNothing,'Add / Drop Classes','Ok');
               }
               stopActivitySpinner();
               },
               error:function(response) {
               //alert("error"+JSON.stringify(response));
               //alertServerError(e.status);
               stopActivitySpinner();
               },
               
               });
    }catch(e){}
}


//-------------------- Code for Classroom Availability ------------------//


/**
 * getStudentTermCode()
 * <function to get list of term code from server>
 * @param {string} serviceTicket genetaed after successfull login
 * @return {JSON}
 */

function getStudentTermCode(serviceTicket) {
    
    var server_url= serviceName+"/enmu/services/student/bannertermsForRegistrationLookup"+"?ticket="+serviceTicket;
    
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function () {
                   setTimeout(function() {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data) {
                   dispStudentTermCode(data);
                   stopActivitySpinner();
                   },
                   
                   error: function(e) {
                   stopActivitySpinner();
                   }
                   });
}


function getClassroomAvailDataTicket(stdTermCode) {
    
    regTermCode = stdTermCode;
    if(stdTermCode != "") {
        startActivitySpinner();
        classroomAddDataTicketCount = classroomAddDataTicketCount + 1;
        url_service="/enmu/services/student/bannerclassroomAvailabilityFilterCriteria";
        return_Where=getclassroomAddData;
        request_ST(url_service,return_Where,false);
    } else {
        $("#classroomAvailabilityMain").empty();
        stopActivitySpinner();
    }
}

function getclassroomAddData(serviceTicket) {
    
    var server_url;
    if(classroomAddDataTicketCount == "1") {
        server_url= serviceName+"/enmu/services/student/bannerclassroomAvailabilityFilterCriteria"+"?ticket="+serviceTicket;
    } else {
        server_url= serviceName+"/enmu/services/student/bannerclassroomAvailabilityFilterCriteria";
    }
    
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function () {
                   setTimeout(function() {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data) {
                   console.log(JSON.stringify(data));
                   showClassroomAvailabilityPage(data);
                   stopActivitySpinner();
                   },
                   
                   error: function(e) {
                   console.log(JSON.stringify(e));
                   stopActivitySpinner();
                   }
                   });
    
    
}

function getClassAvailDataTicket() {
    startActivitySpinner();
    serviceTicketCount = serviceTicketCount + 1;
    url_service="/enmu/services/student/bannerclassroomAvailability/termCode="+regTermCode;
    return_Where=getClassroomAvailData;
    request_ST(url_service,return_Where,false);
}

function getClassroomAvailData(serviceTicket) {
    
    console.log(serviceTicket);
    //startActivitySpinner();
    var server_url;
    
    
    var days;
    var weekDayArray = "";
    var startTime = $('#classroomStartTime').val();
    var endTime = $('#classroomEndTime').val();
    var bldgCode = $('#buildingCodesDropDown').val();
    var campusCode = $('#campusCodesDropDown').val();
    var siteCode = $('#siteCodesDropDown').val();
    var capacity = $('#classroomCapacity').val();
    var roomAttrs;
    
    if((startTime > 2400) || (endTime > 2400)) {
        navigator.notification.alert("Start and End Time should be in 24 hour format",doNothing,'Classroom Availability','Ok');
        stopActivitySpinner();
        return;
    }
    if( $('#selectedRoomAttr').is(':empty') ) {
        roomAttrs = "";
    } else {
        roomAttrs = localStorage.roomAttrArray;
    }
    
    if(startTime === undefined) {
        startTime ="";
    }
    if(endTime === undefined) {
        endTime ="";
    }
    if(bldgCode === undefined) {
        bldgCode ="";
    }
    if(campusCode === undefined) {
        campusCode ="";
    }
    if(siteCode === undefined) {
        siteCode ="";
    }
    if(capacity === undefined) {
        capacity ="";
    }
    if((roomAttrs == undefined) && (roomAttrs == null)) {
        roomAttrs ="";
    }
    if ($('#monday').is(":checked")) {
        weekDayArray +="M";
    }
    if ($('#tuesday').is(":checked")) {
        weekDayArray +="T";
    }
    if ($('#wednesday').is(":checked")) {
        weekDayArray +="W";
    }
    if ($('#thursday').is(":checked")) {
        weekDayArray +="T";
    }
    if ($('#friday').is(":checked")) {
        weekDayArray +="F";
    }
    
    // if(serviceTicketCount == "1") {
    //
    // //server_url= serviceName+"/enmu/services/student/bannerclassroomAvailability/termCode="+regTermCode+"?ticket="+serviceTicket;
    // server_url= serviceName+"/enmu/services/student/bannerclassroomAvailability/termCode="+regTermCode+"&days="+weekDayArray+"&startTime="+startTime+"&endTime="+endTime+"&bldgCode="+bldgCode+"&campusCode="+campusCode+"&siteCode="+siteCode+"&capacity="+capacity+"&roomAttrs="+roomAttrs+"?ticket="+serviceTicket;
    // } else {
    //server_url= serviceName+"/enmu/services/student/bannerclassroomAvailability/termCode="+regTermCode;
    server_url= serviceName+"/enmu/services/student/bannerclassroomAvailability/termCode="+regTermCode+"&days="+weekDayArray+"&startTime="+startTime+"&endTime="+endTime+"&bldgCode="+bldgCode+"&campusCode="+campusCode+"&siteCode="+siteCode+"&capacity="+capacity+"&roomAttrs="+roomAttrs;
    //}
    console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function () {
                   setTimeout(function() {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data) {
                   console.log("success"+JSON.stringify(data));
                   dispClassroomAvailData(data);
                   setTimeout(function(){stopActivitySpinner()},2000);
                   },
                   
                   error: function(e) {
                   console.log("error"+JSON.stringify(e));
                   setTimeout(function(){stopActivitySpinner()},2000);
                   }
                   });
}

/**************************************************** Emergency contact details Model Functions *************************************************/

/**
 *To access emergency contacts
 */
function getEmergencyContact(serviceTicket)
{
    var server_url = serviceName+"/enmu/services/student/banneremergencycontactfetch"+"?ticket="+serviceTicket;
    var xhr=$.ajax({
                   url: server_url,
                   type: 'GET',
                   //data:{},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   success: function(data)
                   {
                   console.log(JSON.stringify(data));
                   emergencyContacts(data);
                   },
                   error: function(e)
                   {
                   stopActivitySpinner();
                   alertServerError(e.status);
                   }
                   
                   });
}


/**
 *To access emergency contact codes
 */
function getEmergencyContactCodes(serviceTicket)
{
    //startActivitySpinner();
    var server_url = serviceName+"/enmu/services/student/banneremergencycontactcodes"+"?ticket="+serviceTicket;
    var xhr=$.ajax({
                   url: server_url,
                   type: 'GET',
                   //data:{},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   success: function(data) {
                   //stopActivitySpinner();
                   console.log(JSON.stringify(data));
                   var isEmptyData = jQuery.isEmptyObject(data);
                   var isEmptyCountry = jQuery.isEmptyObject(data.countrycodes);
                   var isEmptyState = jQuery.isEmptyObject(data.statecodes);
                   var isEmptyRelation = jQuery.isEmptyObject(data.relationshipcodes);
                   
                   if(!data || data.length == 0 || typeof(data.error) != 'undefined' || isEmptyData || isEmptyCountry || isEmptyState || isEmptyRelation)
                   {
                   navigator.notification.alert('We seem to be unable to connect to MyENMU right now. Please try again later.',null,'Oops!','Ok');
                   stopActivitySpinner();
                   }
                   else
                   {
                   window.localStorage.setItem("EMRGNCY_JSON",JSON.stringify(data));
                   url_service="/enmu/services/student/banneremergencycontactfetch";
                   return_Where = getEmergencyContact;
                   request_ST(url_service,return_Where,false);
                   }
                   },
                   error: function(e) {
                   //stopActivitySpinner();
                   alertServerError(e.status);
                   }
                   
                   });
    
    
}

/**
 *Dispaly Emergency contact codes in select option of contact details
 */
function getContactCodesJSON()
{
    //startActivitySpinner();
    var data = window.localStorage.getItem("EMRGNCY_JSON");
    data = jQuery.parseJSON(data);
    var jsonKey = ["countrycodes","statecodes","relationshipcodes"];
    
    
    if(Object_keys(data, 0) == jsonKey[0])
    {
        var html_data = "<option>Select</option>";
        html_data += "<option value='"+countryUSA+"'>United States of America</option>";
        html_data += "<option value='"+countryCanada+"'>Canada</option>";
        for(var i=0;i<data.countrycodes.length;i++)
        {
            if(!(data.countrycodes[i].COUNTRY_CODE == countryUSA || data.countrycodes[i].COUNTRY_CODE == countryCanada)){
                html_data += "<option value='"+data.countrycodes[i].COUNTRY_CODE+"'>"+data.countrycodes[i].COUNTRY_DESC+"</option>";
            }
        }
        $('#country').empty();
        $('#country').html(html_data);
    }
    
    if(Object_keys(data, 1) == jsonKey[1])
    {
        var html_data = "<option>Select</option>";
        for(var i=0;i<data.statecodes.length;i++)
        {
            html_data += "<option value='"+data.statecodes[i].STATE_CODE+"'>"+data.statecodes[i].STATE_DESC+"</option>";
        }
        $('#state').empty();
        $('#state').html(html_data);
    }
    
    if(Object_keys(data, 2) == jsonKey[2])
    {
        var html_data = "<option>Select</option>";
        for(var i=0;i<data.relationshipcodes.length;i++)
        {
            if(data.relationshipcodes[i].RELATIONSHIP_DESC != "Multiple"){
                html_data += "<option value='"+data.relationshipcodes[i].RELATIONSHIP_CODE+"'>"+data.relationshipcodes[i].RELATIONSHIP_DESC+"</option>";
            }
        }
        $('#relation').empty();
        $('#relation').html(html_data);
        setTimeout(function(){stopActivitySpinner()},2000);
    }
    
}


/**
 *get name of code values in emergency contacts
 */
function getEmegencyCodeName(countryKey, stateKey, relationKey)
{
    var data = window.localStorage.getItem("EMRGNCY_JSON");
    data = jQuery.parseJSON(data);
    var jsonKey = ["countrycodes","statecodes","relationshipcodes"];
    
    if(countryKey)
    {
        if(Object_keys(data, 0) == jsonKey[0])
        {
            for(var i=0;i<data.countrycodes.length;i++)
            {
                if(data.countrycodes[i].COUNTRY_CODE == countryKey)
                {
                    return data.countrycodes[i].COUNTRY_DESC;
                }
            }
            
        }
    }
    
    
    if(stateKey)
    {
        //console.log("State Code= "+JSON.stringify(data.statecodes));
        
        if(Object_keys(data, 1) == jsonKey[1])
        {
            for(var i=0;i<data.statecodes.length;i++)
            {
                if(data.statecodes[i].STATE_CODE == stateKey)
                {
                    
                    return data.statecodes[i].STATE_DESC;
                }
                
            }
        }
    }
    
    
    if(relationKey)
    {
        if(Object_keys(data, 2) == jsonKey[2])
        {
            for(var i=0;i<data.relationshipcodes.length;i++)
            {
                if(data.relationshipcodes[i].RELATIONSHIP_CODE == relationKey)
                {
                    return data.relationshipcodes[i].RELATIONSHIP_DESC;
                }
            }
        }
    }
    
}

/**
 *To save update contacts
 */
function setEmergencyContact(serviceTicket)
{
    var relation = ($('#relation').val()=='Select')?null:$('#relation').val();
    if(relation == null)
    {
        navigator.notification.alert("Please select a relationship type.",null,"Emergency Contacts",'Ok');
        stopActivitySpinner();
        return;
    }
    
    var lastNameVal = $('#last_name').val() ? $('#last_name').val().trim() : '';
    var firstNameVal = $('#first_name').val() ? $('#first_name').val().trim() : '';
    var middleNameVal = $('#middle_name').val() ? $('#middle_name').val().trim() :'';
    
    
    
    if (!firstNameVal)
    {
        navigator.notification.alert("Please enter a first name.",null,"Emergency Contacts",'Ok');
        stopActivitySpinner();
        return;
    }else{
        if(/^[a-zA-Z- ]*$/.test(firstNameVal) == false) {
            navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in name.',doNothing,'Emergency Contacts','Ok');
            stopActivitySpinner();
            return;
        }
    }
    
    if (!lastNameVal)
    {
        navigator.notification.alert("Please enter a last name.",null,"Emergency Contacts",'Ok');
        stopActivitySpinner();
        return;
    }else{
        if(/^[a-zA-Z- ]*$/.test(lastNameVal) == false) {
            navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in name.',doNothing,'Emergency Contacts','Ok');
            stopActivitySpinner();
            return;
        }
    }
    
    if (!middleNameVal)
    {

    }else{
        if(/^[a-zA-Z- ]*$/.test(middleNameVal) == false) {
            navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in name.',doNothing,'Emergency Contacts','Ok');
            stopActivitySpinner();
            return;
        }
    }
    
    
    
    
    /*var country = $('#country').val();
     if(country == "Select")
     {
     navigator.notification.alert("Please select a Country",null,"Emergency Contact",null);
     stopActivitySpinner();
     return;
     }*/
    var country = '157'
    
    var zip = $('#zip').val() ? $('#zip').val().trim() : '';
    var state = ($('#state').val()=='Select')? '' : $('#state').val();
    
    
    //if(country == countryUSA)
    //{
    if(state != ''){
        if(zip.length == 5){
            
        }else{
            var zipPattern = new RegExp("^\\d{5}(-\\d{4})?$");
            //var zipPattern1 = /^[0-9]{5}$/;
            //var zipPattern2 = /^[0-9]{4}$/;
            
            if(!zipPattern.test(zip))
            {
                navigator.notification.alert("Please enter a valid zip code.",null,"Zip Code",'Ok');
                stopActivitySpinner();
                return;
            }
            else
            {
                
            }
            
        }
    }
    //}
    /*else if(zip && !isValidPostalCode(zip, country))
     {
     navigator.notification.alert("Please enter a valid zip code.",null,"Zip Code",'Ok');
     stopActivitySpinner();
     return;
     }*/
    
    var street1 = $('#street1').val() ? $('#street1').val().trim() : '';
    var street2 = $('#street2').val() ? $('#street2').val().trim() : '';
    var street3 = $('#street3').val() ? $('#street3').val().trim() : '';
    
    
    if(/^[a-zA-Z0-9- ]*$/.test(street1) == false) {
        navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in address.',doNothing,'Address','Ok');
        stopActivitySpinner();
        return;
    }
    
    if(/^[a-zA-Z0-9- ]*$/.test(street2) == false) {
        navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in address.',doNothing,'Address','Ok');
        stopActivitySpinner();
        return;
    }
    
    if(/^[a-zA-Z0-9- ]*$/.test(street3) == false) {
        navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in address.',doNothing,'Address','Ok');
        stopActivitySpinner();
        return;
    }
    
    
    var cityVal = $('#city').val() ? $('#city').val().trim() : '';
    
    
    if(street1 && !cityVal)
    {
        navigator.notification.alert("Please enter City.",null,"Emergency Contact",'Ok');
        stopActivitySpinner();
        return;
    }
    
    if(!street1 && cityVal)
    {
        navigator.notification.alert("Please enter valid address.",null,"Address",'Ok');
        stopActivitySpinner();
        return;
    }
    
    
    
    if(street1 && !zip)
    {
        
        navigator.notification.alert("Please enter a valid zip code.",null,"Zip Code",'Ok');
        stopActivitySpinner();
        return;
    }
    if(!street1 && zip)
    {
        navigator.notification.alert("Please enter valid address.",null,"Address",'Ok');
        stopActivitySpinner();
        return;
    }
    if(street1 && zip && cityVal){
        if(zip.length == 5){
            
        }else{
            var zipPattern = new RegExp("^\\d{5}(-\\d{4})?$");
            //var zipPattern1 = /^[0-9]{5}$/;
            //var zipPattern2 = /^[0-9]{4}$/;
            
            if(!zipPattern.test(zip))
            {
                navigator.notification.alert("Please enter a valid zip code.",null,"Zip Code",'Ok');
                stopActivitySpinner();
                return;
            }
            else
            {
                
            }
            
        }
        
        if(/^[a-zA-Z0-9- ]*$/.test(cityVal) == false) {
            navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in city.',doNothing,'Address','Ok');
            stopActivitySpinner();
            return;
        }
    }
    
    
    var houseNumberVal = $('#house_number').val() ? $('#house_number').val().trim() : '';
    if(houseNumberVal.length > 10)
    {
        navigator.notification.alert("House number should be less than 10 characters.",null,"Emergency Contact",'Ok');
        stopActivitySpinner();
        return;
    }
    
    //var countryPhone = $('#country_phone').val() ? $('#country_phone').val().trim() : '';
    var countryPhone ='';
    var areaPhone = $('#area_phone').val() ? $('#area_phone').val().trim() : '';
    var phone = $('#phone').val() ? $('#phone').val().trim() : '';
    var extension = $('#extension').val() ? $('#extension').val().trim() : '';
    
    if(areaPhone.length == 0 || phone.length == 0)
    {
        navigator.notification.alert("Please enter a valid phone number.",null,"Emergency Contact",'Ok');
        stopActivitySpinner();
        return;
    }
    
    
    if(areaPhone.length != 3){
        navigator.notification.alert("Please enter a valid phone number.",null,"Emergency Contact",'Ok');
        stopActivitySpinner();
        return;
    }
    else if(areaPhone.search(/^[0-9]+$/) === -1) {
        navigator.notification.alert("Please enter a valid phone number.",null,"Emergency Contact",'Ok');
        stopActivitySpinner();
        return;
    }
    
    if(phone.length != 7){
        navigator.notification.alert("Please enter a valid phone number.",null,"Emergency Contact",'Ok');
        stopActivitySpinner();
        return;
    }
    else if(phone.search(/^[0-9]+$/) === -1) {
        navigator.notification.alert("Please enter a valid phone number.",null,"Emergency Contact",'Ok');
        stopActivitySpinner();
        return;
    }
    
    
    var priorityCode = $('#priority').val();
    var oldPriorityCode = $('#old_priority').val();
    var sur = $('#sur').val();
    
    
    //var street2 = "";
    //var street3 = "";
    //var address = $('#address').val();
    var address = "";
    
    var server_url = serviceName+"/enmu/services/student/banneremergencycontactsave"+"?ticket="+serviceTicket;
    
    console.log("priority="+priorityCode+"&oldPriority="+oldPriorityCode+"&lastName="+lastNameVal+"&firstName="+firstNameVal+"&middleName="+middleNameVal+"&houseNumber="+houseNumberVal+"&streetLine1="+street1+"&streetLine2="+street2+"&streetLine3="+street3+"&streetLine4="+address+"&city="+cityVal+"&stateCode="+state+"&countryCode="+country+"&zipCode="+zip+"&phoneArea="+areaPhone+"&phoneNumber="+phone+"&phoneExtn="+extension+"&reltCode="+relation+"&phoneCountryCode="+countryPhone);
    //startActivitySpinner();
    var xhr=$.ajax({
                   url: server_url,
                   contentType: 'application/json;charset=UTF-8',
                   type: 'POST',
                   //data:{oldPriority:oldPriorityCode, priority:priorityCode, lastName:lastNameVal, firstName:firstNameVal, middleName:middleNameVal, houseNumber:houseNumberVal, streetLine1:street1, streetLine2:street2, streetLine3:street3, streetLine4:address, city:cityVal, stateCode:state, countryCode:country, zipCode:zip, phoneArea:areaPhone, phoneNumber:phone, phoneExtn:extension, reltCode:relation, phoneCountryCode:countryPhone},
                   data:"priority="+priorityCode+"&oldPriority="+oldPriorityCode+"&lastName="+lastNameVal+"&firstName="+firstNameVal+"&middleName="+middleNameVal+"&houseNumber="+houseNumberVal+"&streetLine1="+street1+"&streetLine2="+street2+"&streetLine3="+street3+"&streetLine4="+address+"&city="+cityVal+"&stateCode="+state+"&countryCode="+country+"&zipCode="+zip+"&phoneArea="+areaPhone+"&phoneNumber="+phone+"&phoneExtn="+extension+"&reltCode="+relation+"&phoneCountryCode="+countryPhone,
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   success: function(data)
                   {
                   console.log('success setEmergencyContact--> ');
                   console.log(data);
                   if(data)
                   {
                   navigator.notification.alert('Your Emergency Contacts have been updated.',null,'Success!','Ok');
                   }
                   url_service="/enmu/services/student/banneremergencycontactfetch";
                   return_Where = getEmergencyContact;
                   request_ST(url_service,return_Where,false);
                   },
                   error: function(e)
                   {
                   console.log('error setEmergencyContact--> ');
                   console.log(e);
                   stopActivitySpinner();
                   alertServerError(e.status);
                   }
                   
                   
                   });
    
    
}


/**
 *To delete entry from contacts
 */
function deleteEmergencyContact(serviceTicket)
{
    //startActivitySpinner();
    var remove = $('#remove_priority').val();
    var server_url = serviceName+"/enmu/services/student/banneremergencycontactdelete"+"?ticket="+serviceTicket;
    var xhr=$.ajax({
                   url: server_url,
                   type: 'POST',
                   data:{priority:remove},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   success: function(data)
                   {
                   if(data)
                   {
                   navigator.notification.alert('Contact deleted successfully',null,'Emergency Contacts','Ok');
                   }
                   url_service="/enmu/services/student/banneremergencycontactfetch";
                   return_Where = getEmergencyContact;
                   request_ST(url_service,return_Where,false);
                   },
                   error: function(e)
                   {
                   stopActivitySpinner();
                   alertServerError(e.status);
                   }
                   
                   });
}


/************************************************************* Address update model functions *************************************************/


/**
 *To access address
 */
function getAddress(serviceTicket)
{
    var server_url = serviceName+"/enmu/services/student/banneraddressfetch"+"?ticket="+serviceTicket;
    var xhr=$.ajax({
                   url: server_url,
                   type: 'GET',
                   //data:{},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   success: function(data)
                   {
                   console.log(JSON.stringify(data));
                   if(data)
                   {
                   addressDisplay(data);
                   }
                   },
                   error: function(e)
                   {
                   stopActivitySpinner();
                   alertServerError(e.status);
                   }
                   
                   });
}





/**
 *To access address codes
 */
function getAddressContactCodes(serviceTicket)
{
    //startActivitySpinner();
    var server_url = serviceName+"/enmu/services/student/banneraddresscodes"+"?ticket="+serviceTicket;
    console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: 'GET',
                   //data:{},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   success: function(data)
                   {
                   console.log(JSON.stringify(data));
                   //stopActivitySpinner();
                   var isEmptyData = jQuery.isEmptyObject(data);
                   var isEmptyCounty = jQuery.isEmptyObject(data.countycodes);
                   var isEmptyTeleType = jQuery.isEmptyObject(data.telephonetypecodes);
                   var isEmptyNation = jQuery.isEmptyObject(data.nationcodes);
                   var isEmptyState = jQuery.isEmptyObject(data.statecodes);
                   var isEmptyAddressType = jQuery.isEmptyObject(data.addresstypecodes);
                   
                   if(!data || data.length == 0 || typeof(data.error) != 'undefined' || isEmptyData || isEmptyTeleType || isEmptyNation || isEmptyState || isEmptyAddressType)
                   {
                   navigator.notification.alert('We seem to be unable to connect to MyENMU right now. Please try again later.',null,'Oops!','Ok');
                   stopActivitySpinner();
                   }
                   else
                   {
                   window.localStorage.setItem("ADDRESS_JSON",JSON.stringify(data));
                   url_service="/enmu/services/student/banneraddressfetch";
                   return_Where=getAddress;
                   request_ST(url_service,return_Where,false);
                   }
                   },
                   error: function(e)
                   {
                   //stopActivitySpinner();
                   alertServerError(e.status);
                   }
                   
                   });
    
    
}

///**
// *Dispaly address codes in select options
// */
//function getAddressCodesJSON()
//{
//    var data = window.localStorage.getItem("ADDRESS_JSON");
//    data = jQuery.parseJSON(data);
//    var jsonKey = ["countycodes","telephonetypecodes","nationcodes","statecodes","addresstypecodes"];
//
//
//    if(Object_keys(data,0) == jsonKey[0])
//    {
//        var html_data = "<option>Select</option>";
//        for(var i=0;i<data.countycodes.length;i++)
//        {
//            html_data += "<option value='"+data.countycodes[i].COUNTY_CODE+"'>"+data.countycodes[i].COUNTY_DESC+"</option>";
//        }
//        $('#countyValue').empty();
//        $('#countyValue').html(html_data);
//    }
//
//    if(Object_keys(data,1) == jsonKey[1])
//    {
//        var html_data = "<option>Select</option>";
//        for(var i=0;i<data.telephonetypecodes.length;i++)
//        {
//            html_data += "<option value='"+data.telephonetypecodes[i].TELE_CODE+"'>"+data.telephonetypecodes[i].TELE_DESC+"</option>";
//        }
//        $('#teleTypeCode').empty();
//        $('#teleTypeCode').html(html_data);
//    }
//
//    if(Object_keys(data,2) == jsonKey[2])
//    {
//        var html_data = "<option>Select</option>";
//        for(var i=0;i<data.nationcodes.length;i++)
//        {
//            html_data += "<option value='"+data.nationcodes[i].NATION_CODE+"'>"+data.nationcodes[i].NATION_DESC+"</option>";
//        }
//        $('#nationValue').empty();
//        $('#nationValue').html(html_data);
//    }
//
//    if(Object_keys(data,3) == jsonKey[3])
//    {
//        var html_data = "<option>Select</option>";
//        for(var i=0;i<data.statecodes.length;i++)
//        {
//            html_data += "<option value='"+data.statecodes[i].STATE_CODE+"'>"+data.statecodes[i].STATE_DESC+"</option>";
//        }
//        $('#stateValue').empty();
//        $('#stateValue').html(html_data);
//    }
//
//    if(Object_keys(data,4) == jsonKey[4])
//    {
//        var html_data = "<option>Select</option>";
//        for(var i=0;i<data.addresstypecodes.length;i++)
//        {
//            html_data += "<option value='"+data.addresstypecodes[i].ADDRESS_CODE+"'>"+data.addresstypecodes[i].ADDRESS_DESC+"</option>";
//        }
//        $('#addressTypeCode').empty();
//        $('#addressTypeCode').html(html_data);
//        //setTimeout(function(){stopActivitySpinner()},2000);
//    }
//
//}


/**
 *Dispaly address codes in select options
 */
function getAddressCodesJSON()
{
    startActivitySpinner();
    var data = window.localStorage.getItem("ADDRESS_JSON");
    data = jQuery.parseJSON(data);
    var jsonKey = ["telephonetypecodes","nationcodes","statecodes","addresstypecodes"];
    
    
    /*if(Object_keys(data,0) == jsonKey[0])
     {
     var countyKey = [];
     var countyVal = [];
     for(var i=0;i<data.countycodes.length;i++)
     {
     countyKey[i] = data.countycodes[i].COUNTY_CODE;
     countyVal[i] = data.countycodes[i].COUNTY_DESC;
     }
     
     var displayId = 'countyDispVal';
     var saveId = 'countyValue';
     var dataKey = countyKey;
     var dataValue = countyVal;
     onAutoComplete(displayId, saveId, dataKey, dataValue);
     
     }*/
    
    if(Object_keys(data,0) == jsonKey[0])
    {
        var html_data = "<option>Select</option>";
        for(var i=0;i<data.telephonetypecodes.length;i++)
        {
            html_data += "<option value='"+data.telephonetypecodes[i].TELE_CODE+"'>"+data.telephonetypecodes[i].TELE_DESC+"</option>";
        }
        $('#teleTypeCode').empty();
        $('#teleTypeCode').html(html_data);
    }
    
    if(Object_keys(data,1) == jsonKey[1])
    {
        var nationKey = [];
        var nationVal = [];
        for(var i=0;i<data.nationcodes.length;i++)
        {
            nationKey[i] =   data.nationcodes[i].NATION_CODE;
            nationVal[i] =   data.nationcodes[i].NATION_DESC;
        }
        
        var displayId = 'nationDispVal';
        var saveId = 'nationValue';
        var dataKey = nationKey;
        var dataValue = nationVal;
        onAutoComplete(displayId, saveId, dataKey, dataValue);
    }
    
    if(Object_keys(data,2) == jsonKey[2])
    {
        /*var stateKey = [];
         var stateVal = [];
         for(var i=0;i<data.statecodes.length;i++)
         {
         stateKey[i] = data.statecodes[i].STATE_CODE;
         stateVal[i] = data.statecodes[i].STATE_DESC;
         }
         var displayId = 'stateDispVal';
         var saveId = 'stateValue';
         var dataKey = stateKey;
         var dataValue = stateVal;
         onAutoComplete(displayId, saveId, dataKey, dataValue);*/
        
        var stateLength = data.statecodes.length;
        if(bannerAppAutoCompleter == false) {  // Check if bannerAppAutoCompleter is set as a false then state will be shown in drop down
            var html_state = "<option>Select</option>";
            for(var i=0; i<stateLength; i++) {
                html_state += "<option value='"+data.statecodes[i].STATE_CODE+"'>"+data.statecodes[i].STATE_DESC+"</option>"
            }
            $('#stateValue').empty();
            $('#stateValue').html(html_state);
            
            
        }
    }
    
    if(Object_keys(data,3) == jsonKey[3])
    {
        var html_data = "<option>Select</option>";
        for(var i=0;i<data.addresstypecodes.length;i++)
        {
            html_data += "<option value='"+data.addresstypecodes[i].ADDRESS_CODE+"'>"+data.addresstypecodes[i].ADDRESS_DESC+"</option>";
        }
        $('#addressTypeCode').empty();
        $('#addressTypeCode').html(html_data);
        setTimeout(function(){stopActivitySpinner()},200);
    }
    
}


/**
 *get name of code values in Address
 */
function getAddressCodeName(countyKey, teleKey, nationKey, stateKey, addressKey)
{
    var data = window.localStorage.getItem("ADDRESS_JSON");
    data = jQuery.parseJSON(data);
    var jsonKey = ["telephonetypecodes","nationcodes","statecodes","addresstypecodes"];
    
    /* if(countyKey)
     {
     if(Object_keys(data, 0) == jsonKey[0])
     {
     for(var i=0;i<data.countycodes.length;i++)
     {
     if(data.countycodes[i].COUNTY_CODE == countyKey)
     {
     return data.countycodes[i].COUNTY_DESC;
     }
     }
     
     }
     }*/
    
    
    if(teleKey)
    {
        if(Object_keys(data, 0) == jsonKey[0])
        {
            for(var i=0;i<data.telephonetypecodes.length;i++)
            {
                if(data.telephonetypecodes[i].TELE_CODE == teleKey)
                {
                    return data.telephonetypecodes[i].TELE_DESC;
                }
            }
        }
    }
    
    if(nationKey)
    {
        if(Object_keys(data, 1) == jsonKey[1])
        {
            for(var i=0;i<data.nationcodes.length;i++)
            {
                if(data.nationcodes[i].NATION_CODE == nationKey)
                {
                    return data.nationcodes[i].NATION_DESC;
                }
            }
        }
    }
    
    if(stateKey)
    {
        if(Object_keys(data, 2) == jsonKey[2])
        {
            for(var i=0;i<data.statecodes.length;i++)
            {
                if(data.statecodes[i].STATE_CODE == stateKey)
                {
                    return data.statecodes[i].STATE_DESC;
                }
            }
        }
    }
    
    if(addressKey)
    {
        console.log(JSON.stringify(data));
        
        console.log("Address =  "+Object_keys(data, 3)+" "+jsonKey[3]);
        
        if(Object_keys(data, 4) == jsonKey[4])
        {
            for(var i=0;i<data.addresstypecodes.length;i++)
            {
                if(data.addresstypecodes[i].ADDRESS_CODE == addressKey)
                {
                    
                    return data.addresstypecodes[i].ADDRESS_DESC;
                }
            }
        }
        
    }
    
}


/**
 *To save Address
 */
function setAddress(serviceTicket)
{
    var  server_url = serviceName+"/enmu/services/student/banneraddressinsert"+"?ticket="+serviceTicket;
    var addressTypeCode = $('#addressTypeCode').val();
    var checkAddressArr = window.localStorage.getItem("CHECK_ADDRESS_TYPE");
    checkAddressArr = jQuery.parseJSON(checkAddressArr);
    
    //var fromDateOne = ($('#validFrom').val()).split('-');
    //fromDateOne = new Date(fromDateOne[0],fromDateOne[1]-1,fromDateOne[2]);
    /** to check address type exist with same address and datestamp  **/
    var fromDateOne = "";
    for(var key in checkAddressArr)
    {
        if(key)
        {
            if(key == addressTypeCode)
            {
                navigator.notification.alert(getAddressCodeName(null, null, null, null, key)+" address type already exists. Please select another address type to continue",null,"Address",'Ok');
                stopActivitySpinner();
                return;
            }
        }
        
    }
    
    try{
        /*var fromDate = reformatDate($('#validFrom').val());
         var toDate = reformatDate($('#until').val());
         if(fromDate == "undefined-undefined-")
         {
         navigator.notification.alert("Please enter a Valid from date",null,"Address",null);
         stopActivitySpinner();
         return;
         }
         
         
         var fromDateObj = ($('#validFrom').val()).split('-');
         var toDateObj = ($('#until').val()).split('-');;
         
         var fromDateCheck = new Date(fromDateObj[0],fromDateObj[1]-1,fromDateObj[2]);
         var toDateCheck = new Date(toDateObj[0],toDateObj[1]-1,toDateObj[2]);
         
         var current = new Date();
         var today = new Date(current.getFullYear(),current.getMonth(), current.getDate()); //to elimnate time from date object and compare it with mention date
         
         if(fromDateCheck < today)
         {
         navigator.notification.alert("Valid from date should be greater than today's date",null,"Address",null);
         stopActivitySpinner();
         return;
         }
         
         if(fromDateCheck > toDateCheck)
         {
         navigator.notification.alert("Until date should be greater than Valid from date",null,"Address",null);
         stopActivitySpinner();
         return;
         }
         
         if(toDate == "undefined-undefined-")
         {
         toDate = ""; // Insert to date to blank i.e, it has unlimited end value
         }
         
         */
        var city = $('#cityValue').val() ? $('#cityValue').val().trim() : '';
        if (!(city.search(/[^a-z. A-Z. ]+/) === -1))
        {
            navigator.notification.alert("Only alphabets and periods are allowed in City.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        
        if(city == "")
        {
            navigator.notification.alert("Please enter City.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        
        //var nationCode = 157;
        
        var nationCode = $('#nationValue').val();
        if(!nationCode || nationCode=="Select")
        {
            //navigator.notification.alert("Please enter nation.",null,"Address",null);
            //stopActivitySpinner();
            //return;
            var nationCode = 157;
        }
        
        var stateCode = $('#stateValue').val() ? $('#stateValue').val().replace('Select','').trim() : '';
        if(stateCode == ""){
            navigator.notification.alert("Please select a valid state.",null,"State",'Ok');
            stopActivitySpinner();
            return;
        }
        //var countyCode = $('#countyValue').val() ? $('#countyValue').val().replace('Select','').trim() : '';
        var zip = $('#zipCode').val() ? $('#zipCode').val().trim() : '';
        
        
        
        //alert("1472")
        //if(nationCode == countryUSA)
        //{
        if(zip.length == 5){
            
        }else{
            var zipPattern = new RegExp("^\\d{5}(-\\d{4})?$");
            //var zipPattern1 = /^[0-9]{5}$/;
            //var zipPattern2 = /^[0-9]{4}$/;
            
            if(!zipPattern.test(zip))
            {
                navigator.notification.alert("Please enter a valid zip code.",null,"Zip Code",'Ok');
                stopActivitySpinner();
                return;
            }
            else
            {
                
            }
            
        }
        
        // if zip check valid
        /*if(zip && !zipPattern.test(zip))
         {
         navigator.notification.alert("Please enter a valid Zip code",null,"Emergency Contact",null);
         stopActivitySpinner();
         return;
         }
         */
        // if state check zip
        /*if(stateCode)
         {
         if(!zip)
         {
         navigator.notification.alert("Please enter Zip code",null,"Emergency Contact",null);
         stopActivitySpinner();
         return;
         }
         }*/
        //}
        /*else if(zip && !isValidPostalCode(zip, nationCode))
         {
         navigator.notification.alert("Please enter a valid Zip code",null,"Emergency Contact",null);
         stopActivitySpinner();
         return;
         }*/
        
        countyCode = "";
        
        if(addressTypeCode == "Select")
        {
            navigator.notification.alert("Please select Address type.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        // alert("1532")
        /*var houseNumber = $('#houseNumber').val() ? $('#houseNumber').val().trim() : '';
         if(houseNumber.length>10)
         {
         navigator.notification.alert("House number should be less than 10 characters",null,"Address",null);
         stopActivitySpinner();
         return;
         }*/
        
        
        var phoneCountryCode = $('#countryPhone').val() ? $('#countryPhone').val().trim() : '';
        var phoneAreaCode = $('#areaPhone').val() ? $('#areaPhone').val().trim() : '';
        var phoneNumber = $('#phoneValue').val() ? $('#phoneValue').val().trim() : '';
        var extension = $('#extensionValue').val() ? $('#extensionValue').val().trim() : '';
        //var internationalAccessCode = $('#internationalAccess').val() ? $('#internationalAccess').val().trim() : '';
        var internationalAccessCode = "";
        
        if(phoneCountryCode.length != 0){
            if (phoneCountryCode.search(/^$|^[0-9 ]+$/) === -1) {
                navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
                stopActivitySpinner();
                return;
            }
        }
        
        if(phoneAreaCode.length != 0){
            if(phoneAreaCode.length != 3){
                navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
                stopActivitySpinner();
                return;
            }
            else if(phoneAreaCode.search(/^[0-9]+$/) === -1) {
                navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
                stopActivitySpinner();
                return;
            }
        }
        
        if(phoneNumber.length != 0){
            if(phoneNumber.length != 7){
                navigator.notification.alert("Please enter a valid phone number. ",null,"Address",'Ok');
                stopActivitySpinner();
                return;
            }
            else if(phoneNumber.search(/^[0-9 ]+$/) === -1) {
                navigator.notification.alert("Please enter a valid phone number. ",null,"Address",'Ok');
                stopActivitySpinner();
                return;
            }
        }
        
        if(extension.search(/^$|^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        
        /*if(internationalAccessCode.length != 0){
         if (internationalAccessCode.search(/^$|^[0-9 ]+$/) === -1) {
         navigator.notification.alert("Enter numbers in International Access code",null,"Address",null);
         stopActivitySpinner();
         return;
         }
         }*/
        
        
        /* if(phoneCountryCode.length>4 || phoneAreaCode.length != 3 || phoneNumber.length != 7 || extension.length != 3)
         {
         navigator.notification.alert("Please enter a valid phone number.",null,"Address",null);
         stopActivitySpinner();
         return;
         }*/
        
        
        //alert("1627")
        
        var street1 = $('#address1').val() ? $('#address1').val().trim() : '';
        var street2 = (($('#address2').val()=="null")?"":$('#address2').val());
        var street3 = (($('#address3').val()=="null")?"":$('#address3').val());
        var street4 = "";//$('#address4').val() ? $('#address4').val().trim() : '';
        //var unlistedInd = $('#unlistedInd') ? $('#unlistedInd').val().trim() : '';
        var unlistedInd = "";
        
        if(street1.length == 0){
            navigator.notification.alert('Please enter a valid address.',doNothing,'Address','Ok');
            stopActivitySpinner();
            return;
        }
        /*if(street3.length == 0){
         navigator.notification.alert('Please enter a valid address.',doNothing,'Address','Ok');
         stopActivitySpinner();
         return;
         }*/
        
        if(/^[a-zA-Z0-9- ]*$/.test(street1) == false) {
            navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in addresses.',doNothing,'Address','Ok');
            stopActivitySpinner();
            return;
        }
        
        if(/^[a-zA-Z0-9- ]*$/.test(street2) == false) {
            navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in addresses.',doNothing,'Address','Ok');
            stopActivitySpinner();
            return;
        }
        
        if(/^[a-zA-Z0-9- ]*$/.test(street3) == false) {
            navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in addresses.',doNothing,'Address','Ok');
            stopActivitySpinner();
            return;
        }
        
        
        //alert("1605")
        
        var a='' , b='', c='', d='', e='', f='', g='', h='', j='', k='',l='',m='';
        if(lengthCal > 1)
        {
            for(var i=1;i<lengthCal;i++)
            {
                a += (($('#teleTypeCode_'+i).val()=="null")?"":$('#teleTypeCode_'+i).val());
                b += (($('#addCountryPhone_'+i).val()=="null")?"":$('#addCountryPhone_'+i).val());
                c += (($('#addAreaPhone_'+i).val()=="null")?"":$('#addAreaPhone_'+i).val());
                d += (($('#addPhoneValue_'+i).val()=="null")?"":$('#addPhoneValue_'+i).val());
                e += (($('#addExtensionValue_'+i).val()=="null")?"":$('#addExtensionValue_'+i).val());
                f += (($('#addInternationalAccess_'+i).val()=="null")?"":$('#addInternationalAccess_'+i).val());
                g += (($('#addUnlistedInd_'+i).val()=="null")?"":$('#addUnlistedInd_'+i).val());
                h += (($('#addSequence_'+i).val()=="null")?"":$('#addSequence_'+i).val());
            }
        }
        /*var additionalTeleCodes = $('#teleTypeCode').val()+a;
         //var additionalCountryCodes = (($('#addCountryPhone').val()+b)=="null"?"":($('#addCountryPhone').val()+b));
         var additionalCountryCodes = "";
         var additionalareaCodes =   (($('#addAreaPhone').val()+c)=="null"?"":($('#addAreaPhone').val()+c));
         var additonalPhoneNumbers =  (($('#addPhoneValue').val()+d)=="null"?"":($('#addPhoneValue').val()+d));
         var additionalExtensions = (($('#addExtensionValue').val()+e)=="null"?"":($('#addExtensionValue').val()+e));
         //var additionalIntAccess =  (($('#addInternationalAccess').val()+f)=="null"?"":($('#addInternationalAccess').val()+f));
         var additionalIntAccess =  "";
         var additionalUnlistedInd =  (($('#addUnlistedInd').val("")+g)=="null"?"":($('#addUnlistedInd').val()+g));
         var additionalSequence =  (($('#addSequence').val()+h)=="null"?"":($('#addSequence').val()+h));
         var additionalDelete = "";*/
        
        var additionalTeleCodes = a;
        //var additionalCountryCodes = (($('#addCountryPhone').val()+b)=="null"?"":($('#addCountryPhone').val()+b));
        var additionalCountryCodes = "";
        var additionalareaCodes =   ((c)=="null"?"":(c));
        var additonalPhoneNumbers =  ((d)=="null"?"":(d));
        var additionalExtensions = ((e)=="null"?"":(e));
        //var additionalIntAccess =  (($('#addInternationalAccess').val()+f)=="null"?"":($('#addInternationalAccess').val()+f));
        var additionalIntAccess =  "";
        var additionalUnlistedInd =  ((g)=="null"?"":(g));
        var additionalSequence =  ((h)=="null"?"":(h));
        var additionalDelete = "";
        
        
        
        if(additonalPhoneNumbers == "")
        {
            additionalTeleCodes = ""; //in case user doesn't insert additional phone number
        }
        
        if (additionalTeleCodes.search(/Select/) != -1) {
            navigator.notification.alert("Please select Telephone type.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        
        /*if (additionalCountryCodes.search(/^$|^[-,0-9 ]+$/) === -1) {
         navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
         stopActivitySpinner();
         return;
         }*/
        
        //   alert(additionalareaCodes+" "+additonalPhoneNumbers+" "+additionalExtensions);
        if (additionalareaCodes.search(/^$|^[-,0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        
        if (additonalPhoneNumbers.search(/^$|^[-,0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        
        if (additionalExtensions.search(/^$|^[-,0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        
        /*if (additionalIntAccess.search(/^$|^[-,0-9 ]+$/) === -1) {
         navigator.notification.alert("Enter numbers in International Access Code",null,"Address",'Ok');
         stopActivitySpinner();
         return;
         }*/
        
        /*if(phoneCountryCode.length>4 || phoneAreaCode.length != 3 || phoneNumber.length != 7 || extension.length != 3)
         {
         navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
         stopActivitySpinner();
         return;
         }*/
        
        console.log("data passed address insert--> atypcode="+addressTypeCode+"&fromdate=&todate=&housenumber=&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes="+additionalCountryCodes+"&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+additionalDelete);
        
        //startActivitySpinner();
        
        
        
        var xhr=$.ajax({
                       url: server_url,
                       contentType: 'application/json;charset=UTF-8',
                       type: 'POST',
                       data: "atypcode="+addressTypeCode+"&fromdate=&todate=&housenumber=&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes="+additionalCountryCodes+"&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+additionalDelete,
                       beforeSend:function (){
                       setTimeout(function(){
                                  xhr.abort();
                                  },globalAjaxTimer);},
                       success: function(data)
                       {
                       console.log('success setAddress--> ');
                       console.log(data);
                       if(data)
                       {
                       navigator.notification.alert('Address saved successfully.',null,'Address Update','Ok');
                       }
                       url_service="/enmu/services/student/banneraddressfetch";
                       return_Where = getAddress;
                       request_ST(url_service,return_Where,false);
                       },
                       error: function(e)
                       {
                       console.log('error setAddress--> ');
                       console.log(e);
                       stopActivitySpinner();
                       alertServerError(e.status);
                       }
                       });
    }catch(e){alert(e)}
    
}



/**
 *To update address
 */
function setUpdateAddress(serviceTicket)
{
    
    var additionalTeleCodesArr = [], additionalCountryCodesArr = [], additionalareaCodesArr = [], additonalPhoneNumbersArr = [], additionalExtensionsArr = [], additionalIntAccessArr = [], additionalUnlistedIndArr = [], additionalSequenceArr = [];
    var storeUpdateData = "";
    var server_url = serviceName+"/enmu/services/student/banneraddressupdate"+"?ticket="+serviceTicket;
    var sequence = $('#seq').val();
    if(sequence)
    {
        storeUpdateData += "seqno="+sequence+"&";
    }
    
    //var fromDate = reformatDate($('#validFrom').val());
    //var toDate = reformatDate($('#until').val());
    /*if(fromDate == "undefined-undefined-")
     {
     navigator.notification.alert("Please enter Valid from date",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }
     
     
     var fromDateObj = ($('#validFrom').val()).split('-');
     var toDateObj = ($('#until').val()).split('-');;
     
     var fromDateCheck = new Date(fromDateObj[0],fromDateObj[1]-1,fromDateObj[2]);
     var toDateCheck = new Date(toDateObj[0],toDateObj[1]-1,toDateObj[2]);
     var current = new Date();
     var today = new Date(current.getFullYear(),current.getMonth(), current.getDate()); //to elimnate time from date object and compare it with mention date
     
     
     if(fromDateCheck > toDateCheck)
     {
     navigator.notification.alert("Until date should be greater than Valid from date",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }
     
     
     if(toDateCheck < today)
     {
     navigator.notification.alert("Until date should be greater than today's date",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }
     
     if(toDate == "undefined-undefined-")
     {
     toDate = "";
     }
     
     
     
     var houseNumber = $('#houseNumber').val();
     if(houseNumber.length>10)
     {
     navigator.notification.alert("House number should be less than 10 characters",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }
     */
    
    var city = $('#cityValue').val() ? $('#cityValue').val().trim() : '';
    if (!(city.search(/[^a-z. A-Z. ]+/) === -1))
    {
        navigator.notification.alert("Only alphabets and periods are allowed in City.",null,"Address",'Ok');
        stopActivitySpinner();
        return;
    }
    
    if(city == "")
    {
        navigator.notification.alert("Please enter City.",null,"Address",'Ok');
        stopActivitySpinner();
        return;
    }
    
    var nationCode = $('#nationValue').val();
    
    if(!nationCode || nationCode=="Select")
    {
        //navigator.notification.alert("Please enter Nation.",null,"Address",'Ok');
        //stopActivitySpinner();
        //return;
        var nationCode = 157;
    }
    
    var stateCode = $('#stateValue').val() ? $('#stateValue').val().replace('Select','').trim() : '';
    
    if(stateCode == ""){
        navigator.notification.alert("Please select a valid state.",null,"State",'Ok');
        stopActivitySpinner();
        return;
    }
    //var countyCode = $('#countyValue').val() ? $('#countyValue').val().replace('Select','').trim() : '';
    var zip = $('#zipCode').val() ? $('#zipCode').val().trim() : '';
    
    
    
    //alert("1472")
    //if(nationCode == countryUSA)
    //{
    if(zip.length == 5){
        
    }else{
        var zipPattern = new RegExp("^\\d{5}(-\\d{4})?$");
        //var zipPattern1 = /^[0-9]{5}$/;
        //var zipPattern2 = /^[0-9]{4}$/;
        
        if(!zipPattern.test(zip))
        {
            navigator.notification.alert("Please enter a valid zip code.",null,"Zip Code",'Ok');
            stopActivitySpinner();
            return;
        }
        else
        {
            
        }
        
    }
    // if zip check valid
    /*if(zip && !zipPattern.test(zip))
     {
     navigator.notification.alert("Please enter a valid Zip code",null,"Emergency Contact",'Ok');
     stopActivitySpinner();
     return;
     }
     */
    // if state check zip
    if(stateCode)
    {
        if(!zip)
        {
            navigator.notification.alert("Please enter Zip code.",null,"Emergency Contact",'Ok');
            stopActivitySpinner();
            return;
        }
    }
    //}
    /*else if(zip && !isValidPostalCode(zip, nationCode))
     {
     navigator.notification.alert("Please enter a valid Zip code",null,"Emergency Contact",'Ok');
     stopActivitySpinner();
     return;
     }*/
    
    countyCode = "";
    var addressTypeCode = $('#addressTypeCode').val();
    var checkAddressArr = window.localStorage.getItem("CHECK_ADDRESS_TYPE");
    checkAddressArr = jQuery.parseJSON(checkAddressArr);
    
    
    
    if(addressTypeCode == "Select")
    {
        navigator.notification.alert("Please select Address type.",null,"Address",'Ok');
        stopActivitySpinner();
        return;
    }
    // alert("1532")
    /*var houseNumber = $('#houseNumber').val() ? $('#houseNumber').val().trim() : '';
     if(houseNumber.length>10)
     {
     navigator.notification.alert("House number should be less than 10 characters",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }*/
    
    
    var phoneCountryCode = $('#countryPhone').val() ? $('#countryPhone').val().trim() : '';
    var phoneAreaCode = $('#areaPhone').val() ? $('#areaPhone').val().trim() : '';
    var phoneNumber = $('#phoneValue').val() ? $('#phoneValue').val().trim() : '';
    var extension = $('#extensionValue').val() ? $('#extensionValue').val().trim() : '';
    //var internationalAccessCode = $('#internationalAccess').val() ? $('#internationalAccess').val().trim() : '';
    var internationalAccessCode = "";
    
    /* if(phoneCountryCode.length != 0){
     if (phoneCountryCode.search(/^$|^[0-9 ]+$/) === -1) {
     navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }
     }*/
    
    if(phoneAreaCode.length != 0){
        if(phoneAreaCode.length != 3){
            navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        else if (phoneAreaCode.search(/^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
    }
    
    if(phoneNumber.length != 0){
        if(phoneNumber.length != 7)
        {
            navigator.notification.alert("Please enter a valid phone number. ",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
        else if(phoneNumber.search(/^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number. ",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
    }
    
    if(extension.length != 0){
        if(extension.search(/^[0-9 ]+$/) === -1) {
            navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
            stopActivitySpinner();
            return;
        }
    }
    
    /* if(internationalAccessCode.length != 0){
     if (internationalAccessCode.search(/^$|^[0-9 ]+$/) === -1) {
     navigator.notification.alert("Enter numbers in International Access code",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }
     }*/
    
    
    /*if(phoneCountryCode.length>4 || phoneAreaCode.length != 3 || phoneNumber.length != 7 || extension.length != 3)
     {
     navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }*/
    
    
    //alert("1627")
    
    var street1 = $('#address1').val() ? $('#address1').val().trim() : '';
    var street2 = (($('#address2').val()=="null")?"":$('#address2').val());
    var street3 = (($('#address3').val()=="null")?"":$('#address3').val());
    var street4 = "";//$('#address4').val() ? $('#address4').val().trim() : '';
    //var unlistedInd = $('#unlistedInd') ? $('#unlistedInd').val().trim() : '';
    var unlistedInd = "";
    
    if(street1.length == 0){
        navigator.notification.alert('Please enter a valid address.',doNothing,'Address','Ok');
        stopActivitySpinner();
        return;
    }
    
    
    if(/^[a-zA-Z0-9- ]*$/.test(street1) == false) {
        navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in addresses.',doNothing,'Address','Ok');
        stopActivitySpinner();
        return;
    }
    
    if(/^[a-zA-Z0-9- ]*$/.test(street2) == false) {
        navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in addresses.',doNothing,'Address','Ok');
        stopActivitySpinner();
        return;
    }
    
    if(/^[a-zA-Z0-9- ]*$/.test(street3) == false) {
        navigator.notification.alert('Special characters (" , + - * / \ @ # $ % ^ &) should not be used in addresses.',doNothing,'Address','Ok');
        stopActivitySpinner();
        return;
    }
    
    // alert("2034")
    
    var a='' , b='', c='', d='', e='', f='', g='', h='', j='', k='',l='',m='';
    
    if(updateAdditionalTeleCodes.length != 0){a=updateAdditionalTeleCodes+","}
    if(UpdateadditionalCountryCodes.length != 0){b=UpdateadditionalCountryCodes+","}
    if(UpdateadditionalareaCodes.length != 0){c=UpdateadditionalareaCodes+","}
    if(UpdateadditonalPhoneNumbers.length != 0){d=UpdateadditonalPhoneNumbers+","}
    if(UpdateadditionalExtensions.length != 0 ){e=UpdateadditionalExtensions+","}
    if(UpdateadditionalIntAccess.length != 0){f=UpdateadditionalIntAccess+","}
    if(UpdateadditionalUnlistedInd.length != 0){g=UpdateadditionalUnlistedInd+","}
    if(UpdateadditionalSequence.length != 0){h=UpdateadditionalSequence+","}
    
    if(lengthCal > 1)
    {
        for(var i=1;i<lengthCal;i++)
        {
            a += (($('#teleTypeCode_'+i).val()=="null")?"":$('#teleTypeCode_'+i).val());
            b += (($('#addCountryPhone_'+i).val()=="null")?"":$('#addCountryPhone_'+i).val());
            c += (($('#addAreaPhone_'+i).val()=="null")?"":$('#addAreaPhone_'+i).val());
            d += (($('#addPhoneValue_'+i).val()=="null")?"":$('#addPhoneValue_'+i).val());
            e += (($('#addExtensionValue_'+i).val()=="null")?"":$('#addExtensionValue_'+i).val());
            f += (($('#addInternationalAccess_'+i).val()=="null")?"":$('#addInternationalAccess_'+i).val());
            g += (($('#addUnlistedInd_'+i).val()=="null")?"":$('#addUnlistedInd_'+i).val());
            h += (($('#addSequence_'+i).val()=="null")?"":$('#addSequence_'+i).val());
        }
    }
    /*var additionalTeleCodes = $('#teleTypeCode').val()+a;
     //var additionalCountryCodes = (($('#addCountryPhone').val()+b)=="null"?"":($('#addCountryPhone').val()+b));
     var additionalCountryCodes = "";
     var additionalareaCodes =   (($('#addAreaPhone').val()+c)=="null"?"":($('#addAreaPhone').val()+c));
     var additonalPhoneNumbers =  (($('#addPhoneValue').val()+d)=="null"?"":($('#addPhoneValue').val()+d));
     var additionalExtensions = (($('#addExtensionValue').val()+e)=="null"?"":($('#addExtensionValue').val()+e));
     //var additionalIntAccess =  (($('#addInternationalAccess').val()+f)=="null"?"":($('#addInternationalAccess').val()+f));
     var additionalIntAccess =  "";
     var additionalUnlistedInd =  (($('#addUnlistedInd').val("")+g)=="null"?"":($('#addUnlistedInd').val()+g));
     var additionalSequence =  (($('#addSequence').val()+h)=="null"?"":($('#addSequence').val()+h));
     var additionalDelete = "";*/
    
    var additionalTeleCodes = a;
    //var additionalCountryCodes = (($('#addCountryPhone').val()+b)=="null"?"":($('#addCountryPhone').val()+b));
    var additionalCountryCodes = "";
    var additionalareaCodes =   ((c)=="null"?"":(c));
    var additonalPhoneNumbers =  ((d)=="null"?"":(d));
    var additionalExtensions = ((e)=="null"?"":(e));
    //var additionalIntAccess =  (($('#addInternationalAccess').val()+f)=="null"?"":($('#addInternationalAccess').val()+f));
    var additionalIntAccess =  "";
    var additionalUnlistedInd =  ((g)=="null"?"":(g));
    var additionalSequence =  ((h)=="null"?"":(h));
    var additionalDelete = "";
    
    
    
    if(additonalPhoneNumbers == "")
    {
        additionalTeleCodes = ""; //in case user doesn't insert additional phone number
    }
    
    if (additionalTeleCodes.search(/Select/) != -1) {
        navigator.notification.alert("Please select Telephone type",null,"Address",'Ok');
        stopActivitySpinner();
        return;
    }
    
    /*if (additionalCountryCodes.search(/^$|^[-,0-9 ]+$/) === -1) {
     navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }*/
    
    if (additionalareaCodes.search(/^$|^[-,0-9 ]+$/) === -1) {
        navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
        stopActivitySpinner();
        return;
    }
    if (additonalPhoneNumbers.search(/^$|^[-,0-9 ]+$/) === -1) {
        navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
        stopActivitySpinner();
        return;
    }
    if (additionalExtensions.search(/^$|^[-,0-9 ]+$/) === -1) {
        navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
        stopActivitySpinner();
        return;
    }
    
    /*if (additionalIntAccess.search(/^$|^[-,0-9 ]+$/) === -1) {
     navigator.notification.alert("Enter numbers in International Access Code",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }*/
    
    /*if(phoneCountryCode.length>4 || phoneAreaCode.length != 3 || phoneNumber.length != 7 || extension.length != 3)
     {
     navigator.notification.alert("Please enter a valid phone number.",null,"Address",'Ok');
     stopActivitySpinner();
     return;
     }*/
    
    
    
    
    
    
    
    try{ additionalTeleCodesArr = ($('#addPriUpdate').val().replace(/null/g,"")).split(',');
        //additionalCountryCodesArr = ($('#addCountryPhone').val().replace(/null/g,"")).split(',');
        additionalCountryCodesArr = "";
        additionalareaCodesArr = ($('#addAreaPhone').val().replace(/null/g,"")).split(',');
        additonalPhoneNumbersArr =  ($('#addPhoneValue').val().replace(/null/g,"")).split(',');
        additionalExtensionsArr =  ($('#addExtensionValue').val().replace(/null/g,"")).split(',');
        //additionalIntAccessArr =  ($('#addInternationalAccess').val().replace(/null/g,"")).split(',');
        additionalIntAccessArr =  "";
        
        additionalUnlistedIndArr = ($('#addUnlistedInd').val().replace(/null/g,"")).split(',');
        additionalSequenceArr = ($('#addSequence').val()).split(',');
        
        
        var addDelInd = additionalUnlistedIndArr;// sent same blank value as unlisted indicator
    }catch(e){}
    //alert("2047")
    //console.log("data passed address insert--> atypcode="+addressTypeCode+"&fromdate=&todate=&housenumber=&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes="+additionalCountryCodes+"&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensions+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+additionalDelete);
    
    try{
        console.log("update primary address -->"+storeUpdateData+"atypcode="+addressTypeCode+"&fromdate=&todate=&housenumber=&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addlseqnos="+additionalSequence+"&addltelecodes="+additionalTeleCodes+"&addlcountrycodes="+additionalCountryCodesArr+"&addlareacodes="+additionalareaCodes+"&addlphonenumbers="+additonalPhoneNumbers+"&addlphoneextns="+additionalExtensionsArr+"&addlintlaccess="+additionalIntAccess+"&addlunlistedinds="+additionalUnlistedInd+"&addldelinds="+additionalDelete);
    }catch(e){alert(e)}
    //startActivitySpinner();
    var xhr=$.ajax({
                   url: server_url,
                   contentType: 'application/json;charset=UTF-8',
                   type: 'POST',
                   data: storeUpdateData+"atypcode="+addressTypeCode+"&fromdate=&todate=&housenumber=&streetline1="+street1+"&streetline2="+street2+"&streetline3="+street3+"&streetline4="+street4+"&city="+city+"&statecode="+stateCode+"&zip="+zip+"&countycode="+countyCode+"&nationcode="+nationCode+"&phonecountrycode="+phoneCountryCode+"&phoneareacode="+phoneAreaCode+"&phonenumber="+phoneNumber+"&phoneextn="+extension+"&intlaccess="+internationalAccessCode+"&unlistedind="+unlistedInd+"&addlseqnos="+additionalSequenceArr+"&addltelecodes="+additionalTeleCodesArr+"&addlcountrycodes="+additionalCountryCodesArr+"&addlareacodes="+additionalareaCodesArr+"&addlphonenumbers="+additonalPhoneNumbersArr+"&addlphoneextns="+additionalExtensionsArr+"&addlintlaccess="+additionalIntAccessArr+"&addlunlistedinds="+additionalUnlistedIndArr+"&addldelinds="+addDelInd,
                   beforeSend:function (){
                   setTimeout(function(){
                              xhr.abort();
                              },globalAjaxTimer);},
                   success: function(data)
                   {
                   console.log('success setUpdateAddress--> ');
                   console.log(data);
                   console.log(JSON.stringify(data));
                   if(data)
                   {
                   navigator.notification.alert('Address updated successfully.',null,'Address','Ok');
                   window.localStorage.removeItem("INITIAL_UPDATE_ADDRESS_STATE");
                   window.localStorage.removeItem("INITIAL_UPDATE_ADDRESS_COUNTRY");
                   
                   }
                   url_service="/enmu/services/student/banneraddressfetch";
                   return_Where = getAddress;
                   request_ST(url_service,return_Where,false);
                   },
                   error: function(e)
                   {
                   console.log('error setUpdateAddress--> ');
                   console.log(e);
                   stopActivitySpinner();
                   alertServerError(e.status);
                   }
                   });
    
    
}



/**
 *Delete entery from address
 */
function deleteAddress(serviceTicket)
{
    startActivitySpinner();
    
    var addressSequence = $('#addSeq').val();
    var addressTypeCode = $('#addType').val();
    var server_url = serviceName+"/enmu/services/student/banneraddressdelete"+"?ticket="+serviceTicket;
    var xhr=$.ajax({
                   url: server_url,
                   type: 'POST',
                   data:{atypcode:addressTypeCode, seqno:addressSequence},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   success: function(data)
                   {
                   console.log("Deleted Success --> "+JSON.stringify(data));
                   if(data)
                   {
                   navigator.notification.alert('Address deleted successfully.',null,'Address','Ok');
                   }
                   url_service="/enmu/services/student/banneraddressfetch";
                   return_Where=getAddress;
                   request_ST(url_service,return_Where,false);
                   },
                   error: function(e)
                   {
                   stopActivitySpinner();
                   alertServerError(e.status);
                   }
                   
                   });
}

/**************************************** Model specific functions to insert modified input values **************************************/
/**
 *Reformat date
 */
function reformatDate(dateStr)
{
    var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
    var dArr;
    if(dateStr.indexOf("/") != -1) dArr = dateStr.split("/");
    else dArr = dateStr.split("-");  // ex input "2010-01-18"
    var reDate =  dArr[2]+ "-" +months[(parseInt(dArr[1])-1)]+ "-" +dArr[0]; //ex out: "18-JAN-2010"
    return reDate;
}

/**
 *date View format
 */
function dateViewFormat(str)
{
    var dArr = str.split("-");  // ex input "18-JAN-2010"
    var reDate =  dArr[1]+ " " +dArr[0]+ ", " +dArr[2]; //ex out: "JAN 18, 2010"
    return reDate;
}



/**
 *date View format for input
 */
function inputTypeDate(str)
{
    var dArr = str.split("-");// ex input "18-JAN-2010"
    var reDate =  dArr[2]+ "-" +switchMonth(dArr[1])+ "-" +dArr[0]; //ex out: "2010-01-18"
    
    function switchMonth(strMonth)
    {
        var mon;
        switch(strMonth)
        {
                
            case "JAN":
                mon = "01";
                break;
                
            case "FEB":
                mon = "02";
                break;
                
            case "MAR":
                mon = "03";
                break;
                
            case "APR":
                mon = "04";
                break;
                
            case "MAY":
                mon = "05";
                break;
                
            case "JUN":
                mon = "06";
                break;
                
            case "JUL":
                mon = "07";
                break;
                
            case "AUG":
                mon = "08";
                break;
                
            case "SEP":
                mon = "09";
                break;
                
            case "OCT":
                mon = "10";
                break;
                
            case "NOV":
                mon = "11";
                break;
                
            case "DEC":
                mon = "12";
                break;
                
            default:
                mon=""
                break;
        }
        return mon;
    }
    
    return reDate;
}


/**
 *to split date
 */
function splitDate(str)
{
    var str = inputTypeDate(str);
    var splitVar =  str.split('-');
    splitVar =    new Date(splitVar[0],splitVar[1]-1,splitVar[2]);
    return splitVar;
}

/** function to sperate diff keys **/

Object.keys = Object_keys;
function Object_keys(obj, index) {
    var keys = [], name;
    for (name in obj) {
        if (obj.hasOwnProperty(name)) {
            keys.push(name);
        }
    }
    return keys[index];
}

/** To display and store value on completion **/
function onAutoComplete(displayId, saveId, dataKey, dataValue)
{
    $('#'+displayId).autocomplete({
                                  lookup: dataValue,
                                  zIndex : 9999,
                                  noCache : true,
                                  onSelect : function(value, data) {
                                  //alert(value+" "+data);
                                  var index = dataValue.indexOf(value);
                                  var codeValue = dataKey[index];
                                  $('#'+saveId).val(codeValue);
                                  //alert($('#'+saveId).val());
                                  if(localStorage.getItem("UPDATE_ADDRESS_FLAG") == 1){
                                  disableFields('one');
                                  }else{
                                  disableFields();
                                  }
                                  
                                  if(saveId == 'stateValue'){
                                  var zip = $('#zipCode').val();
                                  if(!zip)
                                  {
                                  //navigator.notification.alert('Please enter zip',null,'Address','Ok');
                                  }
                                  //showHideMandatoryAddress(codeValue);
                                  }
                                  
                                  }
                                  });
    
    $('#'+displayId).change(function(){
                            var getValue = $('#'+displayId).val();
                            var index = dataValue.indexOf(getValue);
                            if(index == -1)
                            {
                            $('#'+saveId).val("Select");
                            }
                            else
                            {
                            var codeValue = dataKey[index];
                            $('#'+saveId).val(codeValue);
                            }
                            
                            if(localStorage.getItem("UPDATE_ADDRESS_FLAG") == 1){
                            disableFields('one');
                            }else{
                            disableFields();
                            }
                            if(saveId == 'stateValue'){
                            //showHideMandatoryAddress(codeValue);
                            }});
}
